<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	//Check Admin permissions
	$LAFundAdmin->check_admin_access();
	
	$groups = $LAFundRacing->get_unactive_groups();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript" src="/media/js/LAFR-register.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once($_SERVER['DOCUMENT_ROOT'].'/lafundracing/admin/shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1 class="pull-left">Registered / Unactive Organizations</h1>
		
		<div class="clearfix"></div>
		<hr />
		
		<div class="panel-group" id="accordion">
<?php 
		if(sizeof($groups->fetch_array()) > 0) {
			foreach($groups as $key => $value) {
				$org_name = $LAFundRacing->get_fund_organization_name($value['org_id']);
				include($_SERVER['DOCUMENT_ROOT'].'/lafundracing/partials/unactive-group-panel.php');
			}
		} else {
			echo '<h1 style="text-align: center; color: #cb202a;">No Current Groups</h1>';
		}
?>	  
		</div>		
	</div>	

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>