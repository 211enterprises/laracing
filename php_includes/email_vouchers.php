<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['id'])) {
		$hash = makeSQLSafe($mysqli,$_GET['id']);
		$formatHash = "'".str_replace("-","','",$hash)."'";
		
		//FETCH NEW PASSES FROM VOUCHERS
		$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` IN ($formatHash)");
		if($passQuery->num_rows == 0) exit;
		
		//FETCH CLASS INFO
		$passInfoQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` IN ($formatHash)");
		$passInfo = $passInfoQuery->fetch_assoc();
	} else exit;

?>
	<style>
		body {background:url('http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/bg.jpg') repeat #000; width:100%; height:100%; color:#FFF; font-family:Helvetica Neue, Helvetica, Arial; font-size:14px; margin:0; padding:0;}
	</style>
	<table width="500" align="center" border="0">
		<tr>
			<td colspan="5" width="500" height="255">
				<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/header.png" alt="">
			</td>
		</tr>
<?php if($passInfo['class_id'] != 0) {
			$classQuery = $mysqli->query("SELECT * FROM LARX_class_dates,LARX_track_locations WHERE LARX_class_dates.id = ".$passInfo['class_id']." AND LARX_class_dates.track_location = LARX_track_locations.track_id LIMIT 1");
			$class = $classQuery->fetch_assoc(); ?>
		<tr>
			<td width="20"></td>
			<td colspan="4" align="center">
				<font color="cb202a">Customer Name:</font><br><font size="5"><b><?php echo strtoupper($passInfo['buyer_name']); ?></b></font><br>
				<font color="cb202a">Class Date:</font><br><font size="4"><b><?php echo strtoupper(date("M jS, Y",strtotime($class['date']))); ?></b></font><br>
				<font color="cb202a">Class Time:</font><br><font size="4"><b><?php echo strtoupper(date("h:i A",strtotime($class['time']))); ?></b></font><br>
				<font color="cb202a">Track Location:</font><br><font size="4"><b><?php echo strtoupper($class['track_name']); ?></b></font><br>
				<font size="3">
					<?php echo strtoupper($class['address']); ?><br>
					<?php echo strtoupper($class['city'].", ".$class['state']." ".$class['zip_code']); ?>
				</font>
			</td>
		</tr>
<?php } ?>
		<tr>
			<td width="20" height="16"></td>
			<td colspan="3" width="462" height="16"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/cartDivLine.png" alt=""></td>
			<td width="18" height="16"></td>
		</tr>
		<tr>
			<td colspan="5" width="500" height="52">
				<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/orderSummary.png" alt="">
			</td>
		</tr>
<?php while($pass = $passQuery->fetch_assoc()) { ?>
		<tr>
			<td width="20"></td>
			<td colspan="3" width="462" height="71" style="background:url(/media/images/emailPasses/itemBG.png);">
				<font color="808080" size="5"><?php if($pass['driver_name'] != "") echo strtoupper($pass['driver_name']); else echo strtoupper($pass['buyer_name']); ?></font><br>
				<?php echo $pass['pass_number']; ?>  |  <a href="http://laracingx.com/passes/print.php?id=<?php echo $pass['pass_hash']; ?>" target="_blank" style="color:cb202a;">Print This Pass</a><br>
			</td>
			<td width="18"></td>
		</tr>
<?php } ?>
		<tr>
			<td colspan="5" width="500" height="14"></td>
		</tr>
		<tr>
			<td width="20" height="63"></td>
			<td width="462" height="63" colspan="3"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/getPrepared.png" alt="Get Prepared For Your Raceday!"></td>
			<td width="18" height="63"></td>
		</tr>
		<tr>
			<td width="20"></td>
			<td width="462" colspan="3">
				Fire suits, helmets, & gloves are provided for drivers who need them. You may use your own IF the appropriate specs are met. THIN SOLE, CLOSED TOE SHOES are required to drive. Racing shoes and other safety gear is generally available for purchase.
			</td>
			<td width="18"></td>
		</tr>
	</table>
<?php $mysqli->close(); ?>