<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` ORDER BY `first_name` ASC");

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Racer Profiles</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main" data-title="LA Racing X | Racer Profiles">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
	   <a href="/office/racers/newRacer.php" data-role="button" data-icon="plus" data-iconpos="left" data-rel="dialog" data-transition="slidedown" class="ui-btn-right">Add Racer Profile</a>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($racerQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="raceProfiles" data-inset="true" data-split-icon="gear" data-split-theme="c" data-filter="true" data-filter-placeholder="Search by name or phone #...">
   			<li data-listdivider="true" data-theme="a">Racers:</li>
   	<?php while($racer = $racerQuery->fetch_assoc()) { ?>
   			<li>
				<a href="/office/racers/racerHistory.php?r=<?php echo $racer['id']; ?>" data-rel="dialog" data-transition="pop"><?php echo $racer['phone']." - ".$racer['first_name']." ".$racer['last_name']; ?></a>
				<a href="/office/racers/editRacer.php?r=<?php echo $racer['id']; ?>" data-rel="dialog" data-transition="pop">Edit Racer</a>
				<?php if($racer['red_flag'] == 1) echo '<div class="racer-flag"><img src="/office/global/images/red_flag.png" alt="Racer Red Flagged" /></div>'; ?>
			</li>
   	<?php } ?>
   		</ul>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo footer("racers"); ?>
      
</div>

</body>
</html>
<?php
$racersQuery->close();
$mysqli->close();	
?>