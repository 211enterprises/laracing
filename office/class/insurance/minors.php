<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c'");
		$class = $classQuery->fetch_assoc();
		$classID = $class['id'];
		

	} else {
		header("Location: /office/".$c);
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>Minors Assumption of Risk</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="image/png" href="/media/images/favicon.ico" rel="icon" />
<link type="text/css" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" rel="stylesheet" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="https://raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<script src="/office/global/js/jSignature.min.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script>
	$(document).ready(function() {
		$("#minorSignature").jSignature({lineWidth:0})
		$("#witnessSignature").jSignature({lineWidth:0})
    });
</script>
<style type="text/css">
	html, body {background:#5d5d5d;}
	
	* {outline:none;}

@media only screen and (max-width:999px) {
	section.form {position:relative; background:url(/office/global/images/insurance/minorFormBG-mini.jpg) no-repeat; width:750px; height:971px; box-shadow:0px 0px 6px #636363; -webkit-box-shadow:0px 0px 6px #636363; -moz-box-shadow:0px 0px 6px #636363; -o-box-shadow:0px 0px 6px #636363; margin:0 auto;}
	input#location {position:absolute; top:96px; left:36px; width:430px;}
	input#dateSigned {position:absolute; top:96px; right:52px; width:214px;}
	input#minorName {position:absolute; top:642px; left:36px; width:430px;}
	input#minorDate {position:absolute; top:590px; right:28px; width:236px;}
	input#minorAge {position:absolute; top:642px; right:28px; width:236px;}
	input#witnessName {position:absolute; top:732px; right:28px; width:238px;}
	div#minorSignature {position:absolute; top:536px; left:0px; width:504px; height:40px;}
	div#minorSignature canvas {width:504px; height:40px;}
	div#witnessSignature {position:absolute; top:678px; left:0px; width:506px; height:40px;}
	div#witnessSignature canvas {width:506px; height:40px;}
}	
@media only screen and (min-width:1000px) {
	section.form {position:relative; background:url(/office/global/images/insurance/minorFormBG.jpg) no-repeat; width:1000px; height:1294px; box-shadow:0px 0px 6px #636363; -webkit-box-shadow:0px 0px 6px #636363; -moz-box-shadow:0px 0px 6px #636363; -o-box-shadow:0px 0px 6px #636363; margin:0 auto;}
	input#location {position:absolute; top:144px; left:50px; width:570px;}
	input#dateSigned {position:absolute; top:144px; right:68px; width:286px;}
	input#minorName {position:absolute; top:870px; left:50px; width:570px;}
	input#minorDate {position:absolute; top:802px; right:40px; width:314px;}
	input#minorAge {position:absolute; top:870px; right:40px; width:314px;}
	input#witnessName {position:absolute; top:990px; right:40px; width:314px;}
	div#minorSignature {position:absolute; top:724px; left:40px; width:630px; height:40px;}
	div#minorSignature canvas {width:630px; height:40px;}
	div#witnessSignature {position:absolute; top:913px; left:40px; width:630px; height:40px;}
	div#witnessSignature canvas {width:630px; height:40px;}
}
@media only screen and (device-width: 768px) and (orientation: landscape) {
	section.form {position:relative; background:url(/office/global/images/insurance/minorFormBG.jpg) no-repeat; width:1000px; height:1294px; box-shadow:0px 0px 6px #636363; -webkit-box-shadow:0px 0px 6px #636363; -moz-box-shadow:0px 0px 6px #636363; -o-box-shadow:0px 0px 6px #636363; margin:0 auto;}
	input#location {position:absolute; top:144px; left:50px; width:570px;}
	input#dateSigned {position:absolute; top:144px; right:68px; width:286px;}
	input#minorName {position:absolute; top:870px; left:50px; width:570px;}
	input#minorDate {position:absolute; top:802px; right:40px; width:314px;}
	input#minorAge {position:absolute; top:870px; right:40px; width:314px;}
	input#witnessName {position:absolute; top:990px; right:40px; width:314px;}
	div#minorSignature {position:absolute; top:724px; left:40px; width:630px; height:40px;}
	div#minorSignature canvas {width:630px; height:40px;}
	div#witnessSignature {position:absolute; top:913px; left:40px; width:630px; height:40px;}
	div#witnessSignature canvas {width:630px; height:40px;}
}
@media only screen and (device-width: 768px) and (orientation: portrait) {
	section.form {position:relative; background:url(/office/global/images/insurance/minorFormBG-mini.jpg) no-repeat; width:750px; height:971px; box-shadow:0px 0px 6px #636363; -webkit-box-shadow:0px 0px 6px #636363; -moz-box-shadow:0px 0px 6px #636363; -o-box-shadow:0px 0px 6px #636363; margin:0 auto;}
	input#location {position:absolute; top:96px; left:36px; width:430px;}
	input#dateSigned {position:absolute; top:96px; right:52px; width:214px;}
	input#minorName {position:absolute; top:642px; left:36px; width:430px;}
	input#minorDate {position:absolute; top:590px; right:28px; width:236px;}
	input#minorAge {position:absolute; top:642px; right:28px; width:236px;}
	input#witnessName {position:absolute; top:732px; right:28px; width:238px;}
	div#minorSignature {position:absolute; top:536px; left:0px; width:504px; height:40px;}
	div#minorSignature canvas {width:504px; height:40px;}
	div#witnessSignature {position:absolute; top:678px; left:0px; width:506px; height:40px;}
	div#witnessSignature canvas {width:506px; height:40px;}
}

</style>
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page">
   
   <!--CONTENT-->
   <div data-role="content">
	   
   		<section class="form">
	   		<form action="" method="post">
		   		<input type="text" name="location" id="location" />
		   		<input type="date" name="dateSigned" id="dateSigned" />
		   		<input type="text" name="minorName" id="minorName" />
		   		<input type="date" name="minorDate" id="minorDate" />
		   		<input type="number" name="minorAge" id="minorAge" />
		   		<input type="text" name="witnessName" id="witnessName" />
		   		<div id="minorSignature"></div>
		   		<div id="witnessSignature"></div>
	   		</form>
   		</section>
   		
   </div>
   <!--END CONTENT-->
   
<?php //echo classFooter("grouping",$c); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>