<?php
	
	$groupQuery = $mysqli->query("SELECT `voucher_id`,`company_name` FROM `LARX_voucher_companies` ORDER BY `company_name` ASC");

	if(!isset($_POST['addVoucherBulk'])) {
		echo '<h1>Voucher Bulk Uploader:</h1>';
	}
	
	if(isset($_POST['addVoucherBulk'])) {
		$group = makeSQLSafe($mysqli,$_POST['voucherGroup']);
		
		if($_FILES['voucherFile']['size'] > 0) {
			$fname = $_FILES['voucherFile']['name'];
			$chk_ext = explode(".",$fname);
			 
			 if(strtolower($chk_ext[1]) == "csv") {
			 
				 $filename = $_FILES['voucherFile']['tmp_name'];
				 $handle = fopen($filename, "r");
			
				 while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$data = str_replace("'","\'",$data);
					
					if($data[9] != "valid") {
						$status = 1;
					} else {
						$status = 0;
					}
					
					switch($data[10]) {
						case "Groupon":
							$source = "1";
							break;
						case "Living Social":
							$source = "10";
							break;
						case "Screamin' Deals":
							$source = "4";
							break;
						case "Travel Zoo":
							$source = "13";
							break;
						case "Travel Zoo Extras":
							$source = "13";
							break;
						case "SignOn San Diego":
							$source = "11";
							break;
						case "Deal Find":
							$source = "7";
							break;
						case "Deal Grind":
							$source = "8";
							break;
						case "L.A. Times/Deals":
							$source = "9";
							break;
						case "Crowd Savings":
							$source = "6";
							break;
						case "Buy With Me":
							$source = "5";
							break;
						case "The Daily Save":
							$source = "12";
							break;
						default:
							$source = "";
							break;
					}
					
						$checkCode = $mysqli->query("SELECT `voucher_number` FROM `LARX_voucher_codes` WHERE `voucher_number` = '$data[0]'");
						if($checkCode->num_rows == 0) {
							if($data[0] != NULL) {
								$mysqli->query("INSERT INTO `LARX_voucher_codes` (`voucher_id`,`voucher_number`,`redeem`,`buyer_name`,`address`,`city`,`state`,`zip_code`,`phone`,`email`,`timestamp_purchased`,`timestamp`)
								 VALUES ('$source','$data[0]','$status','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]','$data[8]',NOW())");
							}
						}

				 }
				 fclose($handle);
			 }    
		}
		
	}
	
if(!isset($_POST['addVoucherBulk'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<fieldset>
			<label>Voucher CSV File:</label>
			<input type="file" name="voucherFile" id="voucherFile" />

			<span style="color:#cb202a;">CSV Format:</span><br /><span style="color:#FFF;">Voucher Number, Customer Name, Address, City, State, Zip Code, Phone, Email Address, Date Purchased</span>
			<button type="submit">Upload</button>
		</fieldset>
		<input type="hidden" name="addVoucherBulk" />
	</form>
	
<?php } else {
		if($_FILES['voucherFile']['size'] > 0) {
			if($numCols == $acceptedNumCols) { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				Voucher Uploader Complete!<br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Add Additional Codes</a>
			</div>
		<?php } else { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				<span style="color:#cb202a; font-size:20px;">CSV was not formatted correctly. Please use this format:</span><br />
				<span style="font-size:16px;">Voucher Number, Customer Name, Address, City, State, Zip Code, Phone, Email Address, Date Purchased</span><br /><br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Back To Uploader</a>
			</div>
		<?php } ?>
	<?php } else { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				Please try again<br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Add Voucher Codes</a>
			</div>
	<?php }
	  }

$groupQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/vouchers.js"></script>