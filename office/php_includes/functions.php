<?php

date_default_timezone_set('America/Los_Angeles');

function footer($active=null) { ?>
	<div data-role="footer" data-position="fixed">
   		
	   <!--NAVIGATION-->
	   <div data-role="navbar" data-iconpos="top" data-inline="true">
		   <ul>
			   <li><a href="/office/racers/" data-icon="search" <?php if($active == "racers") echo 'class="ui-state-persist"'; ?> data-transition="flip">Racers</a></li>
			   <li><a href="/office/schedule/" data-icon="grid" <?php if($active == "scheduling") echo 'class="ui-state-persist"'; ?> data-transition="flip">Scheduling</a></li>
			   <li><a href="/office/transactions/" data-rel="dialog" data-icon="gear" <?php if($active == "transactions") echo 'class="ui-state-persist"'; ?> data-transition="pop">Transactions</a></li>
			   <li><a href="/office/passes/" data-icon="check" <?php if($active == "passes") echo 'class="ui-state-persist"'; ?> data-transition="flip">Race Passes</a></li>
			   <li><a href="/office/reporting/" data-icon="info" <?php if($active == "reporting") echo 'class="ui-state-persist"'; ?> data-transition="flip">Reporting</a></li>
		   </ul>
	   </div>
	   <!--END NAVIGATION-->
   	
   </div>
<?php
}

function classFooter($active=null,$hash=null) { ?>
	<div data-role="footer" data-position="fixed">
   		
	   <!--NAVIGATION-->
	   <div data-role="navbar" data-iconpos="top" data-inline="true">
		   <ul>
			   <li><a href="/office/class/<?php echo $hash; ?>" data-icon="search" <?php if($active == "checkin") echo 'class="ui-state-persist"'; ?> data-transition="flip">Check In</a></li>
			   <li><a href="/office/class/packages/<?php echo $hash; ?>" data-icon="search" <?php if($active == "packages") echo 'class="ui-state-persist"'; ?> data-transition="flip">Packages</a></li>
			   <li><a href="/office/class/grouping/<?php echo $hash; ?>" data-icon="grid" <?php if($active == "grouping") echo 'class="ui-state-persist"'; ?> data-transition="flip">Grouping</a></li>
			   <li><a href="/office/class/assign/<?php echo $hash; ?>" data-icon="gear" <?php if($active == "assign") echo 'class="ui-state-persist"'; ?> data-transition="flip">Assign Cars</a></li>
			   <li><a href="/office/class/alerts/<?php echo $hash; ?>" data-icon="alert" <?php if($active == "alerts") echo 'class="ui-state-persist"'; ?> data-transition="flip">Class Alerts</a></li>
		   </ul>
	   </div>
	   <!--END NAVIGATION-->
   	
   </div>
<?php
}


function passesFooter($active=null) { ?>
	<div data-role="footer" data-position="fixed">
   		
	   <!--NAVIGATION-->
	   <div data-role="navbar" data-iconpos="top" data-inline="true">
		   <ul>
			   <li><a href="/office/passes/" data-icon="gear" <?php if($active == "process") echo 'class="ui-state-persist"'; ?> data-transition="flip">Process Passes</a></li>
			   <li><a href="/office/passes/scheduled/" data-icon="star" <?php if($active == "scheduled") echo 'class="ui-state-persist"'; ?> data-transition="flip">Scheduled Passes</a></li>
			   <li><a href="/office/passes/active/" data-icon="star" <?php if($active == "active") echo 'class="ui-state-persist"'; ?> data-transition="flip">Active Passes</a></li>
			   <li><a href="/office/passes/void/" data-icon="delete" <?php if($active == "void") echo 'class="ui-state-persist"'; ?> data-transition="flip">Void Passes</a></li>
			   <li><a href="/office/passes/used/" data-icon="search" <?php if($active == "used") echo 'class="ui-state-persist"'; ?> data-transition="flip">Used Passes</a></li>
		   </ul>
	   </div>
	   <!--END NAVIGATION-->
   	
   </div>
<?php
}


function classGrouping($mysqli,$classID,$groupNum) {
	$groupQuery = $mysqli->query("SELECT * FROM `LARX_class_grouping` WHERE `class_id` = '$classID' LIMIT 1");
	if($groupQuery->num_rows == 1) {
		$group = $groupQuery->fetch_array();
		$groupData = str_replace(array("[","]"),'',$group['group_data']);
		$groupArray = explode(",",$groupData);
		foreach($groupArray as $value) {
			$array = explode("=>",$value);
			$groupName = trim($array[0]);
			$groupRacerID = trim($array[1]);
			if($groupName == $groupNum) {
				if($groupName !== "undefined") {
					$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$groupRacerID' LIMIT 1");
					if($racerQuery->num_rows == 1) {
						$racer = $racerQuery->fetch_array();
						if($racer['driver_name'] != NULL) $racerName = $racer['driver_name']; else $racerName = $racer['buyer_name'];
						echo '<li data-racer="'.$racer['id'].'">'.$racerName.'</li>';
					}
				}
			}
		}
		
	}
}


function makeSQLSafe($mysqli,$string) {
	$string = trim($string);
	while(strpos($string,"  ") !== false) {
		$string = str_replace("  "," ",$string);
	}
	if (get_magic_quotes_gpc()) $string = stripslashes($string);
	return $mysqli->real_escape_string($string);
}

?>