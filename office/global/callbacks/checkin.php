<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	$c = makeSQLSafe($mysqli,$_POST['c']);
	$r = makeSQLSafe($mysqli,$_POST['r']);
	$currentDateTime = date("Y-m-d h:i:s");

	//QUERY CLASS
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
	if($classQuery->num_rows == 1) {
		$class = $classQuery->fetch_array();
		$classID = $class['id'];
		
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$r' LIMIT 1");
		if($racerQuery->num_rows == 1) {
			$racer = $racerQuery->fetch_array();
			
			if($class['id'] == $racer['class_id']) {			
				if($racer['checked_in'] == 0) {
					//CHECK IN RACER
					$mysqli->query("UPDATE `LARX_race_passes` SET `pass_status`='used', `checked_in` = 1, `checkin_timestamp` = '$currentDateTime' WHERE `id` = '$r' LIMIT 1");
					//GROUP RACER BY TIMESTAMP
					$groupQuery = $mysqli->query("SELECT * FROM `LARX_class_grouping` WHERE `class_id` = '$classID' LIMIT 1");
					$rg = array();
					if($groupQuery->num_rows == 1) {
						$grouping = $groupQuery->fetch_assoc();
						//CHECK GROUPING ARRAYS
						$groupingArray = explode(",",str_replace(array("[","]"),"",$grouping['group_data']));
						foreach($groupingArray as $value) {
							if($value != "") {
								$formatGrouping = explode(" => ",$value);
								$groupNum = str_replace("group_","",$formatGrouping[0]);
								$groupRacer = $formatGrouping[1];
								array_push($rg,$groupNum);
							}
						}

						foreach(array_count_values($rg) as $k => $v) {
							if($v < 4) {
								//echo "Group #".$k." has ".$v." racers in it.<br />";
								$assignGroupNum = $k;
							} else {
								$assignGroupNum = $k + 1;
							}
						}
						
						$groupData = $grouping['group_data']."[group_".$assignGroupNum." => ".$racer['id']."],";
						$mysqli->query("UPDATE `LARX_class_grouping` SET `group_data` = '$groupData' WHERE `class_id` = '$classID' LIMIT 1");
						
					} else {
						$groupData = "[group_1 => ".$racer['id']."],";
						$mysqli->query("INSERT INTO `LARX_class_grouping` (`class_id`,`group_data`,`timestamp`) VALUES ('$classID','$groupData',NOW())");
					}
					//RETURN SUCCESS
					echo '{"response":"success"}';
				} else {					
					echo '{"response":"Racer already checked in."}';
				}
			}
			
		} else {
			echo '{"response":"Racer Not Found"}';
		}
		
	} else echo '{"response":"Class Not Found"}';
?>