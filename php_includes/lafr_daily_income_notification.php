<?php
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");

	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$today = date("Y-m-d");
	$income = $LAFundAdmin->generate_daily_income($today);
	$pass_total = array_sum($income['total']);
	$total = number_format($pass_total * 150);
	
	$to  = 'allan@211enterprises.com' . ', ';
	$to .= 'jim@211enterprises.com' . ', ';
	$to .= 'andra@211enterprises.com' . ', ';
	$to .= 'monica@laracing.com' . ', ';
	$to .= 'deanna@211enterprises.com' . ', ';
	$to .= 'laura@laracing.com';

	$message .= "LA Fund Racing Daily Income Breakdown:<br /><br />";
	$message .= '--------------------------------------------<br />';
	
	foreach($income['transactions'] as $val) {
		$message .= 'Organization: '.$val['name'].'<br />';
		$message .= 'Pass Quantity: '.$val['quantity'].'<br />';
		$message .= '--------------------------------------------<br />';
	}
	
	$message .= "<br />";
	$message .= "Daily Income Total: $".$total;
	
	$headers = 'From: noreply@laracingx.com' . "\r\n" .
    'Reply-To: noreply@laracingx.com' . "\r\n" .
    'Content-type: text/html; charset: utf8' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
	
	mail($to, "LA Fund Racing Daily Income - ".$today, $message, $headers);
?>