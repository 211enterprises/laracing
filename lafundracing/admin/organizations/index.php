<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->check_admin_access();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<div class="alert alert-warning" style="margin-top: 20px;">
			Total Sales To Date: <?php echo $LAFundAdmin->get_total_sales(); ?>
		</div>
		
		<h1 class="pull-left">Organizations</h1>
		<a href="<?php echo LAFR_ADMIN_ROOT.'/organizations/add.php'; ?>" class="btn btn-danger pull-right" style="margin-top: 30px;">Add Organization</a>
		
		<div class="clearfix"></div>
		
		<div class="panel-group" id="accordion">
<?php 
		foreach($LAFundRacing->get_active_fund_organizations() as $key => $value) {
			$division = $LAFundRacing->fund_divisions($value['id'], NULL, 1);
			// Get organization total sales
			$org_sales = [];
    	foreach($LAFundAdmin->get_organization_total_sales($value["id"]) as $trans) {
      	array_push($org_sales, $trans["pass_quantity"]);
    	}
    	// Panel partial
			include('../../partials/organization-panel.php');
		}
?>	  
		</div>		
	</div>	

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>