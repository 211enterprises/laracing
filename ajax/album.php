<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['a']) && is_numeric($_GET['a'])) {
		$album = makeSQLSafe($mysqli,$_GET['a']);
		
		$thumbsQuery = $mysqli->query("SELECT * FROM `LARX_albums`,`LARX_album_media` WHERE LARX_albums.album_id = LARX_album_media.album_id AND LARX_albums.album_id = '$album' ORDER BY LARX_album_media.order ASC");
		
		$imageQuery = $mysqli->query("SELECT * FROM `LARX_albums`,`LARX_album_media` WHERE LARX_albums.album_id = LARX_album_media.album_id AND LARX_albums.album_id = '$album' ORDER BY LARX_album_media.order ASC LIMIT 1");
		if($imageQuery->num_rows > 0) {
			$image = $imageQuery->fetch_assoc(); ?>
			<div class="imageContainer" style="display:block;">
				<h2><?php echo $image['album_name']; ?></h2>
				<div class="displayImage">
			
			<?php switch($image['media_source']) {
					case "youtube":
						echo '<iframe src="http://www.youtube.com/embed/'.$image['media_path'].'?autoplay=1"></iframe>';	
						break;
					case "vimeo":
						echo '<iframe src="http://player.vimeo.com/video/'.$image['media_path'].'?autoplay=1"></iframe>';
						break;
					case "image":
						echo '<img src="'.$image['media_path'].'" alt="" />';
						break;
					default:
						echo '<img src="'.$image['media_path'].'" alt="" />';
						break;
				  } ?>
					
				</div>
			</div>
			<div class="selectImageContainer" style="display:block;">
				
				<div class="showGalleryThumbs">
					<ul>
			<?php while($thumb = $thumbsQuery->fetch_assoc()) {
					switch($thumb['media_source']) {
						case "youtube":
							echo '<li id="mediaGallery'.$thumb['id'].'"><img src="http://img.youtube.com/vi/'.$thumb['media_path'].'/0.jpg" alt="" /></li>';
							break;
						case "vimeo":
							echo '<li id="mediaGallery'.$thumb['id'].'">'.getVimeoThumb($thumb['media_path'],"medium").'</li>';
							break;
						case "image":
							echo '<li id="mediaGallery'.$thumb['id'].'"><img src="'.$thumb['media_path'].'" alt="" /></li>';
							break;
						default:
							echo '<li id="mediaGallery'.$thumb['id'].'"><img src="'.$thumb['media_path'].'" alt="" /></li>';
							break;
					  }
				  } ?>
					</ul>
				</div>
				
				<button type="button" class="ui-galleryBtn-left" data-slider="prev">Prev</button>
				<button type="button" class="ui-galleryBtn-right" data-slider="next">Next</button>
			</div>
<?php
		} else {
			echo '<div class="voucherPopUp" style="display:block; z-index:503;">Media coming soon.</div>';
		}
	} else {
		echo '<div class="voucherPopUp" style="display:block; z-index:503;">No album found. Please try again later.</div>';
	}
	
?>