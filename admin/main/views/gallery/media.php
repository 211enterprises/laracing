<?php

	$id = makeSQLSafe($mysqli,$_GET['id']);
	$albumQuery = $mysqli->query("SELECT * FROM `LARX_albums` WHERE `album_id` = '$id' LIMIT 1");
	if($albumQuery->num_rows == 1) {
		$album = $albumQuery->fetch_array();
		
		//QUERY MEDIA
		$mediaQuery = $mysqli->query("SELECT * FROM `LARX_album_media` WHERE `album_id` = '$id' ORDER BY `order` ASC");
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Album Media Can\'t Be Found</div>';
	}
	
	
?>

	<h1>Album Media: <small><?php echo $album['album_name']; ?></small></h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=gallery&action=addmedia" class="addContent">Add Media</a>
	
<?php if($mediaQuery->num_rows > 0) { ?>
	<ul class="media">
<?php while($media = $mediaQuery->fetch_assoc()) { ?>
		<li id="mediaOrder_<?php echo $media['id']; ?>">
			<div class="mediaImage">
	<?php switch($media['media_source']) {
			case "youtube";
				echo '<img src="http://img.youtube.com/vi/'.$media['media_path'].'/0.jpg" alt="" />';
				break;
			case "vimeo":
				echo getVimeoThumb($media['media_path'],"medium");
				break;
			case "image":
				echo '<img src="'.$media['media_path'].'" alt="" />';
				break;
			default:
				echo '<img src="'.$media['media_path'].'" alt="" />';
				break;
		} ?>
			</div>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Album Media</div>';

$albumQuery->close();
$mysqli->close(); ?>
<script type="text/javascript" src="/media/js/admin/gallery.js"></script>