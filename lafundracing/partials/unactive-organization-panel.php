<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#organization<?php echo $value['id']; ?>">
        <?php echo $value['name']; ?>
      </a>
    </h4>
  </div>
  <div id="organization<?php echo $value['id']; ?>" class="panel-collapse collapse">
    <div class="panel-body">
			<div class="row">
				<div class="col-md-4">
		      Email: <?php echo $value['email']; ?><br />
				</div>
				<div class="col-md-4">
		      Address: <?php echo $value['address']; ?><br />
				</div>
				<div class="col-md-4">
		      City: <?php echo $value['city']; ?><br />
				</div>
				<div class="col-md-4">
		      State: <?php echo $value['state']; ?><br />
				</div>
				<div class="col-md-4">
		      Zip Code: <?php echo $value['zip_code']; ?><br />
				</div>
				<div class="col-md-4">
		      Phone: <?php echo $value['phone']; ?><br />
				</div>
				<div class="col-md-4">
		      EIN: <?php echo $value['EIN']; ?><br />
				</div>
				<div class="col-md-4">
		      Non-Profit: <?php echo ($value['non-profit'] == 1) ? 'Yes' : 'No'; ?><br />
				</div>
			</div>
      <hr />
      
      <div class="row" style="margin-top: 30px;">
      	<div class="col-md-6">
      		<a href="" class="btn btn-block btn-primary approve-organization" data-id="<?php echo $value['id']; ?>">Approve Organization</a>
      	</div>
      	<div class="col-md-6">
      		<a href="" class="btn btn-block btn-primary delete-organization" data-id="<?php echo $value['id']; ?>">Delete Organization</a>
      	</div>
      </div>
    </div>
  </div>
</div> 