<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$id' LIMIT 1");
	if($passQuery->num_rows == 1) {
		$pass = $passQuery->fetch_array();
		$passID = $pass['id'];
				
		if(!isset($_POST['assignRacer'])) {
			echo '<h1>Assign Racer:</h1>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer Can\'t Be Found</div>';
	}
	
	if(isset($_POST['assignRacer'])) {
		$racerName = makeSQLSafe($mysqli,$_POST['racerName']);
		$racerVideo = makeSQLSafe($mysqli,$_POST['raceVideo']);
		$assignClass = makeSQLSafe($mysqli,$_POST['assignClass']);
		$racerNotes = makeSQLSafe($mysqli,$_POST['racerNotes']);
		$assignRacer = makeSQLSafe($mysqli,$_POST['assignRacer']);
		
		$mysqli->query("UPDATE `LARX_race_passes` SET `class_id`='$assignClass', `race_video`='$racerVideo' WHERE `id` = '$assignRacer' LIMIT 1");
		$mysqli->query("INSERT INTO `LARX_class_roster` (`class_id`,`racer_name`,`pass_id`,`racer_video`,`racer_notes`,`timestamp`) VALUES ('$assignClass','$racerName','$assignRacer','$racerVideo','$racerNotes',NOW())");
		
		$checkClassQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$assignClass'");
		$checkClass = $checkClassQuery->fetch_assoc();
		$checkClassTaken = $checkClass['slots_taken'] + 1;
		
		//UPDATE SLOTS
		$mysqli->query("UPDATE `LARX_class_dates` SET `slots_taken` = '$checkClassTaken' WHERE `id` = '$assignClass' LIMIT 1");
	} else {
		$today = date("Y-m-d");
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `date` > '$today' ORDER BY `date` ASC");
	}
?>

<?php 
if(!isset($_POST['assignRacer'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>Racer Name:</label>
			<input type="text" name="racerName" id="racerName" value="<?php if($pass['driver_name'] != "") echo $pass['driver_name']; else echo $pass['buyer_name']; ?>" />
			
			<label>Pass Number:</label>
			<input type="text" name="passNumber" id="passNumber" readonly="readonly" value="<?php echo $pass['pass_number']; ?>" />
			
			<label>Race Video Purchased</label>
			<select name="raceVideo" id="raceVideo">
				<option value="0">Select:</option>
				<option value="1" <?php if($pass['race_video'] == 1) echo 'selected="selected"'; ?>>Yes</option>
				<option value="0" <?php if($pass['race_video'] == 0) echo 'selected="selected"'; ?>>No</option>
			</select>
			
			<label>Assigned Class:</label>
			<select name="assignClass" id="assignClass">
				
				<option value="">Select:</option>
	<?php if($classQuery->num_rows > 0) {
			while($class = $classQuery->fetch_assoc()) {
				$classSpots = $class['class_limit'] - $class['slots_taken'];
				if($classSpots > 0) {
					echo '<option value="'.$class['id'].'">'.date("m-d-Y",strtotime($class['date'])).' - '.date("h:i A",strtotime($class['time'])).'</option>';
				}
			}
		  } ?>
				
			</select>
			
			<label>Racer Notes:</label>
			<textarea name="racerNotes" id="racerNotes"></textarea>
			
			<button type="submit">Assign Racer</button>
		</fieldset>
		<input type="hidden" name="assignRacer" value="<?php echo $pass['id']; ?>" />
	</form>	
<?php
} else {
	echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer Has Been Assigned</div>';
}


$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/racer.js"></script>
