<?php
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$faqQuery = $mysqli->query("SELECT * FROM `LARX_faq` WHERE `id` = '$id' LIMIT 1");
	if($faqQuery->num_rows == 1) {
		$faq = $faqQuery->fetch_array();
		
		if(!isset($_POST['updateFAQ'])) {
			echo '<h1>Edit FAQ:</h1>';
			echo '<a href="/admin/main/faq/delete/'.$faq['id'].'" class="deleteContent">Delete This FAQ</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">FAQ Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateFAQ'])) {
		$faqID = makeSQLSafe($mysqli,$_POST['updateFAQ']);
		$question = makeSQLSafe($mysqli,$_POST['question']);
		$answer = makeSQLSafe($mysqli,$_POST['answer']);
		
		$mysqli->query("UPDATE `LARX_faq` SET `question` = '$question', `answer` = '$answer' WHERE `id` = '$faqID' LIMIT 1");
		//header("Location: /admin/main/histor");
		//exit();
	} ?>

<?php if(!isset($_POST['updateFAQ'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>Question:</label>
			<input type="text" name="question" id="question" value="<?php echo $faq['question']; ?>" />
			<label>Answer:</label>
			<textarea name="answer" id="answer"><?php echo $faq['answer']; ?></textarea>
			<button type="submit">Update FAQ</button>
		</fieldset>
		<input type="hidden" name="updateFAQ" value="<?php echo $faq['id']; ?>" />
	</form>	
<?php
	} else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">FAQ Has Been Updated</div>';

$faqQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/faq.js"></script>