<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$racerQuery = $mysqli->query("SELECT * FROM `LARX_class_roster` WHERE `id` = '$id' LIMIT 1");
	$racerFound = $racerQuery->num_rows;
	if($racerFound == 1) {
		$racer = $racerQuery->fetch_array();
		$passID = $racer['pass_id'];
		
		$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$passID' LIMIT 1");
		$race_pass = $passQuery->fetch_array();
		
		if(!isset($_POST['updateRacer'])) {
			echo '<h1>Edit Racer:</h1>';
			echo '<a href="/admin/main/classes/deleteracer/'.$racer['id'].'" class="deleteContent">Delete This Racer</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateRacer'])) {
		$racerName = makeSQLSafe($mysqli,$_POST['racerName']);
		$racerVideo = makeSQLSafe($mysqli,$_POST['raceVideo']);
		$racerNotes = makeSQLSafe($mysqli,$_POST['racerNotes']);
		
		$mysqli->query("UPDATE `LARX_class_roster` SET `racer_name`='$racerName', `racer_video`='$racerVideo', `racer_notes`='$racerNotes' WHERE `id` = '$id' LIMIT 1");

	}
	
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '".$racer['class_id']."' LIMIT 1");
	$class = $classQuery->fetch_assoc();
	
?>

<?php 
if($racerFound == 1) {
if(!isset($_POST['updateRacer'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>Racer Name:</label>
			<input type="text" name="racerName" id="racerName" value="<?php echo $racer['racer_name']; ?>" />
			
			<label>Pass Number:</label>
			<input type="text" name="passNumber" id="passNumber" readonly="readonly" value="<?php echo $race_pass['pass_number']; ?>" />
			
			<label>Race Video Purchased</label>
			<select name="raceVideo" id="raceVideo">
				<option value="0">Select:</option>
				<option value="1" <?php if($racer['racer_video'] == 1) echo 'selected="selected"'; ?>>Yes</option>
				<option value="0" <?php if($racer['racer_video'] == 0) echo 'selected="selected"'; ?>>No</option>
			</select>
			
			<label>Assigned Class:</label>
			<input type="text" name="class" id="class" readonly="readonly" value="<?php echo $class['date']." - ".date("h:i A",strtotime($class['time'])); ?>" />
			
			<label>Racer Notes:</label>
			<textarea name="racerNotes" id="racerNotes"><?php echo $racer['racer_notes']; ?></textarea>
			
			<button type="submit">Edit Racer</button>
		</fieldset>
		<input type="hidden" name="updateRacer" value="<?php echo $roster['id']; ?>" />
	</form>	
<?php
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer Has Been Updated</div>';
	}
}

$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/racer.js"></script>
