<div class="row-fluid">
	<div class="col-md-12">
		<h3>Organization Information:</h3>
	</div>
	<div class="col-md-6">
		Name: <span><?php echo ($organization['name'] != NULL) ? $organization['name'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Email Address: <span><?php echo ($organization['email'] != NULL) ? $organization['email'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Address: <span><?php echo ($organization['address'] != NULL) ? $organization['address'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		City: <span><?php echo ($organization['city'] != NULL) ? $organization['city'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		State: <span><?php echo ($organization['state'] != NULL) ? $organization['state'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Zip Code: <span><?php echo ($organization['zip_code'] != NULL) ? $organization['zip_code'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Phone: <span><?php echo ($organization['phone'] != NULL) ? $organization['phone'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Representative Name: <span><?php echo ($organization['rep_name'] != NULL) ? $organization['rep_name'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		EIN Number: <span><?php echo ($organization['EIN'] != NULL) ? $organization['EIN'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Non Profit: <span><?php echo ($organization['non-profit'] == 1) ? 'Yes' : 'No'; ?></span>
	</div>
	<div class="col-md-12">
		Active: <span><?php echo ($organization['active'] == 1) ? 'Yes' : 'No'; ?></span>
	</div>
</div>