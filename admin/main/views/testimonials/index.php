<?php
	$testQuery = $mysqli->query("SELECT * FROM `LARX_testimonials` ORDER BY `order` ASC");
?>

	<h1>Client Testimonials</h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=testimonials&action=add" class="addContent">Add a Testimonial</a>
	
<?php if($testQuery->num_rows > 0) { ?>
	<ul class="testimonials">
<?php while($test = $testQuery->fetch_assoc()) { ?>
		<li id="testOrder_<?php echo $test['id']; ?>">
			<a href="<?php echo ADMIN_ROOT; ?>/?controller=testimonials&action=edit&id=<?php echo $test['id']; ?>">
				<div class="clientName"><?php echo $test['client_name']; ?></div>
				<div class="location"><?php echo $test['client_location']; ?></div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Testimonials</div>';

$testQuery->close();
$mysqli->close(); ?>
<script type="text/javascript" src="/media/js/admin/testimonials.js"></script>