<?php

	class Payments {
		
		function __construct($connection, $test = 0) {
			$this->db = $connection;
			$this->test = $test;
			if($test) {
				//Development Keys
				$this->application_login = 'lafundracing.laracingx.com';
				$this->connection_ticket = 'SDK-TGT-57-iCBa2bW4fR7ertFyLj6laQ';
			} else {
				//Production Keys
				$this->application_login = 'lafundracingprod.laracingx.com';
				$this->connection_ticket = 'SDK-TGT-78-6hEt3M1EP2xGUP_MkQU$dQ';	
			}
		}	
		
		public function authorize_capture($connection, $data) {
			$name = makeSQLSafe($connection, $data['firstName']) .' '. makeSQLSafe($connection, $data['lastName']);
			$number = makeSQLSafe($connection, $data['cardNum']);
			$expY = makeSQLSafe($connection, $data['ExpY']);
			$expM = makeSQLSafe($connection, $data['ExpM']);
			$cvv = makeSQLSafe($connection, $data['cardCVV2']);
      $quantity = makeSQLSafe($connection, $data['quantity']);
			$amount = $quantity * 150;
			
			return $this->capture($name, $number, $expY, $expM, $cvv, $amount);
		}
		
		protected function capture($name, $number, $expY, $expM, $cvv, $amount) {
			// Include the QuickBooks files
			require_once($_SERVER['DOCUMENT_ROOT'].'/quickbooks/QuickBooks.php');
			
			$dsn = null;
			$path_to_private_key_and_certificate = null;
			$status_array = Array();
			
			// Create an instance of the MerchantService object 
			$MS = new QuickBooks_MerchantService($dsn, $path_to_private_key_and_certificate, $this->application_login, $this->connection_ticket);
			// If you're using a Intuit QBMS development account, you must set this to true! 
			if($this->test) {
				$MS->useTestEnvironment(true);
			} else {
				$MS->useTestEnvironment(false);
			}
			// If you want to see the full XML input/output, you can turn on debug mode
			$MS->useDebugMode(false);
			
			// null vars

			$address = null;
			$postalcode = null;
			
			// Create the CreditCard object
			$Card = new QuickBooks_MerchantService_CreditCard($name, $number, $expY, $expM, $address, $postalcode, $cvv);
			
			if($Transaction = $MS->authorize($Card, $amount)) {
				//print('Card authorized!' . "\n");
				//print_r($Transaction);	
				
				// Get the transaction as a string which can later be turned back into a transaction object
				$str = $Transaction->serialize(); 
				//print('Serialized transaction: ' . $str . "\n\n");
				
				// Now convert it back to a transaction object
				$Transaction = QuickBooks_MerchantService_Transaction::unserialize($str);
				
				// ... maybe you'd rather convert it to an array? 
				$arr = $Transaction->toArray();
				//print('Array transaction: '); 
				//print_r($arr);
				//print("\n\n");
				
				// ... and back again?
				$Transaction = QuickBooks_MerchantService_Transaction::fromArray($arr);
				
				// ... or an XML document?
				$xml = $Transaction->toXML();
				//print('XML transaction: ' . $xml . "\n\n");
				
				// ... and back again? 
				$Transaction = QuickBooks_MerchantService_Transaction::fromXML($xml);
				
				// How about XML that can be used in a qbXML SalesReceiptAdd request?
				$qbxml = $Transaction->toQBXML();
				//print('qbXML transaction info: ' . $qbxml . "\n\n");
				
				// Now that that card has been authorized, let's actually capture the funds. 
				// 
				// You can just pass in the transaction if you want to capture for the same 
				// amount as the authorization. Alternatively, you can pass in a different 
				// amount *less than* the authorization amount to only capture a portion of 
				// the authorization.
				// 	
				// If you want to capture more than the authorization was for, use charge(). 
				
				if ($Transaction = $MS->capture($Transaction, $amount)) {					
					$qbxml = $Transaction->toQBXML();
					
					$status_array['status'] = 1;
				} else {
					$status_array['status'] = 0;
					$status_array['message'] = $MS->errorMessage();
				}
			} else {
				$status_array['status'] = 0;
				$status_array['message'] = $MS->errorMessage();
			}
			
			return $status_array;
		}
	}

?>