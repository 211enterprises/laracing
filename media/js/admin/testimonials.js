
	//DELETE
	$('a.deleteContent').live("click",function() {
		if(confirm("Are you sure you want to delete this testimonial?")) {
			return true;
		}
		return false;
	});
	
	//ASSIGN ORDER
	$(function() {
		$("ul.testimonials").sortable({ opacity: 1.0, cursor: 'move', update: function() {
				var testOrder = $(this).sortable("serialize") + '&action=newTestOrder';
				$.post("/admin/main/ajax/testimonialOrder.php", testOrder, function(theResponse){ });
			}
		});
	});
	
	//SHOW/HIDE OPTIONS
	$('select[name=testimonialType]').live("change",function() {
		var testimonalType = $(this).val();
		if(testimonalType == 1) {
			$('div#videoTestimonial').show();
			$('div#writtenTestimonial').hide();
		} else {
			$('div#videoTestimonial').hide();
			$('div#writtenTestimonial').show();
		}
	});

	
	$('select[name=videoSource]').live("change",function() {
		var videoSource = $(this).val();
		switch(videoSource) {
			case "youtube":
				$('span#videoSample').html("http://www.youtube.com/watch?v=<span>12345678</span>");
				break;
			case "vimeo":
				$('span#videoSample').html("http://vimeo.com/<span>12345678</span>");
				break;
			default:
				$('span#videoSample').html("http://www.youtube.com/watch?v=<span>12345678</span>");
				break;
		}
	});