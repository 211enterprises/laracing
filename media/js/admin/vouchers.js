
	//DELETE
	$('a.deleteContent').live("click",function() {
		if(confirm("Are you sure you want to delete this voucher group?")) {
			return true;
		}
		return false;
	});
	
	
	//ADD GROUP URL
	$('input[name="voucherName"]').live("keyup",function() {
		var voucherGroupName = $(this).val(),
			formatGroupURL = voucherGroupName.toLowerCase().replace(/[^a-zA-Z0-9]/g,"-");
		$('input[name=voucherURL]').val(formatGroupURL);
	});