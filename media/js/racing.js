
	
	//INPUT OPACITY
	$('input,select,textarea').live("focus", function() {
		$(this).animate({
			opacity: "0.5"
		},200);
	});
	$('input,select,textarea').live("blur", function() {
		$(this).animate({
			opacity: "0.3"
		},200);
	});
	
	//ADD ADDITIONAL PASSES
	$('a.addPasses').live("click",function() {
		var currentPasses = $('ul.passes li').index() + 1,
			classHash = this.id,
			newCurrentPasses = $('ul.passes li').index() + 2,
			newPassesRemaining = $('div.passesRemaining span').html() - 1,
			passesRemaining = Number($('div.passesRemaining span').html()) - 1;
			
			if(classHash != "nonDatePurchase") {
				$.ajax({
					url: "/ajax/addPass.php",
					data: "n="+currentPasses+"&h="+classHash,
					success: function(data) {
						$('ul.passes').append(data);
							if(newPassesRemaining >= 0) {
								$('span#passNum').html(newCurrentPasses);
								$('input[name=totalPasses]').val(newCurrentPasses);
								$('div.passesRemaining span').html(passesRemaining);
							}
					}
				});
			} else {
				$.ajax({
					url: "/ajax/addPass.php",
					data: "n="+currentPasses+"&h="+classHash,
					success: function(data) {
						$('ul.passes').append(data);						
						$('span#passNum').html(newCurrentPasses);
						$('input[name=totalPasses]').val(newCurrentPasses);
					}
				});
			}
		
		return false;
	});
	
	//SELECT RACE PACKAGES
	var countPackages = function() {
		var newCurrentPasses = Number($('span#videoNum').html()),
			orderTotal = Number($('div.cartTotal span#orderTotal').html()),
			newPackageTotal = 0,
			newVideoNum = 0,
			newVideoTotal = 0;
			
		
		$('select[id^=racePackage]').each(function() {
			if($(this).val() != "") {
				newPackageTotal += Number($(this).val().split("-")[1]);
			} else {
				newPackageTotal += 0;
			}
		});
		
		$('select[id^=raceVideo]').each(function() {
			if($(this).val() == 1) {
				newVideoTotal += 99;
				newCurrentPasses += 1;
				newVideoNum++;
			} else {
				newVideoTotal += 0;
			}
		});
		
		$('span#videoNum').html(newVideoNum);
		$('div.cartTotal span#orderTotal').html(Number(newPackageTotal) + Number(newVideoTotal));
		
		//SET INPUT VALUE
		$('input[name=orderTotal]').val(Number(newPackageTotal) + Number(newVideoTotal));

	}
	
	
	$('select[id^=racePackage], select[id^=raceVideo], a.removePassBtn').live("change",function() {
		countPackages();
	});
	
	
	//VALIDATE
	var validateFields = function() {
		var billingName = $('input[name=name]'),
			billingEmail = $('input[name=email]'),
			billingPhone = $('input[name=phone]'),
			billingAddress = $('input[name=address]'),
			billingCity = $('input[name=city]'),
			billingState = $('input[name=state]'),
			billingZip = $('input[name=zip_code]'),
			billingCardNum = $('input[name=billingCardNum]'),
			billingCardCode = $('input[name=billingCardCode]'),
			billingCardExpMonth = $('select[name=billingCardExpMonth]'),
			billingCardExpYear = $('select[name=billingCardExpYear]'),
			billingCardType = $('select[name=billingCardType]'),
			acceptContract = $('input[name=agree]');
			
		if(billingName.val() != "" && billingEmail.val() != "" && billingPhone.val() != "" && billingAddress.val() != "" && billingCity.val() != "" && billingState.val() != "" && billingZip.val() != "" &&
		billingCardNum.val() != "" && billingCardCode.val() != "" && billingCardExpMonth.val() != "" && billingCardExpYear.val() != "" && billingCardType.val() != "" && acceptContract.val() != "") {
			return true;
		} else {
			return false;
		}
	}
	
	
	//COUNT VIDEOS
	$('select[id^=raceVideo]').live("change",function() {
		var videoCount = 0;
		$('select[id^=raceVideo]').each(function() {
			if($(this).val() == 1) {
				videoCount++;
			}
		});
		
		$('input[name=totalVideos]').val(videoCount);
		
	});
	
	
	//DELETE PASS
	$('a.removePassBtn').live("click",function() {
		var passParent = $(this).parent().get(0),
			removePass = $('ul.passes li').index(passParent),
			passCount = $('ul.passes li').index(),
			passesRemaining = Number($('div.passesRemaining span').html()) + 1,
			passNumCount = 1,
			videoCount = 0;
		
		$(passParent).remove();
		$('span#passNum').html(passCount);
		
		$('ul.passes li').each(function() {
			
			var newPassNum = $(this).find("span").html() - 1;
			if(newPassNum > 1) {
				$(this).find("span").html(newPassNum)
			}
				
			$('input[name=totalPasses]').val(Number(passCount));

			passNumCount++;
		});
		
		countPackages();
		
		$('select[id^=raceVideo]').each(function() {
			if($(this).val() == 1) {
				videoCount++;
			}
		});
		
		$('div.passesRemaining span').html(passesRemaining);
		$('input[name=totalVideos]').val(videoCount);
		
		return false;
	});
	
	//AGREE TO TERMS
	$('input#agree').live("click",function() {
		if($(this).is(':checked')) {
			$('button.disabled').css("opacity","1.0");
			$('button.disabled').css("cursor","pointer");
			$('button.disabled').removeAttr("disabled");
		} else {
			$('button.disabled').css("opacity","0.6");
			$('button.disabled').css("cursor","default");
			$('button.disabled').attr("disabled","disabled");
		}
		
	});
	
	
	//NUMBERS ONLY		
	$(function() {
	
		$("input[name=billingCardNum],input[name=billingCardCode]").keydown(function(event) {  
          // Allow: backspace, delete, tab and escape
	        if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            if( event.shiftKey|| (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) 
	            {
	       event.preventDefault(); 
	            }
	        }
	    });
	
	});
	
	//VALIDATE FIELDS
	$('button.disabled').live("click",function() {
		var billingFirstName = $('input[name=firstName]'),
			billingLastName = $('input[name=lastName]'),
			billingEmail = $('input[name=email]'),
			billingPhone = $('input[name=phone]'),
			billingAddress = $('input[name=address]'),
			billingCity = $('input[name=city]'),
			billingState = $('input[name=state]'),
			billingZip = $('input[name=zip_code]'),
			billingCardNum = $('input[name=billingCardNum]'),
			billingCardCode = $('input[name=billingCardCode]'),
			billingCardExpMonth = $('select[name=billingCardExpMonth]'),
			billingCardExpYear = $('select[name=billingCardExpYear]'),
			billingCardType = $('select[name=billingCardType]'),
			acceptContract = $('input[name=agree]'),
			valiOrderTotal = $('div.cartTotal span#orderTotal').html(),
			emptyCount = 0;
			
			if(billingFirstName.val() == "") { billingFirstName.css("background","red"); billingFirstName.css("color","black"); } else { billingFirstName.css("background","black"); billingFirstName.css("color","#919191"); }
			if(billingLastName.val() == "") { billingLastName.css("background","red"); billingLastName.css("color","black"); } else { billingLastName.css("background","black"); billingLastName.css("color","#919191"); }
			if(billingEmail.val() == "") { billingEmail.css("background","red"); billingEmail.css("color","black"); } else { billingEmail.css("background","black"); billingEmail.css("color","#919191"); }
			if(billingPhone.val() == "") { billingPhone.css("background","red"); billingPhone.css("color","black"); } else { billingPhone.css("background","black"); billingPhone.css("color","#919191"); }
			if(billingAddress.val() == "") { billingAddress.css("background","red"); billingAddress.css("color","black"); } else { billingAddress.css("background","black"); billingAddress.css("color","#919191"); }
			if(billingCity.val() == "") { billingCity.css("background","red"); billingCity.css("color","black"); } else { billingCity.css("background","black"); billingCity.css("color","#919191"); }
			if(billingState.val() == "") { billingState.css("background","red"); billingState.css("color","black"); } else { billingState.css("background","black"); billingState.css("color","#919191"); }
			if(billingZip.val() == "") { billingZip.css("background","red"); billingZip.css("color","black"); } else { billingZip.css("background","black"); billingZip.css("color","#919191"); }
			
			if(billingCardNum.val() == "") { billingCardNum.css("background","red"); billingCardNum.css("color","black"); } else { billingCardNum.css("background","black"); billingCardNum.css("color","#919191"); }
			if(billingCardCode.val() == "") { billingCardCode.css("background","red"); billingCardCode.css("color","black"); } else { billingCardCode.css("background","black"); billingCardCode.css("color","#919191"); }
			if(billingCardExpMonth.val() == "") { billingCardExpMonth.css("background","red"); billingCardExpMonth.css("color","black"); } else { billingCardExpMonth.css("background","black"); billingCardExpMonth.css("color","#919191"); }
			if(billingCardExpYear.val() == "") { billingCardExpYear.css("background","red"); billingCardExpYear.css("color","black"); } else { billingCardExpYear.css("background","black"); billingCardExpYear.css("color","#919191"); }
			if(billingCardType.val() == "") { billingCardType.css("background","red"); billingCardType.css("color","black"); } else { billingCardType.css("background","black"); billingCardType.css("color","#919191"); }
			
			$('select[id^=racePackage]').each(function() {
				if($(this).val() == "") {
					$(this).css("background","red"); $(this).css("color","black");
					emptyCount++;
				} else {
					$(this).css("background","black"); $(this).css("color","#919191");
					//emptyCount--;
				}
			});
			
			if(emptyCount > 0) {
				$('div.voucherPopUp').html("Please fill in all the required fields.");
				$('div.voucherPopUp').fadeIn();
				$('div.voucherPopUp').delay(1000).fadeOut();
			}
			
			if(billingFirstName.val() != "" && billingLastName.val() != "" && billingEmail.val() != "" && billingPhone.val() != "" && billingAddress.val() != "" && billingCity.val() != "" && billingState.val() != "" && billingZip.val() != "" &&
			billingCardNum.val() != "" && billingCardCode.val() != "" && billingCardExpMonth.val() != "" && billingCardExpYear.val() != "" && billingCardType.val() != "" && acceptContract.val() != "") {
				if(emptyCount == 0) {
					$.ajax({
						type: "GET",
						async: false,
						url: "/startracing/validate.php",
						data: "num="+billingCardNum.val()+"&exp="+billingCardExpMonth.val()+billingCardExpYear.val()+"&code="+billingCardCode.val()+"&amount="+valiOrderTotal,
						success: function(data) {
							if(data == "1") {
									$('#passForm').submit();
							} else if(data == "2") { 
								$('div.voucherPopUp').html("This transaction has been declined. Please try a different card.");
								$('div.voucherPopUp').fadeIn();
								$('div.voucherPopUp').delay(2000).fadeOut();
							} else if(data == "3") {

								$('div.voucherPopUp').html("There has been an error processing this transaction. Please try again later.");
								$('div.voucherPopUp').fadeIn();
								$('div.voucherPopUp').delay(2000).fadeOut();
							} else if(data == "4") {
								$('div.voucherPopUp').html("This transaction is being held for review.. We will contact you asap.");
								$('div.voucherPopUp').fadeIn();
								$('div.voucherPopUp').delay(2000).fadeOut();
							}
						},
						error: function() {
							$('div.voucherPopUp').html("There has been an error processing this transaction. Please try again later.");
							$('div.voucherPopUp').fadeIn();
							$('div.voucherPopUp').delay(1000).fadeOut();
						}	
					});
					//return true;
				}
			} else {
				return false;
			}
			
		return false;
	});
	
	//AGREE LINKS
	$('a#larxpolicies').live("click",function() {
		window.open('http://laracingx.acmediadesigns.net/policies/window.php','','width=1000,height=700,toolbar=0');
		return false;
	});
	
	$(function() {
		$('div.voucherPopUp').delay(1000).fadeOut();
	});
	
	
	
