/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/		
		
		//MOVING NAVIGATION
		$(function() {
			$(window).on('scroll', function() {
				var scrollPosition = $(document).scrollTop();
				if(scrollPosition > 124) {
					$('header section nav.mainNav').css("position","fixed").css("top","4px");
				} else {
					$('header section nav.mainNav').css("position","relative").css("top","0px");
				}
			});
		});
		
		
		//CALENDAR BOOKING
		$(document).ready(function() {
		
			$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
				var showCalDate = this.id;
				$.ajax({
					url: "/ajax/calendar.php",
					data: "d="+showCalDate,
					success: function(data) {
						$('div.calendarContainer').html("");
						$('div.calendarContainer').append(data);
					}
				});
				return false;
			});
		
		});
		
		
		//INPUT OPACITY
		$('input,textarea').live("focus", function() {
			$(this).animate({
				opacity: "0.5"
			},200);
		});
		$('input,textarea').live("blur", function() {
			$(this).animate({
				opacity: "0.3"
			},200);
		});
		
		function validateVoucher(input,voucherSrc,voucherNum) {
			$.ajax({
				url: "/ajax/validateVoucher.php",
				data: "c="+voucherSrc+"&v="+voucherNum,
				success: function(data) {
					if(data == "1") {
						if(voucherNum != "") {
							input.css("background","green");
							input.css("color","black");
							$('button#submitVoucher').removeAttr("onclick");
						} else {
							input.css("background","black");
							input.css("color","#919191");
							$('button#submitVoucher').attr("onclick","return false;");
						}
					} else {
						if(voucherNum != "") {
							input.css("background","red");
							input.css("color","black");
							$('button#submitVoucher').attr("onclick","return false;");
							$('div.voucherPopUp').html("");
							$('div.voucherPopUp').html("Please enter a valid voucher number. If you believe this is an error. Please contact us via phone. 877-901-RACE");
							$('div.voucherPopUp').fadeIn(300);
							$('div.voucherPopUp').delay(3000).fadeOut(300);
						} else {
							input.css("background","black");
							input.css("color","#919191");
							$('button#submitVoucher').attr("onclick","return false;");
						}
					}
				},
				error: function() {
					alert("An error has occuried. Please try again later.");
					$('button#submitVoucher').attr("onclick","return false;");
				}
			});
		}
		
		//VALIDATE VOUCHER NUMBERS
		$(function() {
			$('fieldset.voucherWrap input').live("change",function() {
				var voucherSrc = $(this).attr("data-voucher");
				var voucherNum = $(this).val();
				if(this.id.substr(0, 10) == "voucherNum") {
					validateVoucher($(this),voucherSrc,voucherNum)
				}
			});
			return false;
		});
		
		
		$(function() {
	
			//CALENDAR SHOW DETAILS
			var mouseX, mouseY;
			var classInfo = $('div.calendarClassInfo'); 
			
			$('td.available').live("mouseenter", function() {
				var classDate = this.id;
				if(classDate != "") {
					classInfo.html('');
					classInfo.html('<div class="loading"></div>');
					$(this).mousemove(function(e) {
						mouseX = e.pageX; 
						mouseY = e.pageY;
						classInfo.css("top",mouseY+"px");
						classInfo.css("left",mouseX+"px");
					});
					classInfo.show();
					$.ajax({
						url: "/ajax/classesDetails.php",
						data: "d="+classDate,
						success: function(data) {
							classInfo.html("");
							classInfo.append(data);
						}
					});
				}
			});
			
			classInfo.live("mouseleave", function() {
				classInfo.hide();
			});
			
		});
		
		/*SHOW CLASS DATES*/
		$('a#scheduleBtn').live("click",function() {
			$('div#assignClass').slideToggle('slow');
			return false;
		});
		
		/*-------------------------------------SUBMITTING FORM------------------------------------------*/
		$('button#submitVoucher').live("click",function() {
			var voucherName = $("input#name");
			var voucherPhone = $("input#phone");
			var voucherEmail = $("input#email");
			var voucherAddress = $("input#address");
			var voucherCity = $("input#city");
			var voucherState = $("input#state");
			var voucherZipCode = $("input#zip_code");
			var voucherNum1 = $("input#voucherNum1");
			var voucherDriver1 = $("input#voucherDriver1");				
				
			//NAME
			if(voucherName.val() == "" || voucherName.val() == "Full Name:") {
				voucherName.css("background","red");
				voucherName.css("color","black");
			} else {
				voucherName.css("background","black");
				voucherName.css("color","#919191");
			}
			//PHONE
			if(voucherPhone.val() == voucherPhone.prop("defaultValue") || voucherPhone.val() == "Phone Number:") {
				voucherPhone.css("background","red"); 
				voucherPhone.css("color","black");
			} else {
				voucherPhone.css("background","black");
				voucherPhone.css("color","#919191");
			}
			//EMAIL
			if(voucherEmail.val() == "" || voucherEmail.val() == "Email Address:") {
				voucherEmail.css("background","red");
				voucherEmail.css("color","black");
			} else {
				voucherEmail.css("background","black");
				voucherEmail.css("color","#919191");	
			}
			//ADDRESS
			if(voucherAddress.val() == voucherAddress.prop("defaultValue") || voucherAddress.val() == "Address:") {
				voucherAddress.css("background","red");
				voucherAddress.css("color","black");
			} else {
				voucherAddress.css("background","black");
				voucherAddress.css("color","#919191");
			}
			//CITY
			if(voucherCity.val() == voucherCity.prop("defaultValue") || voucherCity.val() == "City:") {
				voucherCity.css("background","red");
				voucherCity.css("color","black");
			} else {
				voucherCity.css("background","black");
				voucherCity.css("color","#919191");
			}
			//STATE
			if(voucherState.val() == voucherState.prop("defaultValue") || voucherState.val() == "State:") {
				voucherState.css("background","red");
				voucherState.css("color","black");
			} else {
				voucherState.css("background","black");
				voucherState.css("color","#919191");
			}
			//ZIP CODE
			if(voucherZipCode.val() == voucherZipCode.prop("defaultValue") || voucherZipCode.val() == "Zip Code:") {
				voucherZipCode.css("background","red");
				voucherZipCode.css("color","black");
			} else {
				voucherZipCode.css("background","black");
				voucherZipCode.css("color","#919191");
			}
			//VOUCHER NUM 1
			if(voucherNum1.val() == voucherNum1.prop("defaultValue") || voucherNum1.val() == "Voucher Number:") {
				voucherNum1.css("background","red");
				voucherNum1.css("color","black");
			} else {
				voucherNum1.css("background","black");
				voucherNum1.css("color","#919191");
			}
			//DRIVER 1
			if(voucherDriver1.val() == voucherDriver1.prop("defaultValue") || voucherDriver1.val() == "Driver Full Name:") {
				voucherDriver1.css("background","red");
				voucherDriver1.css("color","black");
			} else {
				voucherDriver1.css("background","black");
				voucherDriver1.css("color","#919191");
			}
			
			if(voucherName.val() != "" && voucherName.val() != "Full Name:" && voucherPhone.val() != voucherPhone.prop("defaultValue") && voucherPhone.val() != "Phone Number:" && voucherEmail.val() != "" && voucherEmail.val() != "Email Address:" && voucherAddress.val() != voucherAddress.prop("defaultValue") && voucherAddress.val() != "Address:" && voucherCity.val() != voucherCity.prop("defaultValue") && voucherCity.val() != "City:" && voucherState.val() != voucherState.prop("defaultValue") && voucherState.val() != "State:" && voucherZipCode.val() != voucherZipCode.prop("defaultValue") && voucherZipCode.val() != "Zip Code:" && voucherNum1.val() != voucherNum1.prop("defaultValue") && voucherNum1.val() != "Voucher Number:" && voucherDriver1.val() != voucherDriver1.prop("defaultValue") && voucherDriver1.val() != "Driver Full Name:") {
				$('button#submitVoucher').removeAttr("onclick");
				return true;
			} else {
				$('div.voucherPopUp').html("");
				$('div.voucherPopUp').html("Please fill in all the required fields.");
				$('div.voucherPopUp').fadeIn(300);
				$('div.voucherPopUp').delay(2000).fadeOut(300);
			}
			
			return false;
		});
		
		