/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/
	
	//RACE NOW
	$('li.raceNow, a.bookNow').live("click",function() {
		var racingCalHeight = $('div.startRacing').height() / 2;
		$('div.startRacing').css("marginTop","-"+racingCalHeight+"px");
		$('.racingOverlay').fadeIn(400);
		$('div.startRacing').delay(400).fadeIn(400);
		return false;
	});
	
	$('div.racingOverlay').live("click",function() {
		$('.racingOverlay').delay(400).fadeOut(400);
		$('div.startRacing').fadeOut(400);
		$('div.liveWeather').fadeOut(400);
		$('div.imageContainer').fadeOut(400);
		$('div.selectImageContainer').fadeOut(400);
		$('div#containGallery').fadeOut(400);
		return false;
	});
	
	
	//TRACK WEATHER
	$('li.liveWeather').live("click",function() {
		var weatherHeight = $('div.liveWeather').height() / 2;
		
		$('div.loadWeather').show();
		$('div.weatherData').html("");
		$('div.liveWeather').css("marginTop","-"+weatherHeight+"px");
		$('.racingOverlay').fadeIn(400);
		$('div.liveWeather').delay(400).fadeIn(400);
		$.ajax({
			url: "/ajax/trackWeather.php",
			success: function(data) {
				$('div.loadWeather').hide();
				$('div.weatherData').html("");
				$('div.weatherData').append(data);
			}
		});
		return false;
	});
	
	//MOVING NAVIGATION
	$(function() {
		$(window).on('scroll', function() {
			var scrollPosition = $(document).scrollTop();
			if(scrollPosition > 124) {
				$('header section nav.mainNav').css("position","fixed").css("top","4px");
			} else {
				$('header section nav.mainNav').css("position","relative").css("top","0px");
			}
		});
	});
	
	
	//CALENDAR BOOKING
	$(document).ready(function() {
	
		$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
			var showCalDate = this.id;
			$.ajax({
				url: "/ajax/calendar.php",
				data: "d="+showCalDate,
				success: function(data) {
					$('div.calendarContainer').html("");
					$('div.calendarContainer').append(data);
				}
			});
			return false;
		});
	
	});
	
	//FLASH NOTICE
	$('div.siteWideNotice div.close').live("click", function() {
		$('div.siteWideNotice').slideUp('slow');
	});
	
	//FOOTER BRAND ACTION
	$('a.footerBrand').live("click",function() {
		$('body').animate({
			scrollTop: '0px'
		},1000,"easeOutBack");
		return false;
	});
	
	
	function validEmailAddress(emailAddress) {
	    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	    return pattern.test(emailAddress);
	};
	
	$(function() {
	
		//CALENDAR SHOW DETAILS
		var mouseX, mouseY;
		var classInfo = $('div.calendarClassInfo'); 
		
		$('td.available').live("mouseenter", function() {
			var classDate = this.id;
			if(classDate != "") {
				classInfo.html('');
				classInfo.html('<div class="loading"></div>');
				$(this).mousemove(function(e) {
					mouseX = e.pageX; 
					mouseY = e.pageY;
					classInfo.css("top",mouseY+"px");
					classInfo.css("left",mouseX+"px");
				});
				classInfo.show();
				$.ajax({
					url: "/ajax/classesDetails.php",
					data: "d="+classDate,
					success: function(data) {
						classInfo.html("");
						classInfo.append(data);
					}
				});
			}
		});
		
		classInfo.live("mouseleave", function() {
			classInfo.hide();
		});
		
	});
	
	