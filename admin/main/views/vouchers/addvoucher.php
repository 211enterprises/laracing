<?php

	if(!isset($_POST['addVoucherCode'])) {
		echo '<h1>Add Voucher Code:</h1>';
	}
	
	
	//VOUCHER GROUPS
	$groupQuery = $mysqli->query("SELECT * FROM `LARX_voucher_companies` ORDER BY `company_name` ASC");
	
	
	if(isset($_POST['addVoucherCode'])) {
		$voucherCode = makeSQLSafe($mysqli,$_POST['voucherCode']);
		$name = makeSQLSafe($mysqli,$_POST['name']);
		$address = makeSQLSafe($mysqli,$_POST['address']);
		$city = makeSQLSafe($mysqli,$_POST['city']);
		$state = makeSQLSafe($mysqli,$_POST['state']);
		$zip = makeSQLSafe($mysqli,$_POST['zip']);
		$phone = makeSQLSafe($mysqli,$_POST['phone']);
		$email = makeSQLSafe($mysqli,$_POST['email']);
		$voucherGroup = makeSQLSafe($mysqli,$_POST['voucherGroup']);

		$codeCheck = $mysqli->query("SELECT `voucher_number` FROM `LARX_voucher_codes` WHERE `voucher_number` = '$voucherCode'");
		$codeResult = $codeCheck->num_rows;

		//ADD RECORED
		if($codeResult == 0) {
			$mysqli->query("INSERT INTO `LARX_voucher_codes` (`voucher_id`,`voucher_number`,`buyer_name`,`address`,`city`,`state`,`zip_code`,`phone`,`email`,`timestamp`)
			 VALUES ('$voucherGroup','$voucherCode','$name','$address','$city','$state','$zip','$phone','$email',NOW())");
		}
		
	}
	
if(!isset($_POST['addVoucherCode'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset>
			<label for="voucherCode">Voucher Code:</label>
			<input type="text" name="voucherCode" id="voucherCode" />
			
			<label for="name">Name:</label>
			<input type="text" name="name" id="name" />
			
			<label for="address">Address:</label>
			<input type="text" name="address" id="address" />
			
			<label for="city">City:</label>
			<input type="text" name="city" id="city" />
			
			<label for="state">State:</label>
			<input type="text" name="state" id="state" />
			
			<label for="zip">Zip Code:</label>
			<input type="text" name="zip" id="zip" />
			
			<label for="phone">Phone:</label>
			<input type="text" name="phone" id="phone" />
			
			<label for="email">Email Address:</label>
			<input type="text" name="email" id="email" />
			
			<label>Voucher Group:</label>
			<select name="voucherGroup" id="voucherGroup">
				<option value="">Select a Voucher Group:</option>
		<?php while($group = $groupQuery->fetch_array()) { ?>
				<option value="<?php echo $group['voucher_id']; ?>"><?php echo $group['company_name']; ?></option>
		<?php } ?>
			</select>
			<button type="submit">Add Voucher</button>
		</fieldset>
		<input type="hidden" name="addVoucherCode" />
	</form>
	
<?php 
	} else {
		if($codeResult == 0) {
			echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Voucher Code Has Been Added.<br /><a href="/admin/main/vouchers/addvoucher/" class="addMoreVouchers">Add Additional Codes</a></div>';
		} else {
			echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Voucher Code Has Been Used.<br /><a href="/admin/main/vouchers/addvoucher/" class="addMoreVouchers">Add a Different Code</a></div>';
		}
	}

$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/vouchers.js"></script>