<div class="container terms">
	<div class="row">
		<div class="col-md-12">
			<hr />
			<h3 style="text-align: center; margin-bottom: 0;">Terms and Conditions</h3>
			<hr />
		</div>
		<div class="col-md-12">
			* The group of the organization listed above (herein referred to as the "group") reserves the right to extend the campaign or cancel the campaign at their discretion.<br /><br />
			* The group will be given $50.00 per race pass that is sold on their behalf.<br /><br />
			* Funds and reporting will be administered via check at the close of the campaign. If a campaign is longer than 30 days, funds and reporting will be administered every 30 days throughout the campaign and at the close of the campaign. Please allow 7-10 business days for processing and delivery.
		</div>
	</div>
</div>