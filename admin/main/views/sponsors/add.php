<?php

	$id = makeSQLSafe($mysqli,$_GET['id']);
		
	if(!isset($_POST['addSponsor'])) {
		echo '<h1>Add Sponsor:</h1>';
	}
	
	if(isset($_POST['addSponsor'])) {
		$sponsorID = makeSQLSafe($mysqli,$_POST['updateSponsor']);
		$name = makeSQLSafe($mysqli,$_POST['sponsorName']);
		$formatURL = strstr($_POST['sponsorURL'], "http://");
		if($formatURL) {
			$url = makeSQLSafe($mysqli,$_POST['sponsorURL']);
		} else {
			$url = "http://".makeSQLSafe($mysqli,$_POST['sponsorURL']);
		}
		
		if($_FILES['sponsorFile']['size'] == 0) {
			$mysqli->query("INSERT INTO `LARX_sponsors` (`sponsor_name`, `sponsor_url`, `timestamp`) VALUES ('$name', '$url', NOW())");
			echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Has Been Added</div>';
		} else {
			$tmpFile = $_FILES['sponsorFile']['tmp_name'];
			$nameFile = $_FILES['sponsorFile']['name'];
			$fileSize = $_FILES['sponsorFile']['size'];
			$fileInfo = pathinfo($_FILES['sponsorFile']['name']);
			$fileExt = $fileInfo['extension'];
			$newFileName = SHA1(RAND(0000,9999).date("Y-m-d").$_SESSION['adminID'].rand(0000,9999)).".".$fileExt;
			$acceptType = array("jpg","jpeg","png","gif");
			if(!in_array($fileExt, $acceptType)) {
				echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Banner file type not accepted.</div>';
			} else {
				if($fileSize < 100240) {
					move_uploaded_file($_FILES['sponsorFile']['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/media/images/sponsors/".$newFileName);
					$mysqli->query("INSERT INTO `LARX_sponsors` (`sponsor_name`, `sponsor_banner`, `sponsor_url`, `timestamp`) VALUES ('$name', '/media/images/sponsors/".$newFileName."', '$url', NOW())");
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Has Been Added</div>';
				} else {
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Banner file is to big.</div>';
				}
			}
		}
		//header("Location: /admin/main/histor");
		//exit();
	} ?>

<?php if(!isset($_POST['addSponsor'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<fieldset class="editFAQ">
			<label>Sponsor Name:</label>
			<input type="text" name="sponsorName" id="sponsorName" />
			<label>Sponsor Web URL:</label>
			<input type="text" name="sponsorURL" id="sponsorURL" />
			<label>Replace Banner: <small>jpg, jpeg, gif, png</small></label>
			<input type="file" name="sponsorFile" id="sponsorFile" />
			<button type="submit">Add Sponsor</button>
		</fieldset>
		<input type="hidden" name="addSponsor" />
	</form>	
<?php
	}

$sponsorQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/sponsors.js"></script>