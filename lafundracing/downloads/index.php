<?php
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	
	$displayError = false;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no" />
<title>LA Fund Racing | Download Marketing Materials</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../partials/header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1>Downloads</h1>
		<p class="download-description">
			Some pdf files may contain multiple pages. When printing, make sure you select the option to print front and back.
		</p>
		<hr />

		<div class="row downloads">
			<div class="col-md-6">
				<a href="assets/orgBrochure.pdf" class="download-link">
					<img src="/media/images/pdf-icon.png" alt="Download PDF" /><br />
					Organization Brochure
				</a>
			</div>
			<div class="col-md-6">
				<a href="assets/individualBrochure.pdf" class="download-link">
					<img src="/media/images/pdf-icon.png" alt="Download PDF" /><br />
					Individual Brochure
				</a>
			</div>
		</div>
	</div>	
	
	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>
<?php $mysqli->close(); ?>