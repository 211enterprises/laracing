/*
// LA Fund Racing Registeration
*/
$(function() {
	
	$('select#registeredOrg').change(function() {
		var orgName = $('input#name'),
				orgEmail = $('input#email_address'),
				orgAddress = $('input#address'),
				orgCity = $('input#city'),
				orgState = $('input#state'),
				orgZip = $('input#zip_code'),
				orgPhone = $('input#phone'),
				orgEIN = $('input#EIN'),
				orgNonProfit = $('select#nonprofit'),
				orgCategory = $('select#category');
				
		// Array of field variables
		var fields = [orgName, orgEmail, orgAddress, orgCity, orgState, orgZip, orgPhone, orgEIN, orgNonProfit, orgCategory];
		
		if($(this).val() != "") {
			$('div#registerOrg').hide();
			//Disable required attribute
			$.each(fields, function(index, value) {
				value.removeAttr('required');
			});
		} else {
			$('div#registerOrg').show();
			//Enabled required attribute
			$.each(fields, function(index, value) {
				value.attr('required','required');
			});
		}
	});
	
	//Approve organizations
	$('a.approve-organization').click(function() {
		var elem = $(this),
				groupId = elem.attr('data-id');
		
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: '/lafundracing/admin/services/approve_organization.php',
			data: {id: groupId},
			success: function(data) {
				if(data.status) {
					elem.parent().parent().parent().parent().parent().fadeOut(500);
				}
			},
			error: function() {
				console.error('Something went wrong approving this group');
			}
		});
		
		return false;
	});
	
	//Approve groups
	$('a.approve-group').click(function() {
		var elem = $(this),
				groupId = elem.attr('data-id');
		
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: '/lafundracing/admin/services/approve_group.php',
			data: {id: groupId},
			success: function(data) {
				if(data.status) {
					elem.parent().parent().parent().parent().parent().fadeOut(500);
				}
			},
			error: function() {
				console.error('Something went wrong approving this group');
			}
		});
		
		return false;
	});
});