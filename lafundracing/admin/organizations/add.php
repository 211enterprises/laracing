<?php
	if(session_id() == "") session_start();
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	//Instantiate classes
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->check_admin_access();
	
	$LAFundAdmin->add_organization($_POST);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1>Add Organization:</h1>
		<hr />
		
		<form action="" method="post">
			<div class="row">
				<div class="col-md-6">
					<label for="name">Name:</label>
					<input type="text" name="name" id="name" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="email">Email:</label>
					<input type="email" name="email" id="email" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="address">Address:</label>
					<input type="text" name="address" id="address" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="city">City:</label>
					<input type="text" name="city" id="city" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="state">State:</label>
					<input type="text" name="state" id="state" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="zip_code">Zip Code:</label>
					<input type="text" name="zip_code" id="zip_code" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="phone">Phone:</label>
					<input type="text" name="phone" id="phone" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Representative Name:</label>
					<input type="text" name="rep_name" id="rep_name" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="EIN">EIN (Federal Tax ID):</label>
					<input type="text" name="EIN" id="EIN" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="non_profit">Non-Profit Organization:</label>
					<select name="non_profit" id="non_profit" class="LAFRAdmin" required>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
				<div class="col-md-12">
					<label for="category">Category:</label>
					<select name="category" id="category" required>
						<?php
						foreach($LAFundRacing->get_lafr_categories() as $value) {
							echo '<option value="'.$value['id'].'">'.$value['type'].'</option>';
						}
						?>
					</select>
				</div>
				<div class="col-md-12">
					<label for="active">Active Organization:</label>
					<select name="active" id="active" class="LAFRAdmin" required>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
				<div class="col-md-12">
					<label for="mission_statement">Mission Statement:</label>
					<textarea class="LAFRAdmin" name="mission_statement" id="mission_statement"></textarea>
				</div>
				<div class="col-md-12 pull-right">
					<button type="submit">Add Organization</button>
				</div>
			</div>
			<input type="hidden" name="organization" />
		</form>
			
		</div>		
	</div>	

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>