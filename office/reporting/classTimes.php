<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$date = makeSQLSafe($mysqli,$_GET['d']);
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `date` = '$date' ORDER BY `time` ASC");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Class Times</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo date("M jS, Y",strtotime($date)); ?></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">

<?php if($classQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="classTimes">
   			<li data-listdivider="true" data-theme="a">Choose a class time:</li>
   	<?php while($class = $classQuery->fetch_assoc()) {
   				echo '<li><a href="/office/reporting/classReport.php?h='.$class['class_hash'].'" data-rel="dialog" data-transition="pop">'.date("h:i A",strtotime($class['time'])).'</a></li>';
   		  } ?>
   		</ul>
<?php } else echo '<div style="position:relative; float:left; text-align:center; font-size:18px; color:black; margin:20px 0;">No Class Found</div>'; ?>
   		
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>