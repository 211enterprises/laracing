<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$template = new template;
	$booking = new booking;
	$content = new content;
		
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Racing X : Client Testimonials</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/slideshow.js"></script>
<script type="text/javascript" src="/media/js/testimonials.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

<?php echo $template->header(); ?>

<!--MAIN CONTENT-->
<div class="content">

<?php $content->testimonials($mysqli); ?>
	
</div>
<!--END MAIN CONTENT-->

<?php echo $template->footer($mysqli); ?>

</body>
</html>
<?php $mysqli->close(); ?>