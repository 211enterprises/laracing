/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/		
		
		//MOVING NAVIGATION
		$(function() {
			$(window).on('scroll', function() {
				var scrollPosition = $(document).scrollTop();
				if(scrollPosition > 124) {
					$('header section nav.mainNav').css("position","fixed").css("top","4px");
				} else {
					$('header section nav.mainNav').css("position","relative").css("top","0px");
				}
			});
		});
		
		
		//CALENDAR BOOKING
		$(document).ready(function() {
		
			$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
				var showCalDate = this.id;
				$.ajax({
					url: "/ajax/calendar.php",
					data: "d="+showCalDate,
					success: function(data) {
						$('div.calendarContainer').html("");
						$('div.calendarContainer').append(data);
					}
				});
				return false;
			});
		
		});
		
		
		//INPUT OPACITY
		$('input,textarea').live("focus", function() {
			$(this).animate({
				opacity: "0.5"
			},200);
		});
		$('input,textarea').live("blur", function() {
			$(this).animate({
				opacity: "0.3"
			},200);
		});
		
		
		//MATCH DIVIDE HEIGHT TO TALLEST COLUMN
		$(function() {
			var leftColHeight = $('div.leftCol').height(),
				rightColHeight = $('div.rightCol').height();
			if(leftColHeight > rightColHeight) {var hrHeight = leftColHeight;} else {var hrHeight = rightColHeight;}
			$('hr.divideVertical').css("height",hrHeight+"px");
		});
		
		
		//VIEW MAPS
		var viewMapBtn = $('a.viewMapsBtn');
		viewMapBtn.live({
			mouseover: function() {
				$(this).animate({
					backgroundPositionY : "0px"
				},300,"easeOutBack");
			},
			mouseout: function() {
				$(this).animate({
					backgroundPositionY: "-10px"
				},300,"easeOutBack");
			}
		});
		
		//ON CLICK VIEW MAPS
		viewMapBtn.live("click",function() {
			$('section.mapsContainer ul').slideToggle(600,"easeInBack");
			return false;
		});
		
		
		function validEmailAddress(emailAddress) {
		    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		    return pattern.test(emailAddress);
		};
		
		
		$(function() {
	
			//CALENDAR SHOW DETAILS
			var mouseX, mouseY;
			var classInfo = $('div.calendarClassInfo'); 
			
			$('td.available').live("mouseenter", function() {
				var classDate = this.id;
				if(classDate != "") {
					classInfo.html('');
					classInfo.html('<div class="loading"></div>');
					$(this).mousemove(function(e) {
						mouseX = e.pageX; 
						mouseY = e.pageY;
						classInfo.css("top",mouseY+"px");
						classInfo.css("left",mouseX+"px");
					});
					classInfo.show();
					$.ajax({
						url: "/ajax/classesDetails.php",
						data: "d="+classDate,
						success: function(data) {
							classInfo.html("");
							classInfo.append(data);
						}
					});
				}
			});
			
			classInfo.live("mouseleave", function() {
				classInfo.hide();
			});
			
		});
		
		
		/*-------------------------------------SUBMITTING FORM------------------------------------------*/
		$('button').live("click",function() {
			var contactName = $("input[name=contactName]");
			var contactEmail = $("input[name=contactEmail]");
			var contactRegarding = $("select[name=contactRegarding]");
			var contactComments = $("textarea#contactComments");
				
			if(contactName.val() == "") {contactName.css("background","red"); contactName.css("color","black");} else {contactName.css("background","black"); contactName.css("color","#919191");}
			if(contactEmail.val() == "") {contactEmail.css("background","red"); contactEmail.css("color","black");} else {contactEmail.css("background","black"); contactEmail.css("color","#919191");}
			if(contactRegarding.val() == "") {contactRegarding.css("background","red"); contactRegarding.css("color","black");} else {contactRegarding.css("background","black"); contactRegarding.css("color","#919191");}
			if(contactComments.val() == "") {contactComments.css("background","red"); contactComments.css("color","black");} else {contactComments.css("background","black"); contactComments.css("color","#919191");}
			
			if(validEmailAddress(contactEmail)) {
				contactEmail.css("background","black"); contactEmail.css("color","#919191");
			} else {
				contactEmail.css("background","red"); contactEmail.css("color","black");
			}
			
			if(contactName.val() != "" && contactEmail.val() != "" && contactRegarding.val() != "" && contactComments.val() != "") {
				$('div.rightCol fieldset').html('<div style="position:relative; float:left; width:100%; text-align:center; margin:160px 0px;"><img src="/media/images/ajaxLoader.gif" alt="Loading" /></div>');
				$.ajax({
					url: "/ajax/contact.php",
					data: "n="+encodeURIComponent(contactName.val())+"&e="+encodeURIComponent(contactEmail.val())+"&r="+encodeURIComponent(contactRegarding.val())+"&c="+encodeURIComponent(contactComments.val()),
					success: function(data) {
						$('div.rightCol fieldset').html("");
						$('div.rightCol fieldset').append(data);
					}
				});
			}
			
			return false;
		});
		