

	//ASSIGN ORDER
	//ALBUMS
	$(function() {
		$("ul.albums").sortable({ opacity: 1.0, cursor: 'move', update: function() {
				var albumOrder = $(this).sortable("serialize") + '&action=setAlbumOrder';
				$.post("/admin/main/ajax/albumsOrder.php", albumOrder, function(theResponse){ });
			}
		});
	});
	
	//ALBUM MEDIA
	$(function() {
		$("ul.media").sortable({ opacity: 1.0, cursor: 'move', update: function() {
				var mediaOrder = $(this).sortable("serialize") + '&action=setMediaOrder';
				$.post("/admin/main/ajax/mediaOrder.php", mediaOrder, function(theResponse){ });
			}
		});
	});
	
	
	$('a.deleteContent').live("click",function() {
		if(confirm("Are you sure you want to delete this content?")) {
			return true;
		}
		return false;
	});
	
	
	//SHOW/HIDE OPTIONS
	$('select[name=mediaType]').live("change",function() {
		var mediaType = $(this).val();
		if(mediaType == 1) {
			$('div#videoMedia').show();
			$('div#imageMedia').hide();
		} else {
			$('div#videoMedia').hide();
			$('div#imageMedia').show();
		}
	});
	
	$('select[name=videoSource]').live("change",function() {
		var videoSource = $(this).val();
		switch(videoSource) {
			case "youtube":
				$('span#videoSample').html("http://www.youtube.com/watch?v=<span>12345678</span>");
				break;
			case "vimeo":
				$('span#videoSample').html("http://vimeo.com/<span>12345678</span>");
				break;
			default:
				$('span#videoSample').html("http://www.youtube.com/watch?v=<span>12345678</span>");
				break;
		}
	});