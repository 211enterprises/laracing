<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#organization<?php echo $value['id']; ?>">
        <?php echo $value['name']; ?>
      </a>
      <div class="pull-right">
        <?php echo array_sum($org_sales); ?>
      </div>
    </h4>
  </div>
  <div id="organization<?php echo $value['id']; ?>" class="panel-collapse collapse">
    <div class="panel-body">
      Email: <?php echo $value['email']; ?><br />
      Phone: <?php echo $value['phone']; ?><br />
      EIN: <?php echo $value['EIN']; ?><br />
      <hr />
      Groups:
      <div class="row show-grid">
<?php if(sizeof($division) > 0) {
		foreach($division as $k => $val) {
		  $sales = $LAFundAdmin->get_group_total_sales($val['id']); ?>
				<div class="col-md-3">
        	<div>
        		<a href="" class="glyphicon glyphicon-remove pull-right"></a>
        		<a href="<?php echo LAFR_ADMIN_ROOT.'/reporting/?id='.$val['id']; ?>" class="glyphicon glyphicon-stats pull-right" style="margin-right: 8px;"></a>
        		<a href="<?php echo LAFR_ADMIN_ROOT."/group/edit.php?group=".$val['id']; ?>">
        		  <?php echo $val['name']; ?> <?php echo ($sales != "") ? "(".$sales.")" : "(0)";  ?>
        		</a>
        	</div>
      	</div>			       
<?php 	} 
	} else { ?>
				<div class="col-md-12">
					No Current Groups
				</div>			
<?php } ?>
      </div>
      
      <div class="row" style="margin-top: 30px;">
      	<div class="col-md-6">
      		<a href="<?php echo LAFR_ADMIN_ROOT."/group/add.php?org=".$value['id']; ?>" class="btn btn-block btn-primary">Add Group</a>
      	</div>
      	<div class="col-md-6">
      		<a href="<?php echo LAFR_ADMIN_ROOT."/organizations/edit.php?org=".$value['id']; ?>" class="btn btn-block btn-primary">Edit Organization</a>
      	</div>
      </div>
    </div>
  </div>
</div> 