$(function() {

    $('div.chooseCat ul li').click(function() {
        var elem = $(this),
                categoryID = elem.attr('class'),
                selectDivision = $('select#organization'),
                divisionDIV = $('div.chooseDiv'),
                divisions = [];

        //Activate active icon
        elem.closest('ul').children().removeClass('active');
        elem.addClass('active');

        //Enable division options
        selectDivision.empty();
        selectDivision.removeAttr('disabled');
        divisionDIV.slideUp();
        divisions.push('<option value="">Select:</option>');

        $.each(organizations, function(k, v) {
            if(categoryID == v.category_id) {
                divisions.push('<option value="'+ v.id +'" data-org-id="'+ k +'">'+ v.name +'</option>');
            }
        });

        selectDivision.html(divisions);
    });

    // More information on organizations
    $('a#showOrgInfo').live("click", function() {
        $('div.orgInfo p').slideToggle();
        return false;
    });


    $('select#organization').change(function() {
        var id = $(this).find('option:selected').val(),
                divisionDIV = $('div.chooseDiv'),
                selectDiv = $('select#division'),
                loader = $('div.loading'),
                support_member = $('div#studentInfo'),
                divisions = [],
                orgID = $(this).find('option:selected').attr('data-org-id');

        //Hide divisons and show loader
        divisionDIV.slideUp(function() {
            selectDiv.empty();
        });
        loader.slideDown();

        $.ajax({
            url: '/ajax/fundDivisions.php?org=' + id,
            dataType: 'json',
            success: function(data) {

                selectDiv.empty();

                loader.slideUp(function() {
                    if(data.length > 0) {
                        divisionDIV.slideDown(function() {
                            $.each(data, function(key, val) {
                                divisions.push('<option value="'+ val.id +'">'+ val.name +'</option>');
                            });

                            selectDiv.html(divisions);
                        });
                    }
                });

            },
            error: function() {

            }
        });

        $('div.orgInfo').html('');
        $('div.orgInfo').append('<a href="" id="showOrgInfo">More information about this organization</a><p></p>');
        $('div.orgInfo p').append('<span class="name">'+ organizations[orgID].name +'</span><br />');
        $('div.orgInfo p').append('<span>Address:</span> '+ organizations[orgID].address +'<br />');
        $('div.orgInfo p').append('<span>City:</span> '+ organizations[orgID].city +'<br />');
        $('div.orgInfo p').append('<span>State:</span> '+ organizations[orgID].state +'<br />');
        $('div.orgInfo p').append('<span>Zip Code:</span> '+ organizations[orgID].zip_code +'<br />');
        $('div.orgInfo p').append('<span>Phone:</span> '+ organizations[orgID].phone +'<br />');
        if(organizations[orgID].mission != null || organizations[orgID].mission != undefined) {
            $('div.orgInfo p').append('<span>Organization Misson:</span><br />'+ organizations[orgID].mission);
        } else {
            $('div.orgInfo p').append('<span>Organization Misson:</span><br />Mission Coming Soon.');
        }

        // Hide member supporter if applicable
        if(organizations[orgID].support_member) {
          support_member.slideDown();
          $("input[name='memberName']").prop('required', true);
        } else {
          support_member.slideUp();
          $("input[name='memberName']").prop('required', false);
        }
    });

    // ----------------- Pass Quantity ------------------ //
    $('select#quantity').change(function() {
        var quantity = $(this).val(),
                price = 150,
                split = 50,
                orgProfit = quantity * 50,
                total = quantity * price;

        $('input#quantity').val(quantity);
        $('table.summary tr td.passQuantity').html(quantity);
        $('table.summary tr td.orgSplit span').html(orgProfit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
        $('table.summary tr td.orderTotal span').html(total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    });

    // ----------------- Submit LA Fund Racing form ------------------ //
    $('form').submit(function() {
        if($(this).formValidation()) {
            if($(this.email.value).validateEmail()) {
                return true;
            }	else {
                showFlashNotice('Please enter a valid enter address.', 2000);
            }
        } else {
            showFlashNotice('Please fill in the required fields.', 2000);
        }

        return false;
    });

    // -------------------- Show Organization Sign Up ------------------- //
    $('a#orgSignUp').click(function(e) {
        $('div.racingOverlay').fadeIn(500);

        e.preventDefault();
    });

    // ----------------- Restrict numbers on CC fields ------------------ //
    $('input#cardNum').forceNumbersOnly();
    $('input#cardCVV2').forceNumbersOnly();

    //Close flash notice
    $('div.flashNotice i.close').click(function() {
        $('div.flashNotice').slideUp();
    });
});

$.fn.formValidation = function() {
    var elem = this,
            counter = 0;

    $('form input, form select').each(function(i, field) {
        if(field.value == ""){
            //Check if field is required
            if(field.required) {
                counter++;
            }
        }
    });

    if(counter == 0) {
        return true;
    } else {
        return false;
    }
};

$.fn.validateEmail = function() {
    var regex = /\S+@\S+\.\S+/;

    if(regex.test(this)) {
        return true;
    } else {
        return false;
    }
};

$.fn.forceNumbersOnly = function() {
    $(this).keydown(function(e) {
        if(!(event.keyCode == 8                                // backspace
             || event.keyCode == 9															 // tab
       || event.keyCode == 46                              // delete
       || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
       || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
       || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
    ) {
            event.preventDefault();     // Prevent character input
    }
  });
};

function showFlashNotice(notice, delay) {
    var elem = 'div.flashNotice',
            flashHTML = '<div class="flashNotice"><span>'+ notice +'</span></div>';

    //Remove any existing flash notices
    $(elem).remove();
    //Create new flash element
    $('body').append(flashHTML);

    $(elem + ' span').html(notice);
    $(elem).slideToggle(function() {
        $(elem).delay(delay).slideToggle();
    });
}
