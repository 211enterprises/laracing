<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$racerQuery = $mysqli->query("SELECT * FROM `LARX_class_roster` WHERE `id` = '$id' LIMIT 1");
	if($racerQuery->num_rows == 1) {
		$racer = $racerQuery->fetch_array();
		$classID = $racer['class_id'];
		
		//FETCH ASSIGNED CLASS
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$classID' LIMIT 1");
		$class = $classQuery->fetch_array();
		$classSlots = $class['slots_taken'] - 1;
		
		//UPDATE CLASS
		$mysqli->query("UPDATE `LARX_class_dates` SET `slots_taken` = '$classSlots' WHERE `id` = '$classID' LIMIT 1");
		//DELETE RECORD
		$mysqli->query("DELETE FROM `LARX_class_roster` WHERE `id` = '$id' LIMIT 1");
		
		
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer has been deleted.</div>';
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer can\'t be found. Please try again!</div>';
	}

$racerQuery->close();
$mysqli->close();
?>