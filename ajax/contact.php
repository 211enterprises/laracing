<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['n']) && isset($_GET['e']) && isset($_GET['c'])) {
		$name = makeSQLSafe($mysqli,$_GET['n']);
		$email = makeSQLSafe($mysqli,$_GET['e']);
		$regarding = makeSQLSafe($mysqli,$_GET['r']);
		$comments = makeSQLSafe($mysqli,$_GET['c']);
		
		//MAIL FUNCTION
		$to = "support@laracing.com";
		
		$message = "Name: $name \n";
		$message .= "Email Address: $email \n";
		$message .= "Comments:\n $comments \n";
		
		$headers =  'From: LA Racing X <noreply@laracingx.com>' . "\r\n";
		$headers .= 'Reply-To: noreply@laracingx.com' . "\r\n";
		$headers .= 'Bcc: allan@acmediadesigns.com' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
		
		mail($to,$regarding,$message,$headers);
	
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:20px; text-align:center; margin:160px 0px; text-shadow:2px 2px 0px black; -webkit-text-shadow:2px 2px 0px black; -moz-text-shadow:2px 2px 0px black; -o-text-shadow:2px 2px 0px black;">Thank You! We will contact you as soon as possible.</div>';
	}
	
?>