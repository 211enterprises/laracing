<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$passNumber = makeSQLSafe($mysqli,$_GET['n']);
	$hash = makeSQLSafe($mysqli,$_GET['h']);
	
	if($_GET['h'] == "nonDatePurchase") {
		$packageQuery = $mysqli->query("SELECT * FROM `LARX_classes` ORDER BY `order` ASC"); ?>
		<!--RACE PASS-->
		<li>
			<a href="" class="removePassBtn" title="Remove Racing Pass">Remove Pass</a>
			<div class="passHeader">
				<img src="/media/images/passesIcon.png" alt="Race Passes" />
				Race Pass #<span><?php echo $passNumber + 1; ?></span>
				<div style="position:relative; float:right; font-size:18px;">Do you want a in car camera video?</div>
			</div>
			<fieldset>
				<select class="selectPackage" name="racePackage[]" id="racePackage">
					<option value="">Select Your Racing Package:</option>
		<?php while($package = $packageQuery->fetch_assoc()) {
					echo '<option value="'.$package['id']."-".$package['class_price'].'">'.$package['class_name']." - $".$package['class_price'].'</option>';
			  } ?>
				</select>
				<select class="selectVideo" name="raceVideo[]" id="raceVideo">
					<option value="0">Do you want a video recording? $99</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</fieldset>
		</li>
		<!--ENDRACE PASS-->	
<?php

	} else {
	
		if(isset($_GET['n']) && isset($_GET['h'])) {
			
			$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$hash' LIMIT 1");
			
			if($classQuery->num_rows == 1) {
				$class = $classQuery->fetch_assoc();
				$classID = str_replace(",","','",$class['class_id']);
				$openPasses = $class['class_limit'] - $class['slots_taken'] - 1;
				
				$packageQuery = $mysqli->query("SELECT * FROM `LARX_classes` WHERE `id` IN('".$classID."') ORDER BY `order` ASC");
				
				if($passNumber - $openPasses <= 0) { ?>
					<!--RACE PASS-->
					<li>
						<a href="" class="removePassBtn" title="Remove Racing Pass">Remove Pass</a>
						<div class="passHeader">
							<img src="/media/images/passesIcon.png" alt="Race Passes" />
							Race Pass #<span><?php echo $passNumber + 1; ?></span>
							<div style="position:relative; float:right; font-size:18px;">Do you want a in car camera video?</div>
						</div>
						<fieldset>
							<select class="selectPackage" name="racePackage[]" id="racePackage">
								<option value="">Select Your Racing Package:</option>
					<?php while($package = $packageQuery->fetch_assoc()) {
								echo '<option value="'.$package['id']."-".$package['class_price'].'">'.$package['class_name']." - $".$package['class_price'].'</option>';
						  } ?>
							</select>
							<select class="selectVideo" name="raceVideo[]" id="raceVideo">
								<option value="0">Do you want a video recording? $99</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</fieldset>
					</li>
					<!--ENDRACE PASS-->
	<?php
				} else exit;
			}
			$classQuery->close();
			
		} else {
			exit;
		}
	}

$mysqli->close();
?>