<?php
	$albumsQuery = $mysqli->query("SELECT * FROM `LARX_albums` ORDER BY `order` ASC");
?>

	<h1>LARX Gallery</h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=gallery&action=addmedia" class="addContent">Add Media</a>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=gallery&action=addalbum" class="addContent">Add Album</a>
	
<?php if($albumsQuery->num_rows > 0) { ?>
	<ul class="albums">
<?php while($album = $albumsQuery->fetch_assoc()) { ?>
		<li id="albumOrder_<?php echo $album['album_id']; ?>">
			<a href="<?php echo ADMIN_ROOT; ?>/?controller=gallery&action=editalbum&id=<?php echo $album['album_id']; ?>">
				<div class="albumName"><?php echo $album['album_name']; ?></div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Photo Albums</div>';

$albumsQuery->close();
$mysqli->close(); ?>
<script type="text/javascript" src="/media/js/admin/gallery.js"></script>