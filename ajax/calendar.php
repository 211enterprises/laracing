<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$booking = new booking;
	
	$date = makeSQLSafe($mysqli,$_GET['d']);
	if(isset($_GET['d']) || $_GET['d'] != NULL) {
		$m = substr($date,0,2);
		$y = substr($date,3,4);
	} else {
		$m = date("m");
		$y = date("Y");
	}
	
	echo $booking->calendar($mysqli,$m,$y);
		
?>