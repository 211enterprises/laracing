	
	$(function() {
		
		//INPUT OPACITY
		$('div.frontActionsWrap fieldset input').live("focus", function() {
			$(this).animate({
				opacity: "0.5"
			},200);
		});
		$('div.frontActionsWrap fieldset input').live("blur", function() {
			$(this).animate({
				opacity: "0.3"
			},200);
		});
		
		//MATCH WELCOME DIV LINE TO COLUMN
		var welcomeColHeight = $('div.welcomeRightCol').height();
		$('section.welcome hr.vertical').css('height',welcomeColHeight+'px');
		
		
		//NEWSLETTER SIGNUP
		$('div.frontActionsWrap fieldset button#mailingSignUp').on("click",function() {
			var newsletterName = $('input[name=newsletterName]').val();
			var newsletterEmail = $('input[name=newsletterEmail]').val();
			var newsletterSource = encodeURIComponent($('input[name=newsletterSource]').val());
			
			if(validEmailAddress(newsletterEmail)) {
				//VALIDATED
				$('div.newsletterError').slideUp(400);
				$('div.frontActionsWrap fieldset#newsletter').html("");
				$('div.frontActionsWrap fieldset#newsletter').append('<div class="loading"></div>');
				$.ajax({
					url: "/ajax/newsletter.php",
					data: "n="+newsletterName+"&e="+newsletterEmail+"&s="+newsletterSource,
					success: function(data) {
						$('div.frontActionsWrap fieldset#newsletter').html("");
						$('div.frontActionsWrap fieldset#newsletter').append(data);
					},
					error: function() {
						$('div.frontActionsWrap fieldset#newsletter').html("");
						$('div.frontActionsWrap fieldset#newsletter').append('<div class="newsletterError" style="margin:150px 0px">An Error Had Occurred. Please Try Later.</div>');		
					}
				});
			} else {
				//NOT VALIDATED
				$('div.newsletterError').slideDown(400);
			}
			
			return false;
		});
		
		//LIVE TWEETER FEED
		var tweetFeed = "https://api.twitter.com/1/statuses/user_timeline.json?screen_name="+tweetUsername+"&count=1&callback=?";
		if(typeof tweetUsername === "undefined") {
			var tweetUsername = 'LARacingX';
		}
		$.getJSON(tweetFeed, function(data) {
			$.each(data, function(i,item) {
				ct = item.text;
				ct = ct.replace(/http:\/\/\S+/g, '<a href="$&" target="_blank">$&</a>');
			    ct = ct.replace(/\s(@)(\w+)/g,   ' @<a href="http://twitter.com/$2" target="_blank">$2</a>');
			    ct = ct.replace(/\s(#)(\w+)/g,   ' #<a href="http://search.twitter.com/search?q=%23$2" target="_blank">$2</a>');
				$("div.tweet").append('<div>'+ct +"</div>");
				$("div.followTweets").append(' <a href="http://twitter.com/'+tweetUsername+'" target="_blank">@'+tweetUsername+'</a>');
			});
		});
		
		//SUBMIT REDEEM VOUCHER
		$('div.frontActionsWrap fieldset button#redeemVoucher').live("click",function() {
			var assignVoucher = $('select[name=voucherSoruce]').val();
			if(assignVoucher != "") {
				$('div.frontActionsWrap form#startVoucherForm').attr("action","/"+assignVoucher+"/");
				return true;
			}
			return false;
		});
		
		
		//WATCH VIDEO
		$('a.watchVideo').live("mouseover", function() {
			$(this).animate({
				bottom: "-4px"
			},300,"easeOutBack");
		});
		$('a.watchVideo').live("mouseout", function() {
			$(this).animate({
				bottom: "-14px"
			},300,"easeOutBack");
		});
		
	});
	
	function homeValidateVoucher(input,src,num) {
		$.ajax({
			url: "/ajax/validateVoucher.php",
			data: "c="+src+"&v="+num,
			success: function(data) {
				if(data == 1) {
					if(num != "") {
						input.css("background","green");
						input.css("color","black");
					} else {
						input.css("background","black");
						input.css("color","#919191");
					}
				} else {
					if(num != "") {
						input.css("background","red");
						input.css("color","black");
					} else {
						input.css("background","black");
						input.css("color","#919191");
					}
				}
			},
			error: function() {
				alert("An error has occuried. Please try again later.");
			}
		});
	}
	
	//VALIDATE VOUCHER NUMBERS
	$(function() {
		$('form#startVoucherForm button').live("click",function() {
			var voucherName = $('form#startVoucherForm input[name=\'voucherName\']');
			var voucherEmail = $('form#startVoucherForm input[name=\'voucherEmail\']');
			var voucherNum = $('form#startVoucherForm input[name=\'voucherNumber\']');
			var voucherSrc = $('form#startVoucherForm select[name=\'voucherSource\']');
			
			//VALIDATE FIELDS
			if(voucherName.val() == "") { voucherName.css("background","red"); voucherName.css("color","black"); } else { voucherName.css("background","black"); voucherName.css("color","#919191"); }
			if(voucherEmail.val() == "") { voucherEmail.css("background","red"); voucherEmail.css("color","black"); } else { voucherEmail.css("background","black"); voucherEmail.css("color","#919191"); }
			if(voucherNum.val() == "") { voucherNum.css("background","red"); voucherNum.css("color","black"); } else { voucherNum.css("background","black"); voucherNum.css("color","#919191"); }
			
			//VALIDATE EMAIL
			if(!validEmailAddress(voucherEmail.val())) { voucherEmail.css("background","red"); voucherEmail.css("color","black"); } else { voucherEmail.css("background","black"); voucherEmail.css("color","#919191"); }
			
			if(voucherSrc.val() != "") {
				$('div.frontActionsWrap form#startVoucherForm').attr("action","/"+voucherSrc.val()+"/");
				return true;
			}
			
			//homeValidateVoucher($(this),voucherSrc,voucherNum)
			return false;
		});
	});
	
	$(function() {
	
		//CALENDAR SHOW DETAILS
		var mouseX, mouseY;
		var classInfo = $('div.calendarClassInfo'); 
		
		$('td.available').live("mouseenter", function() {
			var classDate = this.id;
			if(classDate != "") {
				classInfo.html('');
				classInfo.html('<div class="loading"></div>');
				$(this).mousemove(function(e) {
					mouseX = e.pageX; 
					mouseY = e.pageY;
					classInfo.css("top",mouseY+"px");
					classInfo.css("left",mouseX+"px");
				});
				classInfo.show();
				$.ajax({
					url: "/ajax/classesDetails.php",
					data: "d="+classDate,
					success: function(data) {
						classInfo.html("");
						classInfo.append(data);
					}
				});
			}
		});
		
		classInfo.live("mouseleave", function() {
			classInfo.hide();
		});
		
	});
		
		