<?php
	
	if(session_id() == "") session_start();
	
	if(!isset($_SESSION['adminID'])) {
		header("Location: /admin/");
		exit();
	}
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/admin.inc");
	
	$admin = new admin($_GET);
	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<title>LA Racing X : Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/admin.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<!--[if lt IE 9]>
<script src="/media/js/html5shiv.js"></script>
<![endif]-->
</head>
<body>

<!--MAIN CONTENT-->
<section>
	
	<section class="container">
		<header>
			<div class="headerUsers"><?php echo $admin->adminRole($mysqli,$_SESSION['adminID']); ?></div>
			<div class="brand"><a href="/admin/main/"><img src="/media/images/topBrand.png" alt="" /></a></div>
			<a href="/admin/?logout" class="logout">Admin Logout</a>
			
			<nav>
				<ul>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/history/"; ?>">History</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 2) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/classes/"; ?>">Classes</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/gallery/"; ?>">Gallery</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/simulators/"; ?>">Simulators</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/sponsors/"; ?>">Sponsors</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/faq/"; ?>">FAQ</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 3) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/testimonials/"; ?>">Testimonials</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 2) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/vouchers/"; ?>">Vouchers</a></li>
			<?php } ?>
			<?php if($admin->adminRoleValue($mysqli,$_SESSION['adminID']) >= 4) { ?>
					<li><a href="<?php echo ADMIN_ROOT."/settings/"; ?>">Settings</a></li>
			<?php } ?>
				</ul>
			</nav>
			
		</header>
		
		
		<!--MAIN CONTENT-->
		<div class="content">
		<?php $admin->Loader($mysqli); ?>
		</div>
		<!--MAIN CONTENT-->
	</section>
	
</section>
<!--END MAIN CONTENT-->

<footer>
	<?php echo date("Y"); ?> &copy; LA Racing Experience - All Rights Reserved
	<div class="acmedia">
		Powered By: <a href="http://www.acmediadesigns.com" target="_blank">AC Media Designs</a>
	</div>
</footer>

</body>
</html>
<?php $mysqli->close(); ?>