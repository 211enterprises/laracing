<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['n']) && isset($_GET['e']) && isset($_GET['pn']) && isset($_GET['d'])) {
		$name = makeSQLSafe($mysqli,$_GET['n']);
		$email = makeSQLSafe($mysqli,$_GET['e']);
		$phone = makeSQLSafe($mysqli,$_GET['p']);
		$partyNum = makeSQLSafe($mysqli,$_GET['pn']);
		$date = makeSQLSafe($mysqli,$_GET['d']);
		$notes = makeSQLSafe($mysqli,$_GET['notes']);
		
		//MAIL FUNCTION
		$to = "support@laracingx.com";
		$subject = "LA Racing X Requested Event";
		
		$message = "Name: $name \n";
		$message .= "Email Address: $email \n";
		$message .= "Phone Number: $phone \n";
		$message .= "Number in Party: $partyNum \n";
		$message .= "Requested Date: $date \n";
		$message .= "Notes:\n $notes \n";
		
		$headers =  'From: LA Racing X <noreply@laracingx.com>' . "\r\n";
		$headers .= 'Reply-To: noreply@laracingx.com' . "\r\n";
		$headers .= 'Bcc: allan@acmediadesigns.com' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
		
		mail($to,$subject,$message,$headers);
	
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:20px; text-align:center; margin:160px 0px; text-shadow:2px 2px 0px black; -webkit-text-shadow:2px 2px 0px black; -moz-text-shadow:2px 2px 0px black; -o-text-shadow:2px 2px 0px black;">Thank You! We will contact you as soon as possible.</div>';
	}
	
?>