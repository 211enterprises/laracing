<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$sponsorQuery = $mysqli->query("SELECT * FROM `LARX_sponsors` WHERE `id` = '$id' LIMIT 1");
	if($sponsorQuery->num_rows == 1) {
		$sponsor = $sponsorQuery->fetch_array();
		
		//DELETE RECORD
		$bannerFile = $_SERVER['DOCUMENT_ROOT'].$sponsor['sponsor_banner'];
		if(file_exists($bannerFile)) {
			unlink($bannerFile);
		}
		$mysqli->query("DELETE FROM `LARX_sponsors` WHERE `id` = '$id' LIMIT 1");
		
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor has been deleted.</div>';
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor can\'t be found. Please try again!</div>';
	}

$sponsorQuery->close();
$mysqli->close();
?>