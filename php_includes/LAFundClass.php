<?php
	class LAFundRacing extends Core {
	
		function __construct($mysqli) {
			$this->db = $mysqli;
		}
		
		public function fund_categories() {
			$results = $this->db->query("SELECT * FROM `LAFR_categories` ORDER BY `order` ASC");
			echo '<ul>';
				while($org = $results->fetch_array()) {
				echo '<li class="'. $org['id'] .'"><img src="/media/images/lafundracing/'. $org['icon'] .'" alt="" /></li>';
				}
			echo '</ul>';
		}
		
		public function get_lafr_categories() {
			$results = $this->db->query("SELECT * FROM `LAFR_categories` ORDER BY `order` ASC");
			return $results;
			$results->close();
		}
		
		public function get_organization_by_group_id($id) {
			$query = $this->get_fund_division_by_id($id);
			if($query->num_rows == 1) {
				$group = $query->fetch_array();
				$result = $this->db->query("SELECT * FROM `LAFR_organizations` WHERE `id` = '".$group['org_id']."' LIMIT 1");
			
				return $result;	
				$result->close();
			}
			
			$query->close();
		}
		
		public function get_fund_organization_name($org) {
			$query = $this->db->query("SELECT `name` FROM `LAFR_organizations` WHERE `id` = '$org' LIMIT 1");
			$result = $query->fetch_array();
			
			return $result['name'];
			$query->close();
		}
		
		public function get_fund_division_name($div) {
			$query = $this->db->query("SELECT `name` FROM `LAFR_divisions` WHERE `id` = '$div' LIMIT 1");
			$result = $query->fetch_array();
			
			return $result['name'];
			$query->close();
		}
		
		public function get_fund_division_by_id($div) {
			$result = $this->db->query("SELECT * FROM `LAFR_divisions` WHERE `id` = '$div' LIMIT 1");
			
			return $result;
			$result->close();
		}
		
		public function get_active_fund_organizations() {
			$query = $this->db->query("SELECT * FROM `LAFR_organizations` WHERE `active` = 1 AND `archive` = 0 ORDER BY `name` ASC");
			return $query;
			$query->close();
		}
		
		public function get_unactive_organizations() {
			$query = $this->db->query("SELECT * FROM `LAFR_organizations` WHERE `active` = 0 AND `archive` = 0 ORDER BY `name` ASC");
			return $query;
			$query->close();
		}
		
		public function get_unactive_groups() {
			$query = $this->db->query("SELECT * FROM `LAFR_divisions` WHERE `active` = 0 ORDER BY `name` ASC");
			return $query;
			$query->close();
		}
		
		public function active_fund_organizations($format=NULL) {
			$query = $this->db->query("SELECT * FROM `LAFR_organizations` WHERE `active` = 1 AND `archive` = 0 ORDER BY `name` ASC");

			$count = 0;
			$results = array();
			
			while($org = $query->fetch_assoc()) {				
				$results[$count][id] = $org['id'];
				$results[$count][category_id] = $org['category_id'];
				$results[$count][name] = $org['name'];
				$results[$count][address] = $org['address'];
				$results[$count][city] = $org['city'];
				$results[$count][state] = $org['state'];
				$results[$count][zip_code] = $org['zip_code'];
				$results[$count][phone] = $org['phone'];
				$results[$count][mission] = $org['mission_statement'];
        $results[$count][support_member] = (int) $org['support_member'];
					
				$count++;
			}
			
			switch($format) {
				case 'json':
					return json_encode($results);
					break;
				default:
					return $results;
			}
			$query->close();
		}
		
		public function fund_organization_by_id($id) {
			$results = $this->db->query("SELECT * FROM `LAFR_organizations` WHERE `id` = '$id' LIMIT 1");
			return $results;
			$results->close();
		}
		
		public function fund_divisions($fund=null, $format=NULL, $admin=0) {
			$today = date("Y-m-d");
      if($admin) {
        // Show all divisions not based on dates
        $query = $this->db->query("SELECT `id`,`org_id`,`name` FROM `LAFR_divisions` WHERE `org_id` = '$fund' ORDER BY `name` ASC");
      } else {
        $query = $this->db->query("SELECT `id`,`org_id`,`name` FROM `LAFR_divisions` WHERE `org_id` = '$fund' AND `campaign_start` <= NOW() AND `campaign_end` >= NOW() ORDER BY `name` ASC");
      }
			$results = $this->return_data_array($query);
			
			switch($format) {
				case 'json':
					return json_encode($results);
					break;
				default:
					return $results;
			}
			
			$query->close();
		}
		
		public function process_save_order($data) {
			if($this->save_trans_data($data)) {
				return true;
			} else {
				return false;
			}
		}
		
		private function return_data_array($data) {
			$count = 0;
			$array = array();
			
			while($org = $data->fetch_assoc()) {
				
				foreach($org as $k => $v) {
					$array[$count][$k] .= $v;
				}	
				$count++;
			}
			
			return $array;
		}
		
		private function save_trans_data($data) {
			$organization = makeSQLSafe($this->db, $data['organization']);
			$division = makeSQLSafe($this->db, $data['division']);
			$firstName = makeSQLSafe($this->db, $data['firstName']);
			$lastName = makeSQLSafe($this->db, $data['lastName']);
			$member = makeSQLSafe($this->db, $data['memberName']);
			$email = makeSQLSafe($this->db, $data['email']);
			$phone = makeSQLSafe($this->db, $data['phone']);
			$address = makeSQLSafe($this->db, $data['address']);
			$city = makeSQLSafe($this->db, $data['city']);
			$state = makeSQLSafe($this->db, $data['state']);
			$zipCode = makeSQLSafe($this->db, $data['zip_code']);
			$comments = makeSQLSafe($this->db, $data['comments']);
			$quantity = makeSQLSafe($this->db, $data['quantity']);
      $ip = $_SERVER['REMOTE_ADDR'];
			
			$save = $this->db->query("INSERT INTO `LAFR_transactions` (`division_id`,`member_name`,`first_name`,`last_name`,`email_address`,`phone`,`address`,`city`,`state`,`zip_code`,`comments`,`pass_quantity`,`purchaser_ip`,`created_at`)
			VALUES ('$division','$member','$firstName','$lastName','$email','$phone','$address','$city','$state','$zipCode','$comments','$quantity', '$ip', NOW())");
			
			if($save) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	// LA fund racing registration class
	class LAFundRegister extends LAFundRacing {
		function __construct($connect) {
			$this->db = $connect;
		}
		
		public function process_registration($params) {
			// Check if organization is already registered
			if($params['registeredOrg'] != "" && $params['registeredOrg'] != NULL) {
				$this->save_group_registeration($params['registeredOrg'], $params);
				$group_id = $this->db->insert_id;
				// Save group payable information
				$this->save_group_payable_registration($group_id, $params);
				//Send admin notification
				$this->send_admin_register_notification();
				
				return true;
			} else {
				// New organization registration
				if($this->save_organization_registration($params)) {
					$org_id = $this->db->insert_id;
					// Save group
					$this->save_group_registeration($org_id, $params);
					$group_id = $this->db->insert_id;
					// Save group payable information
					$this->save_group_payable_registration($group_id, $params);
					//Send admin notification
					$this->send_admin_register_notification();
					
					return true;
				}
			}
			
			return false;
		}
		
		public function approve_organization($org_id) {
			$approve = $this->db->query("UPDATE `LAFR_organizations` SET `active` = 1 WHERE `id` = '$org_id' LIMIT 1");
			
			if($approve) {
				return true;
			}
			
			return false;
			$approve->close();
		}
		
		public function approve_group($group_id) {
			$approve = $this->db->query("UPDATE `LAFR_divisions` SET `active` = 1 WHERE `id` = '$group_id' LIMIT 1");
			//Fetch group information
			$group = $this->get_fund_division_by_id($group_id)->fetch_array();
			//Send approval email notification
			$this->send_approval_notification($group['contact_email']);
			
			if($approve) {
				return true;
			}
			
			return false;
			$approve->close();
		}
		
		protected function send_admin_register_notification() {
			$to  = "allan@211enterprises.com" . ", ";
			$to .= "laura@laracing.com";
			$subject = "LA Fund Racing Online Registration";
			$message = "<b>An organization and/or group registered on LA Fund Racing!</b>";
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: LA Fund Racing <noreply@laracingx.com>' . "\r\n";
			
			mail($to, $subject, $message, $headers);
		}
		
		protected function send_approval_notification($email) {
			$subject = "LA Fund Racing Approval";
			$message  = "<b>Congratulations!</b><br /><br />";
			$message .= "Your recent LA Fund Racing registration submission has been approved!<br />";
			$message .= "Our system is now ready to accept purchases under your organization and/or group.<br />";
			$message .= "If you have any questions, feel free to give us a call at: (855)RACE-4-FUNDS. We look forward to working with you!";
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: LA Fund Racing <noreply@laracingx.com>' . "\r\n";
			
			mail($email, $subject, $message, $headers);
		}
		
		private function save_organization_registration($params) {
			$name = makeSQLSafe($this->db, $params['name']);
			$email = makeSQLSafe($this->db, $params['email_address']);
			$address = makeSQLSafe($this->db, $params['address']);
			$city = makeSQLSafe($this->db, $params['city']);
			$state = makeSQLSafe($this->db, $params['state']);
			$zip = makeSQLSafe($this->db, $params['zip_code']);
			$phone = makeSQLSafe($this->db, $params['phone']);
			$EIN = makeSQLSafe($this->db, $params['EIN']);
			$nonprofit = makeSQLSafe($this->db, $params['nonprofit']);
			$category = makeSQLSafe($this->db, $params['category']);
			$referral = makeSQLSafe($this->db, $params['rep_name']);
			
			$required = array($name, $email, $address, $city, $state, $zip, $phone, $EIN, $nonprofit, $category);
			$valid = true;
			
			// Validate parameters
			foreach($required as $val) {
				if($val == NULL || $val == "") {
					$valid = false;
				}
			}
			
			//Save organization
			$save = $this->db->query("INSERT INTO `LAFR_organizations` (`category_id`,`name`,`email`,`address`,`city`,`state`,`zip_code`,`phone`,`rep_name`,`EIN`,`non-profit`,`active`,`created_at`) VALUES ('$category','$name','$email','$address','$city','$state','$zip','$phone','$referral','$EIN','$nonprofit',0,NOW())");
			
			// If successfully saved
			if($save && $valid) {
				return true;
			}
			
			return false;
		}
		
		private function save_group_registeration($org_id, $params) {
			$name = makeSQLSafe($this->db, $params['groupName']);
			$campaignStart = makeSQLSafe($this->db, $params['groupCampaignStart']);
			$campaignEnd = makeSQLSafe($this->db, $params['groupCampaignEnd']);
			$contactName = makeSQLSafe($this->db, $params['groupContactName']);
			$email = makeSQLSafe($this->db, $params['groupEmail']);
			$phone = makeSQLSafe($this->db, $params['groupPhone']);
			$mission = makeSQLSafe($this->db, $params['groupMission']);
			$referral = makeSQLSafe($this->db, $params['groupRep']);
			
			$required = array($name, $campaignStart, $campaignEnd, $contactName, $email, $referral);
			$valid = true;
			
			// Make sure all required params are present
			foreach($required as $val) {
				if($val == NULL || $val == "") {
					$valid = false;
				}
			}
			
			if($org_id != "" || $org_id != NULL || $org_id != 0) {
				// Save group
				$save = $this->db->query("INSERT INTO `LAFR_divisions` (`org_id`,`name`,`campaign_start`,`campaign_end`,`contact_name`,`contact_email`,`contact_phone`,`rep_name`,`mission_statement`,`active`,`created_at`) VALUES ('$org_id','$name','$campaignStart','$campaignEnd','$contactName','$email','$phone','$referral','$mission',0,NOW())");
				
				if($save && $valid) {
					return true;
				}
			}
			
			return false;
		}
		
		private function save_group_payable_registration($group_id, $params) {
			$attention = makeSQLSafe($this->db, $params['payAttn']);
			$name = makeSQLSafe($this->db, $params['payName']);
			$address = makeSQLSafe($this->db, $params['payAddress']);
			$city = makeSQLSafe($this->db, $params['payCity']);
			$state = makeSQLSafe($this->db, $params['payState']);
			$zip = makeSQLSafe($this->db, $params['payZip']);
			
			$required = array($attention, $name, $address, $city, $state, $zip);
			$valid = true;
			
			// Validate params
			foreach($required as $val) {
				if($val == "" || $val == NULL) {
					$valid = false;
				}
			}
			
			if($group_id != 0 || $group_id != "" || $group_id != NULL) {
				$save = $this->db->query("INSERT INTO `LAFR_division_payable` (`division_id`,`attention`,`payable_name`,`address`,`city`,`state`,`zip_code`) VALUES ('$group_id','$attention','$name','$address','$city','$state','$zip')");
			
				if($save && $valid) {
					return true;
				}
			}
			
			return false;
		}
	}
?>