<?php
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$sponsorQuery = $mysqli->query("SELECT * FROM `LARX_sponsors` WHERE `id` = '$id' LIMIT 1");
	if($sponsorQuery->num_rows == 1) {
		$sponsor = $sponsorQuery->fetch_array();
		
		if(!isset($_POST['updateSponsor'])) {
			echo '<h1>Edit Sponsor:</h1>';
			echo '<a href="'.ADMIN_ROOT.'/?controller=sponsors&action=delete&id='.$sponsor['id'].'" class="deleteContent">Delete This Sponsor</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateSponsor'])) {
		$sponsorID = makeSQLSafe($mysqli,$_POST['updateSponsor']);
		$name = makeSQLSafe($mysqli,$_POST['sponsorName']);
		$formatURL = strstr($_POST['sponsorURL'], "http://");
		if($formatURL) {
			$url = makeSQLSafe($mysqli,$_POST['sponsorURL']);
		} else {
			$url = "http://".makeSQLSafe($mysqli,$_POST['sponsorURL']);
		}
		
		if($_FILES['sponsorFile']['size'] == 0) {
			$mysqli->query("UPDATE `LARX_sponsors` SET `sponsor_name` = '$name', `sponsor_url` = '$url' WHERE `id` = '$sponsorID' LIMIT 1");
			echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Has Been Updated</div>';
		} else {
			$tmpFile = $_FILES['sponsorFile']['tmp_name'];
			$nameFile = $_FILES['sponsorFile']['name'];
			$fileSize = $_FILES['sponsorFile']['size'];
			$fileInfo = pathinfo($_FILES['sponsorFile']['name']);
			$fileExt = $fileInfo['extension'];
			$newFileName = SHA1(RAND(0000,9999).date("Y-m-d").$_SESSION['adminID'].rand(0000,9999)).".".$fileExt;
			$acceptType = array("jpg","jpeg","png","gif");
			if(!in_array($fileExt, $acceptType)) {
				echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Banner file type not accepted.</div>';
			} else {
				if($fileSize < 100240) {
					move_uploaded_file($_FILES['sponsorFile']['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/media/images/sponsors/".$newFileName);
					$mysqli->query("UPDATE `LARX_sponsors` SET `sponsor_name` = '$name', `sponsor_banner` = '/media/images/sponsors/".$newFileName."', `sponsor_url` = '$url' WHERE `id` = '$sponsorID' LIMIT 1");
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Has Been Updated</div>';
				} else {
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Banner file is to big.</div>';
				}
			}
		}
		//header("Location: /admin/main/histor");
		//exit();
	} ?>

<?php if(!isset($_POST['updateSponsor']) && $sponsorQuery->num_rows == 1) {
if($sponsor['sponsor_banner'] != NULL) { ?>
	<div class="sponsorBanner">
		<img src="<?php echo $sponsor['sponsor_banner']; ?>" alt="" />
	</div>
<?php } ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<fieldset class="editFAQ">
			<label>Sponsor Name:</label>
			<input type="text" name="sponsorName" id="sponsorName" value="<?php echo $sponsor['sponsor_name']; ?>" />
			<label>Sponsor Web URL:</label>
			<input type="text" name="sponsorURL" id="sponsorURL" value="<?php echo $sponsor['sponsor_url']; ?>" />
			<label>Replace Banner: <small>jpg, jpeg, gif, png</small></label>
			<input type="file" name="sponsorFile" id="sponsorFile" value="<?php echo $sponsor['sponsor_url']; ?>" />
			<button type="submit">Update Sponsor</button>
		</fieldset>
		<input type="hidden" name="updateSponsor" value="<?php echo $sponsor['id']; ?>" />
	</form>	
<?php
	}

$sponsorQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/sponsors.js"></script>