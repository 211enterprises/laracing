<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	if(!isset($_GET['id'])) {
		header("/admin/main/classes/");
		exit;
	}
	
	$rosterQuery = $mysqli->query("SELECT * FROM `LARX_class_roster` WHERE `class_id` = '$id'");
?>

	<h1>Class Roster</h1>
<?php
	$classCheckQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$id' LIMIT 1");
	$classCheck = $classCheckQuery->fetch_assoc();
	$takenClass = $classCheck['class_limit'] - $classCheck['slots_taken'];
	$newTakenClass = $classCheck['slots_taken'] + 1;
	if($takenClass > 0) { ?>
	<a href="/admin/main/?controller=classes&action=addracer&id=<?php echo $id; ?>" class="addContent">Add a Racer</a>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=classes&action=assign" class="addContent">Assign Racer</a>
<?php } ?>
	
<?php if($rosterQuery->num_rows > 0) { ?>
	<ul class="roster">
<?php while($roster = $rosterQuery->fetch_assoc()) {
		$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '".$roster['pass_id']."'");
		$pass = $passQuery->fetch_array(); ?>
		<li>
			<a href="/admin/main/classes/racer/<?php echo $roster['id']; ?>/">
				<div class="racer"><?php if($roster['racer_name'] != "") echo $roster['racer_name']; else echo $roster['buyer_name']; ?></div>
				<div class="pass"><span><?php echo $pass['pass_number']; ?></span></div>
				<div class="edit">Edit Racer</div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Roster</div>'; ?>