/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/		
		
		//MOVING NAVIGATION
		$(function() {
			$(window).on('scroll', function() {
				var scrollPosition = $(document).scrollTop();
				if(scrollPosition > 124) {
					$('header section nav.mainNav').css("position","fixed").css("top","4px");
				} else {
					$('header section nav.mainNav').css("position","relative").css("top","0px");
				}
			});
		});
		
		
		//CALENDAR BOOKING
		$(document).ready(function() {
		
			$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
				var showCalDate = this.id;
				$.ajax({
					url: "/ajax/calendar.php",
					data: "d="+showCalDate,
					success: function(data) {
						$('div.calendarContainer').html("");
						$('div.calendarContainer').append(data);
					}
				});
				return false;
			});
		
		});
		
		
		$(function() {
	
		//CALENDAR SHOW DETAILS
		var mouseX, mouseY;
		var classInfo = $('div.calendarClassInfo'); 
		
		$('td.available').live("mouseenter", function() {
			var classDate = this.id;
			if(classDate != "") {
				classInfo.html('');
				classInfo.html('<div class="loading"></div>');
				$(this).mousemove(function(e) {
					mouseX = e.pageX; 
					mouseY = e.pageY;
					classInfo.css("top",mouseY+"px");
					classInfo.css("left",mouseX+"px");
				});
				classInfo.show();
				$.ajax({
					url: "/ajax/classesDetails.php",
					data: "d="+classDate,
					success: function(data) {
						classInfo.html("");
						classInfo.append(data);
					}
				});
			}
		});
		
		classInfo.live("mouseleave", function() {
			classInfo.hide();
		});
		
	});
	
	//SHOW GALLERY IMAGES
	$('a.albumContainer').live("click",function() {
		$('div#containGallery').html("");
		var albumID = this.id.substr(7);
		$('div.racingOverlay').fadeIn(300);
		$('div.racingOverlay').append('<div class="galleryLoader"></div>');
		$.ajax({
			url: "/ajax/album.php",
			data: "a="+albumID,
			success: function(data) {
				$('div.galleryLoader').fadeOut(300);
				$('div#containGallery').html("");
				$('div#containGallery').append(data);
				$('div#containGallery').delay(300).fadeIn(300);
			}
		});
		return false;
	});
	
	//SELECT IMAGES
	$('div.selectImageContainer ul li').live("click",function() {
		var mediaGalleryID = this.id.substr(12);
		$('div.displayImage').html('<div class="galleryLoader"></div>');
		$.ajax({
			url: "/ajax/media.php",
			data: "m="+mediaGalleryID,
			success: function(data) {
				$('div.displayImage').html("");
				$('div.displayImage').append(data);
			}
		});
		return false;
	});
	
	
	//UIX SLIDER
	$('button.ui-galleryBtn-left,button.ui-galleryBtn-right').live("click",function() {
		var windowWidth = $(window).width(),
			containerWidth = $('div.showGalleryThumbs').width(),
			sliderWidth = $('div.showGalleryThumbs ul').width(),
			sliderDirection = $(this).attr("data-slider"),
			thumbNum = Number($('div.showGalleryThumbs ul li').index()) + 1,
			numSliders = Math.round(thumbNum / thumbsInView),
			sliderNum = 1,
			thumbWidth = "130";
			
		//CHECK TO SEE IF THUMBS ARE MORE THAN WIDTH
		if(sliderWidth > windowWidth) {
			var thumbsInView = Math.floor(containerWidth / thumbWidth);
			var newThumbPosition = thumbWidth * thumbsInView;
			
			if(sliderDirection == "next") {
				if(sliderNum != numSliders) {
					var newSliderPosition = Number($('div.showGalleryThumbs ul').css("left").replace(/[-px]/g,"")) + newThumbPosition;
					
					$('div.showGalleryThumbs ul').animate({
						left: "-"+newSliderPosition+"px"
					},400,"easeInBack");
					
					sliderNum++;
				}

			} else {
				var newSliderPosition = Number($('div.showGalleryThumbs ul').css("left").replace(/[-px]/g,"")) - newThumbPosition;
				if(newSliderPosition > 0) {
					$('div.showGalleryThumbs ul').animate({
						left: "-"+newSliderPosition+"px"
					},400,"easeInBack");
				} else {
					$('div.showGalleryThumbs ul').animate({
						left: "0px"
					},400,"easeInBack");
				}
			}
			
		}
		return false;
	});
	
	