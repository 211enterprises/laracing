<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	//Instantiate classes
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->check_admin_access();
	
  //Save group notes
  if(isset($_POST["groupNotes"]) && isset($_POST["commissionNotes"])) {
  	if($LAFundAdmin->modifiy_group_notes($_GET["id"], $_POST)) {
      header("Location: ".$_SERVER["REQUEST_URI"]);
      exit;
  	}
  }
	
	// fetch Report data - types: organization, group, payment, transactions
	$report = $LAFundAdmin->generate_division_report($_GET['id']);
	// Assign variables to each report array
	$organization = $report['organization'][0];
	$group = $report['group'][0];
	$payable = $report['payment'][0];
	$transactions = $report['transactions'];
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<link href="/media/style/reporting-print.css" type="text/css" rel="stylesheet" media="print" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1>Reporting:</h1>
		<hr />
		
		<section class="group-information">
			<?php include_once('organization-information.partial.php'); ?>
			
			<hr />
			
			<?php include_once('group-information.partial.php'); ?>
			
			<hr />
			
			<?php include_once('payment-information.partial.php'); ?>
			
			<div class="clearfix"></div>
		</section>
		
		<section class="groupNotes">
  		<hr />
  		<h3>Group Notes:</h3>
  		<hr />
  		<form action="" method="post">
  			<div class="row">
  				<div class="col-md-12">
  					<label for="groupNotes">Group Notes:</label>
  					<textarea name="groupNotes" id="groupNotes" style="width: 100%;" placeholder="Write something about this group and\or payable information."><?php echo $payable['payable_notes']; ?></textarea>
  				</div>
  				
  				<div class="col-md-12">
  					<label for="commissionNotes">Commission Notes:</label>
  					<textarea name="commissionNotes" id="commissionNotes" style="width: 100%;" placeholder="Write something about representative commissions."><?php echo $payable['commission_notes']; ?></textarea>
  				</div>
  				
  				<div class="col-md-12 pull-right">
    				<button type="submit">Update Notes</button>
  				</div>
  			</div>
  		</form>
		</section>
		
		<section class="sales-report">
			<?php include_once('transactions.partial.php'); ?>
		</section>
		
	</div>	

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>