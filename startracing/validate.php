<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(!empty($_GET)) {
	
		$ccNumber = makeSQLSafe($mysqli,$_GET['num']);
		$exp = makeSQLSafe($mysqli,$_GET['exp']);
		$code = makeSQLSafe($mysqli,$_GET['code']);
		$amount = number_format(makeSQLSafe($mysqli,$_GET['amount']),2,"."," ");
	
		//------------------------------MERCHANT INTEGRATION-------------------------------------------------------------------------------------------------------------------
		// By default, this sample code is designed to post to our test server for
		// developer accounts: https://test.authorize.net/gateway/transact.dll
		// for real accounts (even in test mode), please make sure that you are
		// posting to: https://secure.authorize.net/gateway/transact.dll
	
		$post_url = "https://secure.authorize.net/gateway/transact.dll";
	
		$post_values = array(
		// the API Login ID and Transaction Key must be replaced with valid values
		"x_login"			=> "4A73suHpeTh",
		"x_tran_key"		=> "9T4BCaB66hUj35EQ",
	
		"x_version"			=> "3.1",
		"x_delim_data"		=> "TRUE",
		"x_delim_char"		=> "|",
		"x_relay_response"	=> "FALSE",
	
		"x_type"			=> "AUTH_ONLY",
		"x_method"			=> "CC",
		"x_card_num"		=> "".$ccNumber."",
		"x_exp_date"		=> "".$exp."",
		"x_card_code"		=> "".$code."",
	
		"x_amount"			=> "".$amount."",
		"x_description"		=> "LA Racing X Card Validation",
		
		// Additional fields can be added here as outlined in the AIM integration
		// guide at: http://developer.authorize.net
		);
	
		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
	
		// This sample code uses the CURL library for php to establish a connection,
		// submit the post, and record the response.
		// If you receive an error, you may want to ensure that you have the curl
		// library enabled in your php configuration
		$request = curl_init($post_url); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object
	
		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($post_values["x_delim_char"],$post_response);
	
		//GET THE RESPONSE FOR VALIDATION
		echo $response_array[0];
	}
	
?>