<div class="row-fluid">
	<div class="col-md-12">
		<h3>Group Payable Information:</h3>
	</div>
	<div class="col-md-6">
		Payable Name: <span><?php echo ($payable['payable_name'] != NULL) ? $payable['payable_name'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Attention: <span><?php echo ($payable['attention'] != NULL) ? $payable['attention'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Address: <span><?php echo ($payable['address'] != NULL) ? $payable['address'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		City: <span><?php echo ($payable['city'] != NULL) ? $payable['city'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		State: <span><?php echo ($payable['state'] != NULL) ? $payable['state'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Zip Code: <span><?php echo ($payable['zip_code'] != NULL) ? $payable['zip_code'] : 'N/A'; ?></span>
	</div>
</div>