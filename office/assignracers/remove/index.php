<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$c = makeSQLSafe($mysqli,$_GET['c']);
	if($_GET['c'] != "") {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$c' LIMIT 1");
		if($classQuery->num_rows == 1) {
			$class = $classQuery->fetch_assoc();
			$classID = $class['id'];
		
			//Passes Query
			$passQuery = $mysqli->query("SELECT `id`,`class_id`,`profile_id`,`buyer_name`,`driver_name`,`pass_number`,`pass_status`,`pass_hash`,`timestamp` FROM `LARX_race_passes` WHERE `class_id` = '$classID' AND `pass_status` = 'valid' ORDER BY `timestamp` ASC");
			
		} else {
			header("Location: /office/scheduling/");
			exit;
		}
	} else {
		header("Location: /office/scheduling/");
		exit;
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Remove Assigned Racers</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
	   <a href="" data-role="button" data-rel="back" data-icon="arrow-l" data-iconpos="left" data-theme="a">Back</a>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
<?php if($passQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="processPasses" data-inset="true" data-split-icon="minus" data-split-theme="c" data-filter="true" data-filter-placeholder="Search by name or profile #...">
   			<li data-listdivider="true" data-theme="a">Remove Assigned Racers From Class: <?php echo date("M jS, Y h:i A", strtotime($class['date']." ".$class['time'])); ?></li>
   	<?php while($pass = $passQuery->fetch_assoc()) { ?>
   			<li>
				<a href="/office/passes/viewPass.php?p=<?php echo $pass['pass_hash']; ?>" data-rel="dialog" data-transition="pop"><?php echo $pass['pass_number']." - "; if($pass['buyer_name'] != NULL) echo $pass['buyer_name']; else echo $pass['driver_name']; ?></a>
				<a href="" id="removeAssignedRacerBtn" data-assign-racer="<?php echo $pass['pass_hash']; ?>" data-class-hash="<?php echo $class['class_hash']; ?>">Process Pass</a>
				<?php if($racer['red_flag'] == 1) echo '<div class="racer-flag"><img src="/office/global/images/red_flag.png" alt="Racer Red Flagged" /></div>'; ?>
			</li>
   	<?php } ?>
   		</ul>
<?php } else { ?>
		<ul data-role="listview" data-inset="true">
			<li data-listdivider="true" data-theme="a">No Current Racers Assigned</li>
		</ul>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo footer("scheduling"); ?>
      
</div>

</body>
</html>
<?php
$racersQuery->close();
$mysqli->close();	
?>