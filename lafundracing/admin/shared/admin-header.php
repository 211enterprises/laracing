<!--HEADER-->
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="<?php echo LAFR_ADMIN_ROOT.'/organizations/'; ?>">Organizations</a></li>
			<li><a href="<?php echo LAFR_ADMIN_ROOT.'/approve/organizations'; ?>">Approve Organizations</a></li>
			<li><a href="<?php echo LAFR_ADMIN_ROOT.'/approve/groups'; ?>">Approve Groups</a></li>
      <li><a href="<?php echo LAFR_ADMIN_ROOT.'/logout/'; ?>">Logout</a></li>
    </ul>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
<header class="main">	
	<!--BRANDING-->
	<a href="/" class="brand"><img src="/media/images/topBrand.png" alt="LA Racing X" /></a><br />
	<img src="/media/images/slogan.png" alt="The Ultimate Racing Experience">
</header>
<!--END HEADER-->	