<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/racePassClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/paymentsClass.php");
  // reCaptcha library
  require_once($_SERVER['DOCUMENT_ROOT']."/vendor/recaptchalib.php");
	
	$template = new template;
	$core = new core;
	$LAFundRacing = new LAFundRacing($mysqli);
	$RacePass = new RacePasses($LARXDB);
	$payments = new Payments($mysqli, LAFR_PAYMENT_STATUS);
	
	$processed = false;
	$displayError = false;
	
	if(isset($_POST) && !empty($_POST)) {
    // Check captcha response
    $captcha_resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
      $_SERVER["REMOTE_ADDR"],
      $_POST["recaptcha_challenge_field"],
      $_POST["recaptcha_response_field"]
    );

    if($captcha_resp->is_valid) {
      // Valid reCaptcha
      $captureCard = $payments->authorize_capture($LARXDB, $_POST);

      if($captureCard['status']) {
        if($LAFundRacing->process_save_order($_POST)) {
          $passes = $RacePass->process_fund_race_passes($_POST);
          $processed = true;
        }
      } else {
        //Display card error message to user
        $errorMsg = $captureCard['message'];
        $displayError = true;
      }
    } else {
      // Invalid reCaptcha
      $errorMsg = "Invalid captcha value, please try again.";
      $displayError = true;
    }
	}	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('partials/header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
<?php if(!$processed) { ?>		
		<div class="desc">
			<h2>
				Adventure 20 Race Pass:<br /><br /><br />
				<span class="price">$150</span><br />
				<span class="subText">per race pass</span>
			</h2>
		</div>
		
		<hr />
		<div class="process step1">1</div>
		<hr />
		
		<h2 class="orgHeader">Choose your category: <span>*</span></h2>
		<div class="chooseCat">
	<?php $LAFundRacing->fund_categories(); ?>
		</div>
	</div>
	
	<form action="" method="post">
		<div class="container form">	
			<hr />
			<div class="process step2">2</div>
			<hr />
			<h2 class="chooseOrg">Choose your organization: <span>*</span></h2>
			<fieldset>
				<select name="organization" id="organization" class="organizations" multiple="multiple" required="required">
					<option value="">Select a category:</option>
				</select>
			</fieldset>
			<div class="orgInfo"></div>
			<hr />
			
			<div class="loading divisions">
				<img src="/media/images/lafundracing/ajax-loader.gif" alt="" />
				<hr />
			</div>
			
			<div class="clearfix"></div>
			
			<div class="chooseDiv">
				<div class="process step3">3</div>
				<hr />
				<h2 class="chooseDiv">Choose a group: <span>*</span></h2>
				<?php $core->selectBox('division', array(), 'greySelect', false, $_POST, true); ?>
				<hr />
			</div>

      <div id="studentInfo">
        <h3>Student/Member Information:</h3>
        <fieldset>
        <?php
          $core->labelFor('memberName', 'Name', true);
          $core->input('text', 'memberName', 'form-control input-lg', '', $_POST, true);
        ?>
        </fieldset>
        <span class="memberSubTitle">(Student, athlete, member you're supporting.)</span>
      </div>
		</div>
		
		<hr class="personalDivide" />

		<div class="container form">	
			<h3>Purchasers Information:</h3>
			<fieldset>
			<?php 
				$core->labelFor('firstName', 'First Name:', true);
				$core->input('text', 'firstName', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('lastName', 'Last Name:', true);
				$core->input('text', 'lastName', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('email', 'Email Address:', true);
				$core->input('email', 'email', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('phone', 'Phone:', true);
				$core->input('tel', 'phone', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('address', 'Address:', true);
				$core->input('text', 'address', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('city', 'City:', true);
				$core->input('text', 'city', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('state', 'State:', true);
				$core->input('text', 'state', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('zip_code', 'Zip Code:', true);
				$core->input('text', 'zip_code', 'form-control input-lg', '', $_POST, true);
				
				$core->labelFor('comments', 'Message/Comments:');
				$core->textarea('comments', 'form-control', $_POST['comments']);
			?>
			</fieldset>
			
			<hr />
			<h3>Race Pass(es):</h3>
			<!--<ul class="racePasses">
				<li></li>
			</ul>-->
			<?php 
				$quantity = array();
				for($i = 1; $i <= 50; $i++) {
					$quantity[$i] = $i;
				}

				$core->labelFor('quantity', 'Quantity:');
				$core->selectBox('quantity', $quantity, 'greySelect', false, $_POST);
			?>
			<span class="memberSubTitle">
				<span style="color: #cb202a;">Note:</span> When calling to schedule your race date(s), the race pass number(s) and driver name(s) will need to be provided at that time.
			</span>
			
			<div class="clearfix"></div>
			
			<hr />
			<h3>Credit Card Information:</h3>
			<fieldset>
			<?php 
				$core->labelFor('cardNum', 'Card Number:', true);
				$core->input('text', 'cardNum', 'form-control input-lg', '', null, true, 16);
				
				$core->labelFor('ExpM', 'Expiration Date:', true);
				$core->selectBox('ExpM', $core->cardMonths(), 'selectDate', false, null, true);
				$core->selectBox('ExpY', $core->cardYears(), 'selectDate right', false, null, true);
				
				$core->labelFor('cardCVV2', 'CVV2 Code:', true);
				$core->input('text', 'cardCVV2', 'form-control input-lg', '', null, true, 4);
			?>
			</fieldset>
			
			<hr />
			<h2 style="margin: 0;">Order Summary:</h2>
			<hr />
			<table class="summary">
				<tr>
					<td>Race Pass(es):</td>
					<td class="passQuantity">1</td>
				</tr>
				<tr>
					<td>Amount To Organization:</td>
					<td class="orgSplit">$<span>50.00</span></td>
				</tr>
				<tr>
					<td style="color: #cb202a;">Grand Total:</td>
					<td class="orderTotal">$<span>150.00</span></td>
				</tr>
			</table>

      <hr />

      <div style="float: left;">
        <?php echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, null, true); ?>
      </div>

			<?php $core->submitBtn('Submit'); ?>
		</div>
	</form>
<?php } else { ?>
	<div class="container form">
		<div class="laFundProcessed">
			<h2>
				Thank you!<br />
				Your race pass(es) have been emailed to you.
			</h2>
			<hr />
			<span class="numbers">
				Your race pass number(s):<br />
				<?php echo implode(', ', $passes); ?>
			</span><br />
			<hr />
			<span class="subText" style="font-size: 20px;">
				<span style="color: #cb202a;">Thank you for supporting:</span><br />
				<?php echo $LAFundRacing->get_fund_organization_name($_POST['organization']) .' - '. $LAFundRacing->get_fund_division_name($_POST['division']); ?>
			</span><br />
			<hr />
			<span class="subText">
				Please save your race pass number(s) and feel free to contact us at (888)LA-RACING to schedule your race date.<br />
				If you don't receive your race pass(es), please check your spam folder.
			</span>
		</div>
	</div>
<?php } ?>
	<!--END MAIN CONTENT-->
	
<?php 
	if($displayError) {
		$core->flashNotice($errorMsg, true);
	}
?>
	
	<script type="text/javascript">
		<?php echo 'var organizations = '; print_r($LAFundRacing->active_fund_organizations('json')); ?>
	</script>
	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>
<?php $mysqli->close(); ?>