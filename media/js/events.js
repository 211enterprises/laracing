/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/		
		
		//MOVING NAVIGATION
		$(function() {
			$(window).on('scroll', function() {
				var scrollPosition = $(document).scrollTop();
				if(scrollPosition > 124) {
					$('header section nav.mainNav').css("position","fixed").css("top","4px");
				} else {
					$('header section nav.mainNav').css("position","relative").css("top","0px");
				}
			});
		});
		
		
		//CALENDAR BOOKING
		$(document).ready(function() {
		
			$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
				var showCalDate = this.id;
				$.ajax({
					url: "/ajax/calendar.php",
					data: "d="+showCalDate,
					success: function(data) {
						$('div.calendarContainer').html("");
						$('div.calendarContainer').append(data);
					}
				});
				return false;
			});
		
		});
		
		
		//INPUT OPACITY
		$('input,textarea').live("focus", function() {
			$(this).animate({
				opacity: "0.5"
			},200);
		});
		$('input,textarea').live("blur", function() {
			$(this).animate({
				opacity: "0.3"
			},200);
		});
		
		
		//MATCH DIVIDE HEIGHT TO TALLEST COLUMN
		$(function() {
			var leftColHeight = $('section.scheduleEvent div.leftCol').height(),
				rightColHeight = $('section.scheduleEvent div.rightCol').height();
			if(leftColHeight > rightColHeight) {var hrHeight = leftColHeight;} else {var hrHeight = rightColHeight;}
			$('section.scheduleEvent hr.divideVertical').css("height",hrHeight+"px");
		});
		
		
		$(function() {
	
		//CALENDAR SHOW DETAILS
		var mouseX, mouseY;
		var classInfo = $('div.calendarClassInfo'); 
		
		$('td.available').live("mouseenter", function() {
			var classDate = this.id;
			if(classDate != "") {
				classInfo.html('');
				classInfo.html('<div class="loading"></div>');
				$(this).mousemove(function(e) {
					mouseX = e.pageX; 
					mouseY = e.pageY;
					classInfo.css("top",mouseY+"px");
					classInfo.css("left",mouseX+"px");
				});
				classInfo.show();
				$.ajax({
					url: "/ajax/classesDetails.php",
					data: "d="+classDate,
					success: function(data) {
						classInfo.html("");
						classInfo.append(data);
					}
				});
			}
		});
		
		classInfo.live("mouseleave", function() {
			classInfo.hide();
		});
		
	});
	
	$('button').live("click",function() {
		var eventName = $("input[name=eventName]");
		var eventEmail = $("input[name=eventEmail]");
		var eventPhone = $("input[name=eventPhone]");
		var eventPartyNumber = $("input[name=eventPartyNumber]");
		var eventDate = $("input[name=eventDate]");
		var eventNotes = $("textarea#eventNotes");
			
		if(eventName.val() == "") {eventName.css("background","red"); eventName.css("color","black");} else {eventName.css("background","black"); eventName.css("color","#919191");}
		if(eventEmail.val() == "") {eventEmail.css("background","red"); eventEmail.css("color","black");} else {eventEmail.css("background","black"); eventEmail.css("color","#919191");}
		if(eventPhone.val() == "") {eventPhone.css("background","red"); eventPhone.css("color","black");} else {eventPhone.css("background","black"); eventPhone.css("color","#919191");}
		if(eventPartyNumber.val() == "") {eventPartyNumber.css("background","red"); eventPartyNumber.css("color","black");} else {eventPartyNumber.css("background","black"); eventPartyNumber.css("color","#919191");}
		if(eventDate.val() == "") {eventDate.css("background","red"); eventDate.css("color","black");} else {eventDate.css("background","black"); eventDate.css("color","#919191");}
		if(eventNotes.val() == "") {eventNotes.css("background","red"); eventNotes.css("color","black");} else {eventNotes.css("background","black"); eventNotes.css("color","#919191");}
		
		if(eventName.val() != "" && eventEmail.val() != "" && eventPartyNumber.val() != "" && eventDate.val() != "") {
			$('div.rightCol fieldset').html('<div style="position:relative; float:left; width:100%; text-align:center; margin:160px 0px;"><img src="/media/images/ajaxLoader.gif" alt="Loading" /></div>');
			$.ajax({
				url: "/ajax/events.php",
				data: "n="+encodeURIComponent(eventName.val())+"&e="+encodeURIComponent(eventEmail.val())+"&p="+encodeURIComponent(eventPhone.val())+"&pn="+encodeURIComponent(eventPartyNumber.val())+"&d="+encodeURIComponent(eventDate.val())+"&notes="+encodeURIComponent(eventNotes.val()),
				success: function(data) {
					$('div.rightCol fieldset').html("");
					$('div.rightCol fieldset').append(data);
				}
			});
		}
		
		return false;
	});
		
		