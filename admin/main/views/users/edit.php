<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$adminQuery = $mysqli->query("SELECT * FROM `LARX_admins` WHERE `id` = '$id' LIMIT 1");
	if($adminQuery->num_rows == 1) {
		$admin = $adminQuery->fetch_array();
		
		if(!isset($_POST['updateUser'])) {
			echo '<h1>Edit User:</h1>';
			echo '<a href="/admin/main/users/delete/'.$admin['id'].'" class="deleteContent">Delete This User</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">User Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateUser'])) {
		$firstName = makeSQLSafe($mysqli,$_POST['firstName']);
		$lastName = makeSQLSafe($mysqli,$_POST['lastName']);
		$username = makeSQLSafe($mysqli,$_POST['username']);
		$pass = makeSQLSafe($mysqli,$_POST['password']);
		$accessLevel = makeSQLSafe($mysqli,$_POST['accessLevel']);
		$updateUser = makeSQLSafe($mysqli,$_POST['updateUser']);
		
		if($_POST['password'] != "") {
			$password = SHA1($username.SHA1($pass));
			$mysqli->query("UPDATE `LARX_admins` SET `first_name` = '$firstName', `last_name` = '$lastName', `username` = '$username', `password` = '$password', `access_level` = '$accessLevel' WHERE `id` = '$updateUser' LIMIT 1");
		}

	}
	
	$accessQuery = $mysqli->query("SELECT * FROM `LARX_admin_access` ORDER BY `id` ASC");
?>

<?php if(!isset($_POST['updateClass'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>First Name:</label>
			<input type="text" name="firstName" id="firstName" value="<?php echo $admin['first_name']; ?>" />
		
			<label>Last Name:</label>
			<input type="text" name="lastName" id="lastName" value="<?php echo $admin['last_name']; ?>" />
			
			<label>Username:</label>
			<input type="text" name="username" id="username" value="<?php echo $admin['username']; ?>" />
			
			<label>Password:</label>
			<input type="password" name="password" id="password" />
			
			<label>Access Level:</label>
			<select name="accessLevel" id="accessLevel">
				<option value="">Select:</option>
		<?php while($access = $accessQuery->fetch_array()) { ?>
				<option value="<?php echo $access['id']; ?>" <?php if($admin['access_level'] == $access['id']) echo 'selected="selected"'; ?>><?php echo $access['access_name']; ?></option>
		<?php } ?>
				
			</select>
			
			<button type="submit">Edit User</button>
		</fieldset>
		<input type="hidden" name="updateUser" value="<?php echo $admin['id']; ?>" />
	</form>	
<?php
	} else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">User Has Been Updated</div>';

$adminQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/users.js"></script>
