<?php

/*
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
#  
# 2009-10-14  -  ssx_pass.php
#  script for "CREATING" a PDF for oaoer race passes to be delivered to customer.  Can be used
#  in automation or as the target for an html based web form.
#  Makes use of the fPDF package, pulls a PDF template file based on the data passed and template
#  file listed in the DB. Then simply adds customer specific data in the appropriate locations.
#  
# 2009-10-19 
#  This Script relies on a CONSTANT form base. Discovered today that Illustrator CS2 will change
#  the margin info of files created/last saved by CS3. Notably the top margin gets about 5pts 
#  added to it, shifting everything down, causing the text added here to appear shifted up.
#  So, the template files were saved as follows:
#   from CS3, save as PDF version 1.5 [Acrobat 6]. same options in CS2 tweak the form?!?
#  
#  
#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#
*/
// sending header info interferes with the PDF creating...
//session_start();
//if(empty($_SESSION['userId'])){
//    header('location: ../login/login.php?ref='.$_SERVER['PHP_SELF']);
//    echo "\$_SESSION['userId'] error";
//}
//echo "<a href=\"/login/logout.php?ref=".$_SERVER['PHP_SELF']."\">Log Out</a>";

require_once($_SERVER['DOCUMENT_ROOT'].'/passes/fpdf.php'); 
require_once($_SERVER['DOCUMENT_ROOT'].'/passes/fpdi.php');
require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");

$passID = makeSQLSafe($mysqli,$_GET['id']);

$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `pass_hash` = '$passID' LIMIT 1");

if($passQuery->num_rows == 1) {
	$pass = $passQuery->fetch_array();
	$classID = $pass['class_id'];
	if($pass['driver_name'] != "") $name = $pass['driver_name']; else $name = $pass['buyer_name'];
	$expireDate = strtotime(date("Y-m-d", strtotime($pass['timestamp'])) . " +1 year");
	if($classID == "" || $classID == 0) {
		$classDate = "TBD";
		$classTime = "TBD";
	} else {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$classID' LIMIT 1");
		$class = $classQuery->fetch_array();
		$classDate = date("m-d-Y", strtotime($class['date']));
		$classTime = date("h:i A", strtotime($class['time']));
	}
} else {
	exit;
}

$pdf =& new FPDI();
$race_pass_template = 'templates/Adv20.pdf';

$pagecount = $pdf->setSourceFile( $race_pass_template ); 
$tplidx = $pdf->importPage(1, '/ArtBox'); 
 
$pdf->addPage(); 
$pdf->useTemplate($tplidx, 2, 1, 214); 
$pdf->AddFont('BankGothicBT-Medium','','bankgothicmedium.php');
$pdf->SetFont('BankGothicBT-Medium','',18);
$pdf->SetXY(126,54);
$pdf->Cell(86 , 10 , $name, 0, 0, 'C', false);
$pdf->SetFont('BankGothicBT-Medium','',16);
$pdf->SetXY(126,65);

$pdf->Cell(86 , 10 , $classDate, 0, 0, 'C', false);

$pdf->SetXY(194,69);
//$printed_time = date("g:i a", strtotime( $_POST['class_time'] )); # adds AM/PM to the time
$printed_time = date("h:i A", strtotime( $class['time'] ));
$pdf->Cell(16 , 10 , $classTime, 0, 0, 'C', false);

$pdf->SetXY(39,109);
$pdf->Cell(65 , 10 , date("m-d-Y",$expireDate), 0, 0, 'C', false);
$pdf->SetXY(39,114);
$pdf->Cell(65 , 10 , $pass['pass_number'], 0, 0, 'C', false);

$file_name = "LARX Race Pass ".$name."-".$pass['pass_number'].".pdf";
//$file_name = "LARX Race Pass ".$_POST['pass_num']."-".$_POST['driver_name']."-[".time()."]-".$_POST['race_pass'].".pdf";
	 
$pdf->Output("race_passes/".$file_name, 'F'); //saves as a file on the server
$pdf->Output($file_name, 'D'); //forces a download in the browser

?> 