<?php
	$faqQuery = $mysqli->query("SELECT * FROM `LARX_faq` ORDER BY `order` ASC");
?>

	<h1>Frequently Asked Questions</h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=faq&action=add" class="addContent">Add a FAQ</a>
	
<?php if($faqQuery->num_rows > 0) { ?>
	<ul class="faq">
<?php while($faq = $faqQuery->fetch_assoc()) { ?>
		<li id="larxFAQ_<?php echo $faq['id']; ?>">
			<a href="<?php echo ADMIN_ROOT; ?>/faq/edit/<?php echo $faq['id']; ?>/">
				<div class="question"><?php echo $faq['question']; ?></div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current FAQ</div>';

$faqQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/faq.js"></script>