<?php
	
	class LAFundAdmin extends LAFundRacing {
		
		function __construct($connection) {
			$this->db = $connection;
		}
		
		public function authorize_user($data) {
			//Not using a database to grab different admin accounts. Only using one default set of credentials.
			$sysUser = 'lafradmin';
			$sysPass = 'raiseFunds2013';
			$username = makeSQLSafe($this->db, $data['username']);
			$password = makeSQLSafe($this->db, $data['password']);
			
			if($username === $sysUser && $password === $sysPass) {
				$this->set_admin_session();
				return true;
			}
			
			return false;
		}
		
		public function check_admin_access() {			
			if(LAFR_ADMIN_SESSION != 1) {
				header('Location: '.LAFR_ADMIN_ROOT.'/');
				exit;
			}
		}
		
		public function admin_logout() {
			session_destroy();
			header('Location: '.LAFR_ADMIN_ROOT.'/');
			exit;
		}
		
		public function add_organization($data) {
			if(isset($data['organization'])) {
				if($this->save_organization_data($data)) {
					header('Location: '.LAFR_ADMIN_ROOT.'/organizations/');
					exit;
				}
			}
		}
		
		public function edit_organization($data) {
			if(isset($data['organization']) && is_numeric($data['organization'])) {
				if($this->modifiy_organization_data($data)) {
					header('Location: '.LAFR_ADMIN_ROOT.'/organizations/');
					exit;
				}
			}
		}
		
		public function add_organization_group($data) {
			if(isset($data['organization']) && is_numeric($data['organization'])) {
				if($this->save_group_data($data)) {
					$divID = $this->get_last_division_id();
					if($this->save_group_payable_data($divID, $data)) {
						header('Location: '.LAFR_ADMIN_ROOT.'/organizations/');
						exit;	
					}
				}
			}
		}
		
		public function edit_organization_group($data) {
			if(isset($data['group']) && is_numeric($data['group'])) {
				if($this->modifiy_group_data($data)) {
					if($this->modifiy_group_payable_data($data['group'], $data)) {
						header('Location: '.LAFR_ADMIN_ROOT.'/organizations/');
						exit;	
					}
				}
			}
		}
		
		public function generate_division_report($id) {
			$orgObj = $this->get_organization_by_group_id($id);
			$groupObj = $this->get_fund_division_by_id($id);
			$paymentObj = $this->get_division_payable_by_id($id);
			$transObj = $this->get_transactions_by_id($id);
			
			$report = array();
			if($groupObj->num_rows == 1) {
				// Push organization information into array
				$report['organization'] = array($orgObj->fetch_array());
				// Push group information into array
				$report['group'] = array($groupObj->fetch_array());
				// Push group payment information into array
				$report['payment'] = array($paymentObj->fetch_array());
				// Push transactions for division/group
				for($i = 1; $i <= $transObj->num_rows; $i++) {
					$report['transactions'][$i] = $transObj->fetch_assoc(); 
				}
			}
			
			return $report;
		}
		
		public function get_division_payable_by_id($id) {
			$result = $this->db->query("SELECT * FROM `LAFR_division_payable` WHERE `division_id` = '$id' LIMIT 1");
			
			return $result;
			$result->close();
		}
		
		public function generate_daily_income($date) {
			$incomeObj = $this->get_daily_income($date);
			$income = array();
			$income['transactions'] = array();
			$income['total'] = array();
			
			//Push transactions to array
			while($trans = $incomeObj->fetch_array()) {
				array_push($income['transactions'], $trans);
				array_push($income['total'], $trans['quantity']);
			}
			
			return $income;
		}
		
		public function get_total_sales() {
			$result = $this->db->query("SELECT SUM(pass_quantity) FROM `LAFR_transactions`");
			
			return $result->fetch_array()[0];
			$result->close();
		}
		
		public function get_organization_total_sales($id) {
  		$result = $this->db->query("select 
        divgroup.id as group_id,
        trans.id as transaction_id,
        trans.pass_quantity as pass_quantity,
        trans.created_at as purchased_at
        from LAFR_divisions as divgroup,
        LAFR_transactions as trans
        WHERE divgroup.org_id = $id AND divgroup.id = trans.division_id");
      
      return $result;
      $result->close();
		}
		
		public function get_group_total_sales($id) {
  		$result = $this->db->query("SELECT SUM(pass_quantity) FROM `LAFR_transactions` WHERE `division_id` = '$id'");
			
			return $result->fetch_array()[0];
			$result->close();
		}
		
		public function modifiy_group_notes($id, $group) {
  		$groupNotes = makeSQLSafe($this->db, $group['groupNotes']);
			$commissionNotes = makeSQLSafe($this->db, $group['commissionNotes']);
			
			$save = $this->db->query("UPDATE `LAFR_division_payable` SET `payable_notes` = '$groupNotes', `commission_notes` = '$commissionNotes' WHERE `division_id` = '$id' LIMIT 1");
			if($save) {
  			return true;
			}
  		return false;
  		$save->close();
		}
		
		protected function get_daily_income($date) {
			$result = $this->db->query("SELECT 
			org.name,
			SUM(trans.pass_quantity) as quantity,
			trans.created_at 
			FROM LAFR_transactions as trans, LAFR_divisions as groups, LAFR_organizations as org WHERE 
			trans.division_id = groups.id AND groups.org_id = org.id AND trans.created_at 
			BETWEEN '".$date." 00:00:00.000' AND '".$date." 23:59:59.997' 
			group by org.id");
			
			return $result;
			$result->close();
		}
		
		protected function get_last_division_id() {
			$result = $this->db->query("SELECT `id` FROM `LAFR_divisions` ORDER BY `id` DESC LIMIT 1");
			
			return $result->fetch_array()['id'];
			$result->close();
		}
		
		private function get_transactions_by_id($id) {
			$result = $this->db->query("SELECT * FROM `LAFR_transactions` WHERE `division_id` = '$id' ORDER BY `member_name` ASC");
			
			return $result;
			$result->close();
		}
		
		private function save_organization_data($organization) {
			$catID = makeSQLSafe($this->db, $organization['category']);
			$name = makeSQLSafe($this->db, $organization['name']);
			$email = makeSQLSafe($this->db, $organization['email']);
			$address = makeSQLSafe($this->db, $organization['address']);
			$city = makeSQLSafe($this->db, $organization['city']);
			$state = makeSQLSafe($this->db, $organization['state']);
			$zip = makeSQLSafe($this->db, $organization['zip_code']);
			$phone = makeSQLSafe($this->db, $organization['phone']);
			$mission = makeSQLSafe($this->db, $organization['mission_statement']);
			$repName = makeSQLSafe($this->db, $organization['rep_name']);
			$EIN = makeSQLSafe($this->db, $organization['EIN']);
			$nonprofit = makeSQLSafe($this->db, $organization['non_profit']);
			$active = makeSQLSafe($this->db, $organization['active']);
			
			$save = $this->db->query("INSERT INTO `LAFR_organizations` (`category_id`,`name`,`email`,`address`,`city`,`state`,`zip_code`,`phone`,`mission_statement`,`rep_name`,`EIN`,`non-profit`,`active`,`created_at`) VALUES ('$catID','$name','$email','$address','$city','$state','$zip','$phone','$mission','$repName','$EIN','$nonprofit','$active',NOW())");
			if($save) {
				return true;
			}
			
			return false;
		}
		
		private function modifiy_organization_data($organization) {
			$org = makeSQLSafe($this->db, $organization['organization']);
			$catID = makeSQLSafe($this->db, $organization['category']);
			$name = makeSQLSafe($this->db, $organization['name']);
			$email = makeSQLSafe($this->db, $organization['email']);
			$address = makeSQLSafe($this->db, $organization['address']);
			$city = makeSQLSafe($this->db, $organization['city']);
			$state = makeSQLSafe($this->db, $organization['state']);
			$zip = makeSQLSafe($this->db, $organization['zip_code']);
			$phone = makeSQLSafe($this->db, $organization['phone']);
			$mission = makeSQLSafe($this->db, $organization['mission_statement']);
			$repName = makeSQLSafe($this->db, $organization['rep_name']);
			$EIN = makeSQLSafe($this->db, $organization['EIN']);
			$nonprofit = makeSQLSafe($this->db, $organization['non_profit']);
			$active = makeSQLSafe($this->db, $organization['active']);
			
			$save = $this->db->query("UPDATE `LAFR_organizations` SET `category_id`='$catID', `name`='$name', `email`='$email', `address`='$address', `city`='$city', `state`='$state', `zip_code`='$zip', `phone`='$phone', `mission_statement`='$mission', `rep_name`='$repName', `EIN`='$EIN', `non-profit`='$nonprofit', `active`='$active' WHERE `id` = '$org' LIMIT 1");
			if($save) {
				return true;
			}
			
			return false;
		}
		
		private function save_group_data($group) {
			$orgID = makeSQLSafe($this->db, $group['organization']);
			$name = makeSQLSafe($this->db, $group['name']);
			$startDate = makeSQLSafe($this->db, $group['campaignStart']);
			$endDate = makeSQLSafe($this->db, $group['campaignEnd']);
			$contactName = makeSQLSafe($this->db, $group['contactName']);
			$contactEmail = makeSQLSafe($this->db, $group['contactEmail']);
			$contactPhone = makeSQLSafe($this->db, $group['contactPhone']);
			$rep = makeSQLSafe($this->db, $group['repName']);
			
			$save = $this->db->query("INSERT INTO `LAFR_divisions` (`org_id`,`name`,`campaign_start`,`campaign_end`,`contact_name`,`contact_email`,`contact_phone`,`rep_name`,`created_at`) VALUES ('$orgID','$name','$startDate','$endDate','$contactName','$contactEmail','$contactPhone','$rep',NOW())");
			if($save) {
				return true;
			}
			
			return false;
		}	
		
		private function modifiy_group_data($group) {
			$groupID = makeSQLSafe($this->db, $group['group']);
			$name = makeSQLSafe($this->db, $group['name']);
			$startDate = makeSQLSafe($this->db, $group['campaignStart']);
			$endDate = makeSQLSafe($this->db, $group['campaignEnd']);
			$contactName = makeSQLSafe($this->db, $group['contactName']);
			$contactEmail = makeSQLSafe($this->db, $group['contactEmail']);
			$contactPhone = makeSQLSafe($this->db, $group['contactPhone']);
			$rep = makeSQLSafe($this->db, $group['repName']);
			$mission = makeSQLSafe($this->db, $group['mission']);
			
			$save = $this->db->query("UPDATE `LAFR_divisions` SET `name`='$name', `campaign_start`='$startDate', `campaign_end`='$endDate', `contact_name`='$contactName', `contact_email`='$contactEmail', `contact_phone`='$contactPhone', `rep_name`='$rep', `mission_statement`='$mission' WHERE `id` = '$groupID' LIMIT 1");
			if($save) {
				return true;
			}
			
			return false;
		}	
		
		private function save_group_payable_data($id, $group) {
			$attn = makeSQLSafe($this->db, $group['payAttn']);
			$name = makeSQLSafe($this->db, $group['payName']);
			$address = makeSQLSafe($this->db, $group['payAddress']);
			$city = makeSQLSafe($this->db, $group['payCity']);
			$state = makeSQLSafe($this->db, $group['payState']);
			$zip = makeSQLSafe($this->db, $group['payZip']);
			
			$save = $this->db->query("INSERT INTO `LAFR_division_payable` (`division_id`,`attention`,`payable_name`,`address`,`city`,`state`,`zip_code`) VALUES ('$id','$attn','$name','$address','$city','$state','$zip')");
			if($save) {
				return true;
			}
			
			return false;
		}
		
		private function modifiy_group_payable_data($id, $group) {
			$attn = makeSQLSafe($this->db, $group['payAttn']);
			$name = makeSQLSafe($this->db, $group['payName']);
			$address = makeSQLSafe($this->db, $group['payAddress']);
			$city = makeSQLSafe($this->db, $group['payCity']);
			$state = makeSQLSafe($this->db, $group['payState']);
			$zip = makeSQLSafe($this->db, $group['payZip']);
			
			$save = $this->db->query("UPDATE `LAFR_division_payable` SET `attention`='$attn', `payable_name`='$name', `address`='$address', `city`='$city', `state`='$state', `zip_code`='$zip' WHERE `division_id` = '$id' LIMIT 1");
			if($save) {
				return true;
			}
			
			return false;
		}
		
		private function set_admin_session() {
			$_SESSION['LAFR_ADMIN'] = 1;
		}
	}
?>