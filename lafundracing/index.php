<?php
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	
	$displayError = false;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/shadowbox.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript" src="/media/js/shadowbox/shadowbox.js"></script>
<script type="text/javascript">
	Shadowbox.init();
</script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('partials/header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container splash">
		<h1>Welcome To LA Fund Racing</h1>
		<hr />
				
		<p>
			LA Racing, the number one racing experience in the country, is teaming up with some awesome schools, sports programs, non profit and other organizations that need to raise money, and has created 'LA FUND RACING'!<br /><br />
			This could very well be the easiest and most lucrative fund raising opportunity your organization has ever participated in! It's 100% fully automated and requires no order forms, paperwork, or collecting cash or checks. It's all done through this website, and we do all the work! Simply call us so we can enter your organization into our system. Then have your participants spread the word to their friends and family encouraging them to purchase a race pass for themselves or as a gift. They will pay the lowest price available, have the time of their lives, and your group will earn funds quickly!<br /><br />
			See the videos below for details!<br /><br />
			<a href="//player.vimeo.com/video/78581577?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" class="btn btn-link" title="LA Racing at Irwindale Speedway" rel="shadowbox;width=405;height=340;">
				<img src="//img.youtube.com/vi/eVG5Cf5-kaY/2.jpg" alt="" />
			</a>
			<a href="//www.youtube.com/embed/eVG5Cf5-kaY?rel=0&autoplay=1" class="btn btn-link" title="L.A. Racing featured on NBC Today in L.A." rel="shadowbox;width=405;height=340;">
				<img src="//img.youtube.com/vi/eVG5Cf5-kaY/1.jpg" alt="" />
			</a>
		</p>
		
		<hr />
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-6">
				<a href="http://www.laracing.com" target="_blank" class="btn btn-block btn-red">Visit the LA Racing Website</a>
			</div>
			<div class="col-md-6">
				<a href="/lafundracing/register/" class="btn btn-block btn-red">Organization Sign Up</a>
			</div>
		</div>
		
		<hr />
		<iframe width="100%" height="500" src="//www.youtube.com/embed/YnaLUEkQICc?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
		<hr />
		
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 choose">
				<a href="https://www.laracing.com/lafundracing/purchase.php" class="btn btn-block btn-red">Purchase Tickets</a>
			</div>
		</div>
		
		<hr />
		
		<div class="row">
			<div class="col-md-12">
				<a href="/lafundracing/downloads/" class="btn btn-link">Download Marketing Materials</a>
			</div>
		</div>
	</div>	
	
	<div class="racingOverlay">
		<a href="" class="closeRacing">Close Racing</a>
		<div class="container contact">
			<h4>Sign Up your organization to begin your own<br />LA Fund Racing campaign!</h4>
			<h5>Give us a call today.</h5>
			<h6>1-855-RACE-4-FUNDS</h6>
		</div>
	</div>

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>
<?php $mysqli->close(); ?>