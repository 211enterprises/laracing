<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->check_admin_access();
	
	$orgObj = $LAFundRacing->fund_organization_by_id($_GET['org']);
	
	// Redirect to admin if organization is not found
	if($orgObj->num_rows != 1) {
		header('Location: '.LAFR_ADMIN_ROOT);
		exit;
	} else {
		$org = $orgObj->fetch_array();
	}
	
	// Submit new group
	$LAFundAdmin->add_organization_group($_POST);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/datepicker.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript">
	$(function() {
		$('input#campaignStart').datepicker();
		$('input#campaignEnd').datepicker();
	});
</script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1>Add Group To <?php echo $org['name']; ?>:</h1>
		<hr />
		
		<form action="" method="post">
			<div class="row">
				<div class="col-md-12">
					<label for="name">Name:</label>
					<input type="text" name="name" id="name" class="LAFRAdmin" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Campaign Start Date:</label>
					<input type="text" name="campaignStart" id="campaignStart" data-date-format="yyyy-mm-dd" class="LAFRAdmin" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Campaign End Date:</label>
					<input type="text" name="campaignEnd" id="campaignEnd" data-date-format="yyyy-mm-dd" class="LAFRAdmin" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Contact Name:</label>
					<input type="text" name="contactName" id="contactName" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Contact Email Address:</label>
					<input type="text" name="contactEmail" id="contactEmail" class="LAFRAdmin" />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Contact Phone:</label>
					<input type="text" name="contactPhone" id="contactPhone" class="LAFRAdmin" />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Representative Name:</label>
					<input type="text" name="repName" id="repName" class="LAFRAdmin" />
				</div>
			</div>
			
			<hr />
			
			<h3>Payment Information:</h3>
			
			<div class="row">
				<div class="col-md-6">
					<label for="campaignStart">Attn:</label>
					<input type="text" name="payAttn" id="payAttn" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Payable Name:</label>
					<input type="text" name="payName" id="payName" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Address:</label>
					<input type="text" name="payAddress" id="payAddress" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">City:</label>
					<input type="text" name="payCity" id="payCity" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">State:</label>
					<input type="text" name="payState" id="payState" class="LAFRAdmin" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Zip Code:</label>
					<input type="text" name="payZip" id="payZip" class="LAFRAdmin" required />
				</div>
				<div class="col-md-12 pull-right">
					<button type="submit">Add Group</button>
				</div>
			</div>
			<input type="hidden" name="organization" value="<?php echo $org['id']; ?>" />
		</form>
	</div>

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>