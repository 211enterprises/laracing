<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['d'])) {
		$date = makeSQLSafe($mysqli,$_GET['d']);
		$today = date("Y-m-d");
		
		$dateQuery = $mysqli->query("SELECT * FROM `LARX_track_locations`,`LARX_class_dates` WHERE LARX_track_locations.track_id = LARX_class_dates.track_location AND LARX_class_dates.date = '$date' ORDER BY LARX_class_dates.date ASC");
		if($dateQuery->num_rows > 0) {
			while($class = $dateQuery->fetch_assoc()) {
				$spotsTaken = $class['slots_taken'];
				$classLimit = $class['class_limit'];
			if($spotsTaken < $classLimit) { ?>
				<a href="/startracing/<?php echo $class['class_hash']; ?>/" class="classDetailContainer" onclick="return false" style="cursor: default;">
					<!--<div class="remaining">
						<?php //echo $classLimit - $spotsTaken; ?><br /><span>spots remaining</span>
					</div>-->
					<div class="dateLocation">
						<?php echo date("M jS Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?><br />
						<span><?php echo substr($class['track_name'],0,26); ?></span>
					</div>
					<div class="bookNow">CALL NOW FOR AVAILABILITY!!!</div>
				</a>
		<?php } elseif($class['date'] < $today) { ?>
				<a href="" class="classDetailContainer" onclick="return false">
					<div class="remainingPast">
						This Class Has Already Ran
					</div>
					<div class="dateLocation">
						<?php echo date("M jS Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?><br />
						<span><?php echo substr($class['track_name'],0,26); ?></span>
					</div>
				</a>
		<?php } else { ?>
				<a href="" class="classDetailContainer" onclick="return false">
					<!--<div class="remaining">
						0<br /><span>spots remaining</span>
					</div>-->
					<div class="dateLocation">
						<?php echo date("M jS Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time'])); ?><br />
						<span><?php echo substr($class['track_name'],0,26); ?></span>
					</div>
					<div class="bookNow">CLASS IS FULL</div>
				</a>
		<?php } ?>
<?php
			}
		} else echo '<div style="position:relative; float:left; width:100%; color:black; font-size:20px; text-align:center; margin:50px 0px;">No Classes Found</div>';
		
	} else {
		exit;
	}

	$dateQuery->close();
	$mysqli->close();
?>