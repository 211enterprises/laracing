<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	//require($_SERVER['DOCUMENT_ROOT']."/php_includes/admin.inc");
	
	$admin = new admin;
	
	//$admin->access($mysqli,$_SESSION['adminID'],4);
	
	$adminQuery = $mysqli->query("SELECT * FROM `LARX_admins` ORDER BY `first_name` ASC");
?>

	<h1>Admin Users:</h1>
	<a href="/admin/main/?controller=users&action=add" class="addContent">Add a User</a>
	
<?php if($adminQuery->num_rows > 0) { ?>
	<ul class="classes">
<?php while($admin = $adminQuery->fetch_assoc()) { ?>
		<li>
			<a href="/admin/main/?controller=users&action=edit&id=<?php echo $admin['id']; ?>">
				<div class="location" style="font-size:18px;"><?php echo $admin['first_name']." ".$admin['last_name']; ?></div>
				<div class="edit">Edit User</div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Users</div>'; ?>