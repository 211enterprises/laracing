<?php
	
	header("Location: /");
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$template = new template;
	$booking = new booking;
	$content = new content;
	
	//QUERY POLICIES
	$policyQuery = $mysqli->query("SELECT * FROM `LARX_policies` LIMIT 1");
	$policy = $policyQuery->fetch_array();
	
	if(isset($_GET['class'])) {
		$hash = makeSQLSafe($mysqli,$_GET['class']);
		$today = date("Y-m-d");

		$classQuery = $mysqli->query("SELECT * FROM LARX_classes, LARX_class_dates, LARX_track_locations WHERE LARX_class_dates.class_hash = '$hash' AND LARX_classes.id = LARX_class_dates.class_id AND LARX_class_dates.track_location = LARX_track_locations.track_id AND LARX_class_dates.date >= '$today' LIMIT 1");
		if($classQuery->num_rows == 1) {
			$class = $classQuery->fetch_assoc();
			//VARIABLES
			$trackID = $class['track_id'];
			$trackName = $class['track_name'];
			$spotsTaken = $class['slots_taken'];
			$classLimit = $class['class_limit'];
			
			//CHECK IF BOOKED
			if($spotsTaken >= $classLimit) {
				header("Location: /");
				exit;
			}
			
			$getClassQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$hash' LIMIT 1");
			$getClass = $getClassQuery->fetch_array();
			$classID = $getClass['id'];
			$classTaken = $getClass['slots_taken'];
			
		} else {
			header("Location: /");
			exit;
		}
		
	} else {
		
	}
	
	//---------------------------------------SUBMIT ORDER--------------------------------------------
	if(isset($_POST['submitOrder'])) {
		
		$orderFirstName = makeSQLSafe($mysqli,$_POST['firstName']);
		$orderLastName = makeSQLSafe($mysqli,$_POST['lastName']);
		$orderName = $orderFirstName." ".$orderLastName;
		$orderPhone = makeSQLSafe($mysqli,$_POST['phone']);
		$orderEmail = makeSQLSafe($mysqli,$_POST['email']);
		$orderAddress = makeSQLSafe($mysqli,$_POST['address']);
		$orderCity = makeSQLSafe($mysqli,$_POST['city']);
		$orderState = makeSQLSafe($mysqli,$_POST['state']);
		$orderZip = makeSQLSafe($mysqli,$_POST['zip_code']);
		$orderNotes = makeSQLSafe($mysqli,$_POST['pass_notes']);
		$packageData = makeSQLSafe($mysqli,implode(",",$_POST['racePackage']));
		$videoData = makeSQLSafe($mysqli,implode(",",$_POST['raceVideo']));
		$totalPasses = makeSQLSafe($mysqli,$_POST['totalPasses']);
		$totalVideos = makeSQLSafe($mysqli,$_POST['totalVideos']);
		$orderTotal = number_format(makeSQLSafe($mysqli,$_POST['orderTotal']),2,"."," ");
		$newTakenSpots = $classTaken + count($_POST['racePackage']);
		$ccNumber = makeSQLSafe($mysqli,$_POST['billingCardNum']);
		$expDate = makeSQLSafe($mysqli,$_POST['billingCardExpMonth'].$_POST['billingCardExpYear']);
		$CVV2 = makeSQLSafe($mysqli,$_POST['billingCardCode']);
		$cardType = makeSQLSafe($mysqli,$_POST['billingCardType']);
		
		
		//ASSIGN ORDER ID
		$orderIDQuery = $mysqli->query("SELECT `id` FROM `LARX_orders` ORDER BY `id` DESC LIMIT 1");
		$getOrderID = $orderIDQuery->fetch_array();
		$orderID = $getOrderID['id'] + 1;
		
		
		//------------------------------MERCHANT INTEGRATION-------------------------------------------------------------------------------------------------------------------
		// By default, this sample code is designed to post to our test server for
		// developer accounts: https://test.authorize.net/gateway/transact.dll
		// for real accounts (even in test mode), please make sure that you are
		// posting to: https://secure.authorize.net/gateway/transact.dll
	
		$post_url = "https://secure.authorize.net/gateway/transact.dll";
	
		$post_values = array(
		// the API Login ID and Transaction Key must be replaced with valid values
		"x_login"			=> "4A73suHpeTh",
		"x_tran_key"		=> "9T4BCaB66hUj35EQ",
	
		"x_version"			=> "3.1",
		"x_delim_data"		=> "TRUE",
		"x_delim_char"		=> "|",
		"x_relay_response"	=> "FALSE",
	
		"x_type"			=> "AUTH_CAPTURE",
		"x_method"			=> "CC",
		"x_card_num"		=> "".$ccNumber."",
		"x_exp_date"		=> "".$expDate."",
		"x_card_code"		=> "".$CVV2."",
		"x_card_type"		=> "".$cardType."",
		
		"x_amount"			=> "".$orderTotal."",
		"x_description"		=> "LA Raxing X - Race Passes Purchase",
		
		"x_cust_id"			=> "".$orderID."",
		"x_first_name"		=> "".$orderFirstName."",
		"x_last_name"		=> "".$orderLastName."",
		"x_address"			=> "".$address."",
		"x_city"			=> "".$city."",
		"x_state"			=> "".$state."",
		"x_zip"				=> "".$zipCode."",
		"x_country"			=> "USA",
		
		"x_phone"			=> "".$orderPhone."",
		"x_email"			=> "".$orderEmail."",
		
		"x_notes"			=> "".$orderNotes."",
		
		
		// Additional fields can be added here as outlined in the AIM integration
		// guide at: http://developer.authorize.net
		);
	
		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
	
		// This sample code uses the CURL library for php to establish a connection,
		// submit the post, and record the response.
		// If you receive an error, you may want to ensure that you have the curl
		// library enabled in your php configuration
		$request = curl_init($post_url); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object
	
		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($post_values["x_delim_char"],$post_response);

		//-------------------------------------IF CARD IS APPROVED-------------------------------------//
		if($response_array[0] == 1) {
			
			
			$mysqli->query("INSERT INTO `LARX_orders` (`class_id`,`name`,`phone`,`email_address`,`address`,`city`,`state`,`zip_code`,`package_data`,`video_data`,`total_passes`,`total_videos`,`order_total`,`order_comments`,`timestamp`) 
			VALUES ('$classID','$orderName','$orderPhone','$orderEmail','$orderAddress','$orderCity','$orderState','$orderZip','$packageData','$videoData','$totalPasses','$totalVideos','$orderTotal','$orderNotes',NOW())");
			
			$lastID = $mysqli->insert_id;
			$passID = 0;
			
			foreach($_POST['racePackage'] as $pass) {
				$generatePass = generateRacePass($mysqli);
				$generateHash = generateRaceHash($mysqli);
				$mysqli->query("INSERT INTO `LARX_race_passes` (`class_id`,`buyer_name`,`track_location`,`pass_number`,`order_id`,`pass_hash`,`timestamp`) VALUES ('$classID','$orderName','$trackID','$generatePass','$lastID','$generateHash',NOW())");
				$raceVideo = $_POST['raceVideo'][$passID];
				$lastPass = $mysqli->insert_id;
				$mysqli->query("INSERT INTO `LARX_class_roster` (`class_id`,`racer_name`,`pass_id`,`racer_video`,`timestamp`) VALUES ('$classID','$orderName','$lastPass','$raceVideo',NOW())");
				$passID++;
			}
			
			//-------------------------------------UPDATE CLASS SPOTS TAKEN-------------------------------------//
			$mysqli->query("UPDATE `LARX_class_dates` SET `slots_taken` = '$newTakenSpots' WHERE `id` = '$classID' LIMIT 1");
			
			//---------------------------------------SEND CUSTOMER EMAIL----------------------------------------//
			$emailQuery = $mysqli->query("SELECT * FROM `LARX_orders` ORDER BY `id` DESC LIMIT 1");
			$email = $emailQuery->fetch_assoc();
			$generateEmail = file_get_contents("http://".$_SERVER['SERVER_NAME']."/php_includes/email_passes.php?order=".$email['id']);
			
			$subject = "Thank you for your order ".$orderFirstName;
			
			$headers = "From: LA Racing X <noreply@laracingx.com> \r\n";
			$headers .= "Reply-To: LA Racing X <noreply@laracingx.com> \r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			mail($orderEmail,$subject,$generateEmail,$headers);
				
				
			header("Location: /thankyou/");
			exit();
			
		}
		
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Racing X : Purchase Your Passes</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/slideshow.js"></script>
<script type="text/javascript" src="/media/js/racing.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

<?php echo $template->header(1); ?>

<!--MAIN CONTENT-->
<div class="content">

<?php $content->bookAClass($mysqli,$_GET['class'],$hash); ?>
	
</div>
<!--END MAIN CONTENT-->

<?php echo $template->footer($mysqli); ?>

<div class="voucherPopUp" style="z-index:503;<?php if($response_array[0] == 2 || $response_array[0] == 3 || $response_array[0] == 4) echo 'display:block;'; ?>">
	<?php if(isset($_POST['submitOrder'])) {
		switch($response_array[0]) {
			case 2:
				echo 'Your card was declined. Please try a different method or try again later.';
				break;
			case 3:
				echo 'An error has occurred. Please try again later. ';
				break;
			case 4:
				echo 'This purchase is being held for review. We will contact you as soon as possible.';
				break;
		}
	} ?>
</div>

</body>
</html>
<?php $mysqli->close(); ?>