<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$template = new template;
	$booking = new booking;
	
	//QUERY VOUCHER COMPANIES
	$voucherQuery = $mysqli->query("SELECT DISTINCT(company_name),voucher_url FROM `LARX_voucher_companies` ORDER BY `company_name` ASC");
	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>LA Racing X</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript">
	var tweetUsername = "LARacingX";
</script>
<script type="text/javascript" src="/media/js/slideshow.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript" src="/media/js/home.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

<?php echo $template->homeHeader(); ?>

<!--MAIN CONTENT-->
<div class="content">
	
	<!--WELCOME SECTION-->
	<section class="welcome">
		<!--LEFT COLUMN-->
		<div class="welcomeLeftCol">
			<img src="/media/images/homeRaceParticipants.png" alt="" />
		</div>
		
		<hr class="vertical" />
		
		<!--RIGHT COLUMN-->
		<div class="welcomeRightCol">
			<h1>Welcome To LA Racing</h1>
			<hr class="horizonal" />
			At last! You've found the Racing Experience you've been searching for! L.A. Racing is an on site facility for Race Fans and Thrill Seekers located just minutes from L.A. at the renowned Irwindale Speedway, the 'Crown Jewel' of short track racing! Come Drive a Stock Car at SPEED!!!!<br />
			Get them the most unforgettable gift EVER!!!<br /><br />
			
			Whether you are a first timer or have a little experience under your belt or maybe just a pure adrenaline junkie we have a racing Adventure Program that's right for you! We offer 20 lap Adventure Programs as well as Competition Courses for the more serious racer and unlike the other guys we have race dates available to our customers all year long!<br /><br />
			
			Don't want to race alone? Too much excitement to experience by yourself? Bring a buddy, bring a party or bring all of your hard working employees and treat them to the time of their lives. Whether you have a group of 10 or 100 our staff would love to help you customize a party or corporate event that's right for you and your group!<br /><br />
			So what makes us the best Race Training school around? We've got a national champion crew chief and racing coordinator, along with our other highly qualified instructors, top notch racing machines, friendly staff, competitive prices, flexibility and a genuine concern for our customer's satisfaction. It's no wonder our customers/students come back again and again!!

		</div>
	</section>
	<!--END WELCOME SECTION-->
	
	<!--TWITTER FEED-->
	<section class="twitter">
		<!--<hr />
		<div class="twitterContainer">
			<div class="twitterBG"></div>
			<div class="bird"></div>
			<div class="tweet"></div>
		</div>
		
		<div class="followTweets">Follow</div>-->
		<hr />
	</section>
	
	<!--CLIENT INTERACTION-->
	<section class="frontActionsContainer">
		
		<div class="frontActionsWrap">
			<h3>Start Racing</h3>
			<div class="calendarContainer">
				<?php echo $booking->calendar($mysqli,date("m"),date("Y")); ?>
			</div>
		</div>
		
		<hr class="vertical" />
		
		<div class="frontActionsWrap">
			<h3>Stay Up To Date</h3>
			<div class="newsletterError">Please enter a valid email address.</div>
			<fieldset id="newsletter">
				<input type="text" name="newsletterName" id="newsletterName" placeholder="Name" />
				<input type="text" name="newsletterEmail" id="newsletterEmail" placeholder="Email Address" />
				<input type="text" name="newsletterSource" id="newsletterSource" placeholder="How did you hear about us?" />
				Keep me up to date with the latest LA Racing news and events.
				<button type="submit" id="mailingSignUp" style="margin-top:30px;">Sign Up</button>
			</fieldset>
		</div>
		
		<hr class="vertical" />
		
		<div class="frontActionsWrap">
			<h3>Redeem Your Voucher</h3>
			<form id="startVoucherForm" action="/" method="post">
				<fieldset>
					<input type="text" name="voucherName" id="voucherName" placeholder="Full Name" />
					<input type="text" name="voucherEmail" id="voucherEmail" placeholder="Email Address" />
					<!--<input type="text" name="voucherNumber" id="voucherNumber" placeholder="Voucher Number" />-->
					<select name="voucherSource" id="voucherSource">
						<optgroup label="Select Your Voucher Source!">
							<option value="">Voucher Source:</option>
				<?php if($voucherQuery->num_rows > 0) {
						while($voucher = $voucherQuery->fetch_assoc()) {
							echo '<option value="'.$voucher['voucher_url'].'">'.$voucher['company_name'].'</option>';
						}
					} $voucherQuery->close(); ?>
						</optgroup>
					</select>
					<button type="submit" id="redeemVoucher">Continue</button>
				</fieldset>
			</form>
		</div>
		
	</section>
	
</div>
<!--END MAIN CONTENT-->

<?php echo $template->footer($mysqli); ?>

</body>
</html>
<?php $mysqli->close(); ?>