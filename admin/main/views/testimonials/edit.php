<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$testQuery = $mysqli->query("SELECT * FROM `LARX_testimonials` WHERE `id` = '$id' LIMIT 1");
	if($testQuery->num_rows == 1) {
		$testimonial = $testQuery->fetch_array();
		$testID = $testimonial['id'];
		
		if(!isset($_POST['updateTestimonial'])) {
			echo '<h1>Edit Testimonial:</h1>';
			echo '<a href="'.ADMIN_ROOT.'/?controller=testimonials&action=delete&id='.$testimonial['id'].'" class="deleteContent">Delete This Testimonial</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Sponsor Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateTestimonial'])) {
		$name = makeSQLSafe($mysqli,$_POST['clientName']);
		$location = makeSQLSafe($mysqli,$_POST['clientLocation']);
		$writtenTestimonial = makeSQLSafe($mysqli,$_POST['testimonial']);
		$testmonialType = makeSQLSafe($mysqli,$_POST['testimonialType']);
		$videoSource = makeSQLSafe($mysqli,$_POST['videoSource']);		
		$videoPath = makeSQLSafe($mysqli,$_POST['videoPath']);
		
		//ADD RECORED
		if($testimonial['testimonial_type'] == 0) {
			$mysqli->query("UPDATE `LARX_testimonials` SET `client_name`='$name', `client_location`='$location', `description`='$writtenTestimonial' WHERE `id` = '$testID' LIMIT 1");
		} elseif($testimonial['testimonial_type'] == 1) {
			$mysqli->query("UPDATE `LARX_testimonials` SET `client_name`='$name', `client_location`='$location', `video_source`='$videoSource', `video_url`='$videoPath' WHERE `id` = '$testID' LIMIT 1");
		}
	}
	
if(!isset($_POST['updateTestimonial'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset>
			<label>Client Name:</label>
			<input type="text" name="clientName" id="clientName" value="<?php echo $testimonial['client_name']; ?>" />
			<label>Client Location/Company:</label>
			<input type="text" name="clientLocation" id="clientLocation" value="<?php echo $testimonial['client_location']; ?>" />
		<?php if($testimonial['testimonial_type'] == 0) { ?>
				<label>Client Testimonial:</label>
				<textarea name="testimonial"><?php echo $testimonial['description']; ?></textarea>
		<?php } ?>
		<?php if($testimonial['testimonial_type'] == 1) { ?>
				<label>Video Type:</label>
				<select name="videoSource" id="videoSource">
					<option value="youtube" <?php if($testimonial['video_source'] == "youtube") echo 'selected="selected"'; ?>>YouTube</option>
					<option value="vimeo" <?php if($testimonial['video_source'] == "vimeo") echo 'selected="selected"'; ?>>Vimeo</option>
				</select>
				<label>Video ID: <span id="videoSample">http://www.youtube.com/watch?v=<span>12345678</span></span></label>
				<input type="text" name="videoPath" id="videoPath" value="<?php echo $testimonial['video_url']; ?>" />
		<?php } ?>
			<button type="submit">Update Testimonial</button>
		</fieldset>
		<input type="hidden" name="updateTestimonial" />
	</form>
	
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Testimonial Has Been Update</div>';

$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/testimonials.js"></script>