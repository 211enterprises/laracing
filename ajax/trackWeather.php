<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$weatherQuery = $mysqli->query("SELECT * FROM `LARX_track_locations`");

	if($weatherQuery->num_rows > 0) {
	while($weather = $weatherQuery->fetch_array()) {
		$weatherAPI = file_get_contents("http://free.worldweatheronline.com/feed/weather.ashx?query=".$weather['zip_code']."&format=json&num_of_results=1&key=d782871872023601122009",0,null,null);
		$weatherData = json_decode($weatherAPI, true);
		$tempF = $weatherData['data']['current_condition'][0]['temp_F'];
		$tempC = $weatherData['data']['current_condition'][0]['temp_C'];
		$weatherCode = $weatherData['data']['current_condition'][0]['weatherCode'];
		$weatherDesc = $weatherData['data']['current_condition'][0]['weatherDesc'][0]['value'];
		$icon = $weatherData['data']['current_condition'][0]['weatherIconUrl'][0]['value'];
		if(strstr($icon,"night")) $newIcon = "/media/images/weatherIcons/".$weatherCode."_night.png"; else $newIcon = "/media/images/weatherIcons/".$weatherCode."_day.png"; ?>
	
	<!--TRACK WEATHER-->
	<section class="trackWeather">
		<div class="trackBrand">
			<?php if($weather['track_brand'] != "") echo '<img src="'.$weather['track_brand'].'" alt="'.$weather['track_name'].'" />'; ?>
		</div>
		<hr class="vertical" />
		<div class="trackWeather">
			<div class="icon">
				<img src="<?php echo $newIcon; ?>" alt="<?php echo $weather['track_name']." Live Weather"; ?>" />
			</div>
			<div class="status">
				<span><?php echo $tempF; ?>&deg;</span><span class="degree">F</span><br />
				<span class="weatherDesc"><?php echo $weatherDesc; ?></span>
			</div>
		</div>
	</section>
	<hr />
	<!--END TRACK WEATHER-->
	
<?php } } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:22px; text-align:center; margin:40px 0px;">Track Weather Coming Soon!</div>'; $weatherQuery->close(); ?>
