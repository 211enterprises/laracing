<?php
	
	$albumQuery = $mysqli->query("SELECT `album_id`,`album_name` FROM `LARX_albums` ORDER BY `album_name` ASC");
	
	if(!isset($_POST['addMedia'])) {
		echo '<h1>Add Gallery Media:</h1>';
	}
	
	if(isset($_POST['addMedia'])) {
		$videoSource = makeSQLSafe($mysqli,$_POST['videoSource']);
		$videoPath = makeSQLSafe($mysqli,$_POST['videoPath']);
		$mediaAlbum = makeSQLSafe($mysqli,$_POST['mediaAlbum']);
		$mediaType = makeSQLSafe($mysqli,$_POST['mediaType']);
		$mediaThumb = makeSQLSafe($mysqli,$_POST['mediaThumb']);
		
		//DISABLE OTHER THUMBNAIL IMAGE
		if($mediaThumb == 1 && $mediaAlbum != "") {
			$mysqli->query("UPDATE `LARX_album_media` SET `thumbnail` = 0 WHERE `album_id` = '$mediaAlbum'");
		}
		
		if($mediaType != 0) {
			if($mediaThumb == 0 && $mediaAlbum != "") {
				$mysqli->query("INSERT INTO `LARX_album_media` (`album_id`, `media_path`, `media_source`, `timestamp`) VALUES ('$mediaAlbum', '$videoPath', '$videoSource', NOW())");
				echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Media Has Been Added</div>';
			} elseif($mediaThumb == 1 && $mediaAlbum != "") {
				$mysqli->query("INSERT INTO `LARX_album_media` (`album_id`, `media_path`, `thumbnail`, `media_source`, `timestamp`) VALUES ('$mediaAlbum', '$videoPath', 1, '$videoSource', NOW())");
				echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Media Has Been Added</div>';
			}
		} else {
		
			if($_FILES['mediaImage']['size'] > 0) {
				$tmpFile = $_FILES['mediaImage']['tmp_name'];
				$nameFile = $_FILES['mediaImage']['name'];
				$fileSize = $_FILES['mediaImage']['size'];
				$fileInfo = pathinfo($_FILES['mediaImage']['name']);
				$fileExt = $fileInfo['extension'];
				$newFileName = SHA1(RAND(0000,9999).date("Y-m-d").$_SESSION['adminID'].rand(0000,9999)).".".$fileExt;
				$acceptType = array("jpg","jpeg","png","gif");
				if(!in_array($fileExt, $acceptType)) {
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Media file type not accepted.</div>';
				} else {
					if($fileSize < 3145728) {
						move_uploaded_file($_FILES['mediaImage']['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/media/images/gallery/".$newFileName);
						if($mediaThumb == 1 && $mediaAlbum != "") {
							$mysqli->query("INSERT INTO `LARX_album_media` (`album_id`, `media_path`, `thumbnail`, `media_source`, `timestamp`) VALUES ('$mediaAlbum', '/media/images/gallery/".$newFileName."', 1, '$videoSource', NOW())");
						} else {
							$mysqli->query("INSERT INTO `LARX_album_media` (`album_id`, `media_path`, `media_source`, `timestamp`) VALUES ('$mediaAlbum', '/media/images/gallery/".$newFileName."', '$videoSource', NOW())");
						}
						
						echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Media Has Been Added</div>';
					} else {
						echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Media file is to big.</div>';
					}
				}
			} else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Please select a file.</div>';
		
		}
		
	}
	
if(!isset($_POST['addMedia'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<div class="testimonialType">
			Media Type:
			<select name="mediaType" id="mediaType">
				<option value="0">Image</option>
				<option value="1">Video</option>
			</select>
		</div>
		<fieldset>
			<div id="imageMedia">
				<label>Image File:</label>
				<input type="file" name="mediaImage" id="mediaImage" />
			</div>
			<div id="videoMedia">
				<label>Video Type:</label>
				<select name="videoSource" id="videoSource">
					<option value="">Select:</option>
					<option value="youtube">YouTube</option>
					<option value="vimeo">Vimeo</option>
				</select>
				<label>Video ID: <span id="videoSample">http://www.youtube.com/watch?v=<span>12345678</span></span></label>
				<input type="text" name="videoPath" id="videoPath" />
			</div>
	
			<label>Select a Album:</label>
			<select name="mediaAlbum" id="mediaAlbum">
				<option value="">Select:</option>
	<?php 
		if($albumQuery->num_rows > 0) {
			while($albums = $albumQuery->fetch_array()) {
				echo '<option value="'.$albums['album_id'].'">'.$albums['album_name'].'</option>';
			}
		}
	?>
			</select>
			<label>Make this the album thumbnail:</label>
			<select name="mediaThumb" id="mediaThumb">
				<option value="0">Select:</option>
				<option value="0">No</option>
				<option value="1">Yes</option>
			</select>

			<button type="submit">Add Media</button>
		</fieldset>
		<input type="hidden" name="addMedia" />
	</form>
	
<?php }

$albumQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/gallery.js"></script>