<?php

require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");

/**
 * Example of connecting PHP to the QuickBooks Merchant Service
 * @package QuickBooks
 * @subpackage Documentation
 */

// Show errors
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', true);

// Plain text output
header('Content-Type: text/plain');

// Include the QuickBooks files
require_once 'QuickBooks.php';

// If you want to log requests/responses to a database, you can provide a 
//	database DSN-style connection string here
$dsn = null;
// $dsn = 'mysql://root:@localhost/quickbooks_merchantservice';

// If you're using the DESKTOP model
$path_to_private_key_and_certificate = null;

// This is your login ID that Intuit assignes you during the application 
//	registration process.
$application_login = 'office.laracingx.com';
$connection_ticket = 'SDK-TGT-180-Ipv8VDWzYcYBIEbeqqBWNg';

// Create an instance of the MerchantService object 
$MS = new QuickBooks_MerchantService(
	$dsn, 
	$path_to_private_key_and_certificate, 
	$application_login,
	$connection_ticket);

// If you're using a Intuit QBMS development account, you must set this to true! 
$MS->useTestEnvironment(false);

// If you want to see the full XML input/output, you can turn on debug mode
$MS->useDebugMode(false);

/*
There are several methods available in the QuickBooks_MerchantService class. 
The most common methods are described below: 

 - authorize() 
    This authorizes a given amount against the a credit card. It is important 
    to note that this *does not* actually charge the credit card, it just 
    "reserves" the amount on the credit card and guarentees that if you do a 
    capture() on the same credit card within X days, the funds will be 
    available. 
    
    Authorizations are used in situations where you want to ensure the money 
    will be availble, but not actually charge the card yet. For instance, if 
    you have an online shopping cart, you should authorize() the credit card 
    when the customer checks out. However, because you might not have the item 
    in stock, or there might be other problems with the order, you don't want 
    to actually charge the card yet. Once the order is all ready to ship and 
    you've made sure there's no problems with it, you should issue a capture() 
   	using the returned transaction information from the authorize() to actually 
   	charge the credit card. 
    
 - capture()   
 - charge()
 - void()
 - refund()
 - voidOrRefund() 
 - openBatch()
 - closeBatch()

*/

//POST Parameters
if(isset($_POST['pid']) && isset($_POST['cid']) && isset($_POST['num']) && isset($_POST['expM']) && isset($_POST['expY']) && isset($_POST['cvv']) && isset($_POST['amount'])) {
	// Now, let's create a credit card object, and authorize an amount agains the card
	$number = $_POST['num'];
	$expyear = $_POST['expY'];
	$expmonth = $_POST['expM'];
	$cvv = $_POST['cvv'];
	$tax = $_POST['tax'];
	$pid = makeSQLSafe($mysqli,$_POST['pid']);
	$cid = makeSQLSafe($mysqli,$_POST['cid']);
	
	if($_POST['amount'] != "") {
		$amount = $_POST['amount'];
	} else {
		echo 'An error has occurred.';
		exit;
	}
	
	//INUSRANCE
	$insurance = makeSQLSafe($mysqli,$_POST['insurance']);
	
	//Video Information
	if($_POST['youtube'] == 1 && $_POST['dvd'] == 1 && $_POST['bluray'] == 1) {
		$video = "all";
	} else if($_POST['youtube'] == 0 && $_POST['dvd'] == 1 && $_POST['bluray'] == 1) {
		$video = "both";
	} else if($_POST['youtube'] == 1 && $_POST['dvd'] == 0 && $_POST['bluray'] == 0) {
		$video = "youtube";
	} else if($_POST['youtube'] == 0 && $_POST['dvd'] == 1 && $_POST['bluray'] == 0) {
		$video = "dvd";
	} else if($_POST['youtube'] == 0 && $_POST['dvd'] == 0 && $_POST['bluray'] == 1) {
		$video = "bluray";
	} else {
		$video = "";
	}
	
	//Photo Information
	if($_POST['photo'] == 1 && $_POST['plack'] == 1) {
		$photo = "both";
	} else if($_POST['photo'] == 1 && $_POST['plack'] == 0) {
		$photo = "photo";
	} else if($_POST['photo'] == 0 && $_POST['plack'] == 1) {
		$photo = "plaque";
	} else {
		$photo = "";
	}
	
} else {
	echo "An error has occurred with this transaction request. Missing information.";
	exit;
}

// Create the CreditCard object
$Card = new QuickBooks_MerchantService_CreditCard($name, $number, $expyear, $expmonth, $cvv);

if ($Transaction = $MS->authorize($Card, $amount, $tax)) {
	//print('Card authorized!' . "\n");
	//print_r($Transaction);	
	
	// 	Every time the MerchantService class returns a $Transaction object to you, 
	// 	you should store the returned $Transaction. You'll need the returned 
	// 	$Transaction object (or at the very least the data contained therein) in 
	// 	order to push these transactions to QuickBooks, to actually capture the 
	// 	funds, to issue a refund, or to issue a void. 
	// 	
	// 	There are several convienence methods to convert the $Transaction object to 
	// 	more storage-friendly formats if you would prefer to use these: 
	
	// Get the transaction as a string which can later be turned back into a transaction object
	$str = $Transaction->serialize(); 
	//print('Serialized transaction: ' . $str . "\n\n");
	
	// Now convert it back to a transaction object
	$Transaction = QuickBooks_MerchantService_Transaction::unserialize($str);
	
	// ... maybe you'd rather convert it to an array? 
	$arr = $Transaction->toArray();
	//print('Array transaction: '); 
	//print_r($arr);
	//print("\n\n");
	
	// ... and back again?
	$Transaction = QuickBooks_MerchantService_Transaction::fromArray($arr);
	
	// ... or an XML document?
	$xml = $Transaction->toXML();
	//print('XML transaction: ' . $xml . "\n\n");
	
	// ... and back again? 
	$Transaction = QuickBooks_MerchantService_Transaction::fromXML($xml);
	
	// How about XML that can be used in a qbXML SalesReceiptAdd request?
	$qbxml = $Transaction->toQBXML();
	//print('qbXML transaction info: ' . $qbxml . "\n\n");
	
	if ($Transaction = $MS->capture($Transaction, $amount)) {
		
		// Let's print that qbXML bit again because it'll have more data now
		$qbxml = $Transaction->toQBXML();
		//print('qbXML transaction info: ' . $qbxml . "\n\n");		
		
		//Get racer profile
		$profileQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$pid' LIMIT 1");
		if($profileQuery->num_rows == 1) {
			$profile = $profileQuery->fetch_array();
			$racerID = $profile['profile_id'];
			$racePass = $profile['pass_number'];
		} else {
			$profile = "";
			$racerID = "";
			$racePass = "";
		}
		
		$type = "Racer Package: \n Insurance: $insurance. \n Video: $video \n Photo: $photo";	
		
		$mysqli->query("INSERT INTO `LARX_racer_history` (`racer_id`,`pass_id`,`class_id`,`insurance`,`video`,`photo`,`timestamp`) VALUES ('$racerID','$pid','$cid','$insurance','$video','$photo',NOW())");
		//Log Transaction
		$mysqli->query("INSERT INTO `LARX_transaction_log` (`profile_id`,`pass_num`,`amount`,`tax`,`type`,`timestamp`) VALUES ('$racerID','$racePass','$amount','$tax','$type',NOW())");
		echo '1';
	} else {
		print('Capture ' . $MS->errorNumber() . ': ' . $MS->errorMessage() . "\n");
	}
} else {
	print('Authorization ' . $MS->errorNumber() . ': ' . $MS->errorMessage() . "\n");
}

?>

