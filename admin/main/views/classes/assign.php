<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	if(!isset($_GET['id'])) {
		header("/admin/main/classes/");
		exit;
	}
	
	$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `class_id` = 0 ORDER BY `buyer_name` ASC");	
?>

	<h1>Unassigned Racers</h1>
	
<?php if($passQuery->num_rows > 0) { ?>
	<ul class="roster">
<?php
	while($pass = $passQuery->fetch_assoc()) { ?>
		<li>
			<a href="/admin/main/classes/assignracer/<?php echo $pass['id']; ?>/">
				<div class="racer"><?php if($pass['driver_name'] != "") echo $pass['driver_name']; else echo $pass['buyer_name']; ?></div>
				<div class="pass"><span><?php echo $pass['pass_number']; ?></span></div>
				<div class="edit">Edit Racer</div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Unassigned Race Passes</div>'; ?>