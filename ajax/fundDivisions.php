<?php
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	
	if(!isset($_GET['org'])) {
		header("WWW-Authenticate: Negotiate");
		exit;	
	} else {
		$org = $_GET['org'];
	}
	
	//Instantiate fund racing class
	$LAFundRacing = new LAFundRacing($mysqli);
	
	//Display results as json
	echo $LAFundRacing->fund_divisions($org, 'json');
	
?>