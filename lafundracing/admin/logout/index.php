<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->admin_logout();
?>