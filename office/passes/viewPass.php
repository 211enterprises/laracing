<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$p = makeSQLSafe($mysqli,$_GET['p']);
	//QUERY PASS INFO
	$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `pass_hash` = '$p' LIMIT 1");
	$pass = $passQuery->fetch_array();
	//QUERY TRACK LOCATIONS
	$trackQuery = $mysqli->query("SELECT * FROM `LARX_track_locations` ORDER BY `track_name` ASC");
	//QUERY PASS TYPE
	$typeQuery = $mysqli->query("SELECT * FROM `LARX_pass_types` ORDER BY `pass_name` ASC");
	//QUERY CLASS DATES
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `date` >= '".date('Y-m-d')."' ORDER BY `date` ASC");
	//QUERY RACER PROFILES
	$profileQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` ORDER BY `id` ASC");
	
	
	if(isset($_POST['pid'])) {
		$buyerName = makeSQLSafe($mysqli,$_POST['bn']);
		$driverName = makeSQLSafe($mysqli,$_POST['dn']);
		$tracks = makeSQLSafe($mysqli,$_POST['tracks']);
		$video = makeSQLSafe($mysqli,$_POST['video']);
		$passType = makeSQLSafe($mysqli,$_POST['type']);
		$passStatus = makeSQLSafe($mysqli,$_POST['status']);
		$class = makeSQLSafe($mysqli,$_POST['class']);
		$profile = makeSQLSafe($mysqli,$_POST['profile']);
		$pid = makeSQLSafe($mysqli,$_POST['pid']);
		
		//QUERY RACER
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `id` = '$pid' LIMIT 1");
		$racePass = $racerQuery->fetch_array();
		$currentClassID = $racePass['class_id'];
		
		if($class != "" || $class != NULL || $class != 0) {
			if($currentClassID != $class) {
				//MODIFY CLASSES
				$mysqli->query("UPDATE `LARX_class_dates` SET slots_taken = slots_taken - 1 WHERE id = $currentClassID LIMIT 1");
				$mysqli->query("UPDATE `LARX_class_dates` SET slots_taken = slots_taken + 1 WHERE id = $class LIMIT 1");
			}
		} else {
			if($currentClassID != 0) {
				$mysqli->query("UPDATE `LARX_class_dates` SET slots_taken = slots_taken - 1 WHERE id = $currentClassID LIMIT 1");
			}
		}
		
		//UPDATE RACER
		$mysqli->query("UPDATE `LARX_race_passes` SET `class_id`='$class', `profile_id`='$profile', `buyer_name`='$buyerName', `driver_name`='$driverName', `track_location`='$tracks',`race_video`='$video',`pass_type`='$passType',`pass_status`='$passStatus'
		WHERE `id` = '$pid' LIMIT 1");
		
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | View Racing Pass</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($passQuery->num_rows > 0) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo $pass['pass_number']; ?></h1>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php if($passQuery->num_rows > 0) { ?>
	
	<form action="" method="post">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<label for="firstName">Buyer Name:</label>
				<input type="text" name="buyerName" id="buyerName" value="<?php echo $pass['buyer_name']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="lastName">Driver Name:</label>
				<input type="text" name="driverName" id="driverName" value="<?php echo $pass['driver_name']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="tracks">Track Location:</label>
				<select name="tracks" id="tracks">
					<option value="">Select:</option>
			<?php while($track = $trackQuery->fetch_array()) { ?>
					<option value="<?php echo $track['track_id']; ?>" <?php if($pass['track_location'] == $track['track_id']) echo 'selected="selected"'; ?>><?php echo $track['track_name']; ?></option>
			<?php } ?>
				</select>
			</li>
			<li data-role="fieldcontain">
				<label for="video">Race Video:</label>
				<select name="video" id="video">
					<option value="0">Select:</option>
					<option value="1" <?php if($pass['race_video'] == 1) echo 'selected="selected"'; ?>>Yes</option>
					<option value="0" <?php if($pass['race_video'] == 0) echo 'selected="selected"'; ?>>No</option>
				</select>
			</li>
			<li data-role="fieldcontain">
				<label for="passType">Pass Type:</label>
				<select name="passType" id="passType">
					<option value="">Select:</option>
			<?php while($type = $typeQuery->fetch_array()) { ?>
					<option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $pass['pass_type']) echo 'selected="selected"'; ?>><?php echo $type['pass_name']; ?></option>
			<?php } ?>
				</select>
			</li>
			<li data-role="fieldcontain">
				<label for="passStatus">Pass Status:</label>
				<select name="passStatus" id="passStatus">
					<option value="">Select:</option>
					<option value="valid" <?php if($pass['pass_status'] == 'valid') echo 'selected="selected"'; ?>>Active</option>
					<option value="processing" <?php if($pass['pass_status'] == 'process') echo 'selected="selected"'; ?>>Processing</option>
					<option value="used" <?php if($pass['pass_status'] == 'used') echo 'selected="selected"'; ?>>Used</option>
					<option value="void" <?php if($pass['pass_status'] == 'void') echo 'selected="selected"'; ?>>Void</option>
				</select>
			</li>
			<li data-role="fieldcontain">
				<label for="class">Class Date:</label>
				<select name="class" id="class">
					<option value="">Select:</option>
			<?php while($class = $classQuery->fetch_array()) {
					$classSlots = $class['class_limit'] - $class['slots_taken']; ?>
					<option value="<?php echo $class['id']; ?>" <?php if($pass['class_id'] == $class['id']) echo 'selected="selected"'; ?>><?php echo date("m-d-Y",strtotime($class['date']))." - ".date("h:i A",strtotime($class['time']))." - ".$classSlots." Slots Open"; ?></option>
			<?php } ?>
				</select>
			</li>
			<li data-role="fieldcontain">
				<label for="profile">Racer Profile:</label>
				<select name="profile" id="profile">
					<option value="">Select:</option>
			<?php while($profile = $profileQuery->fetch_array()) { ?>
					<option value="<?php echo $profile['id']; ?>" <?php if($pass['profile_id'] == $profile['id']) echo 'selected="selected"'; ?>><?php echo $profile['id']." - ".$profile['first_name']." ".$profile['last_name']; ?></option>
			<?php } ?>
				</select>
			</li>
			<li data-role="fieldcontain">
				<button type="submit" id="editPassBtn" data-theme="a" data-icon="arrow-r" data-iconpos="right">Edit Race Pass</button>
			</li>
			<input type="hidden" name="pid" id="pid" value="<?php echo $pass['id']; ?>" />
		</ul>
	</form>
	
<?php } else echo '<h3 style="text-align:center;">No Race Pass Found.</h3>'; ?>
   		
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$passQuery->close();
$classQuery->close();
$trackQuery->close();
$mysqli->close();	
?>