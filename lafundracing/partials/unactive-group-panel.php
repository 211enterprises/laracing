<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#organization<?php echo $value['id']; ?>">
        <?php echo $value['name']; ?>
      </a>
    </h4>
  </div>
  <div id="organization<?php echo $value['id']; ?>" class="panel-collapse collapse">
    <div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					Organization Name: <?php echo $org_name; ?>
				</div>
				<div class="col-md-4">
		      Campaign Start: <?php echo $value['campaign_start']; ?><br />
				</div>
				<div class="col-md-4">
		      Campaign End: <?php echo $value['campaign_end']; ?><br />
				</div>
				<div class="col-md-4">
		      Name: <?php echo $value['contact_name']; ?><br />
				</div>
				<div class="col-md-4">
		      Email: <?php echo $value['contact_email']; ?><br />
				</div>
				<div class="col-md-4">
		      Phone: <?php echo $value['contact_phone']; ?><br />
				</div>
				<div class="col-md-4">
		      Representive: <?php echo $value['rep_name']; ?><br />
				</div>
				<div class="col-md-12">
		      Mission Statement:<br />
					<?php echo $value['mission_statement']; ?>
				</div>
			</div>
      <hr />
      
      <div class="row" style="margin-top: 30px;">
      	<div class="col-md-6">
      		<a href="" class="btn btn-block btn-primary approve-group" data-id="<?php echo $value['id']; ?>">Approve Group</a>
      	</div>
      </div>
    </div>
  </div>
</div> 