<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$today = date("Y-m-d");
	$todayQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `date` = '$today' ORDER BY `time` ASC");
	$classQuery = $mysqli->query("SELECT DISTINCT(date) FROM `LARX_class_dates` WHERE `date` < '$today' ORDER BY `date` DESC");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Class Reporting</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">

<?php if($todayQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="raceProfiles" data-inset="true" data-split-icon="gear">
   			<li data-listdivider="true" data-theme="a">Todays Class/Times:</li>
   	<?php while($todayClass = $todayQuery->fetch_assoc()) {
   				echo '<li><a href="/office/reporting/classReport.php?h='.$todayClass['class_hash'].'" data-rel="dialog" data-transition="pop">'.date("h:i A",strtotime($todayClass['time'])).'</a></li>';
   		  } ?>
   		</ul>
<?php } ?>

   		
<?php if($classQuery->num_rows > 0) { ?>
   		<ul data-role="listview" id="raceProfiles" data-inset="true" data-filter="true" data-filter-placeholder="Search by class date...">
   			<li data-listdivider="true" data-theme="a">Previous Class Dates:</li>
   	<?php while($class = $classQuery->fetch_assoc()) {
   				echo '<li><a href="/office/reporting/classTimes.php?d='.$class['date'].'" data-rel="dialog" data-transition="pop">'.date("D - F jS, Y",strtotime($class['date'])).'</a></li>';
   		  } ?>
   		</ul>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo footer("reporting"); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>