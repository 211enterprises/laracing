<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");

	$r = makeSQLSafe($mysqli,$_GET['r']);
	if(isset($_GET['r'])) {
		$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` WHERE `id` = '$r'");
		$racer = $racerQuery->fetch_array();
		$racerID = $racer['id'];
		//FETCH HISTORY
		$historyQuery = $mysqli->query("SELECT * FROM `LARX_racer_history` WHERE `racer_id` = '$racerID' ORDER BY `timestamp` ASC");
	}

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Racer History</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo $racer['first_name']." ".$racer['last_name']." Racing History"; ?></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($historyQuery->num_rows > 0) { ?>
   		<div data-role="collapsible-set" data-icon="arrow-d" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u">
   	<?php while($history = $historyQuery->fetch_assoc()) {
   			$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '".$history['class_id']."' LIMIT 1");
   			$class = $classQuery->fetch_array(); ?>
   			<div data-role="collapsible">
				<h3><?php echo date("D - F jS, Y - h:i A",strtotime($class['date']." ".$class['time'])); ?></h3>
				<ul data-role="listview">
					<li data-role="fieldcontain">
						<div class="ui-grid-a">
							<div class="ui-block-a">
								Insurance Coverage: <span style="color:red;"><?php if($history['insurance'] == 1) echo 'Yes'; else echo 'No'; ?></span>
							</div>
							<div class="ui-block-b">
								In Car Media: 
							<?php 
								switch($history['video']) {
									case "youtube":
										echo '<span style="color:red;">YouTube</span>';
										break;
									case "dvd":
										echo '<span style="color:red;">DVD</span>';
										break;
									case "blueray":
										echo '<span style="color:red;">BlueRay</span>';
										break;
									case "both":
										echo '<span style="color:red;">YouTube &amp; DVD</span>';
										break;
									case "all":
										echo '<span style="color:red;">YouTube &amp; DVD & BlueRay</span>';
										break;
									default:
										echo '<span style="color:red;">None</span>';
										break;
								} ?>
							</div>
						</div>
					</li>
					<li data-role="fieldcontain">
						<div class="ui-grid-a">
							<div class="ui-block-a">
								Photo:
							<?php 
								switch($history['photo']) {
									case "photo":
										echo '<span style="color:red;">In Car Photo</span>';
										break;
									case "plaque":
										echo '<span style="color:red;">Photo &amp; Plaque</span>';
										break;
									case "both":
										echo '<span style="color:red;">In Car Photo & Plaque</span>';
										break;
									default:
										echo '<span style="color:red;">None</span>';
										break;
								} ?>
							</div>
							<div class="ui-block-b"></div>
						</div>
					</li>
					<li data-role="fieldcontain">
						<div class="ui-grid-solo" style="text-align:left;">
							<span style="color:red;">Notes:</span><br />
							<textarea name="racerNotes" id="racerNotes<?php echo $history['id']; ?>" style="width: 100%; height: 140px; resize: none;" placeholder="write something about this racer."><?php echo $history['notes']; ?></textarea>
						</div>
					</li>
					<li data-role="fieldcontain">
						<div class="ui-grid-solo" style="text-align:left;">
							<a href="" id="editRacerNotes" data-racer="<?php echo $racerID; ?>" data-history="<?php echo $history['id']; ?>" data-role="button" data-theme="a">Add/Edit Racer Notes</a>
						</div>
					</li>
				</ul>
			</div>
   	<?php } ?>
   		</div>
<?php } else echo '<h3 style="text-align:center; margin:30px 0;">No Current Racing History</h3>'; ?>
   		
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$racerQuery->close();
$mysqli->close();	
?>