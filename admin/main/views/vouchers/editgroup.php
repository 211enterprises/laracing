<?php

	$id = makeSQLSafe($mysqli,$_GET['id']);
	$voucherQuery = $mysqli->query("SELECT * FROM `LARX_voucher_companies` WHERE `voucher_id` = '$id' LIMIT 1");
	if($voucherQuery->num_rows == 1) {
		$voucher = $voucherQuery->fetch_array();
		$voucherID = $voucher['voucher_id'];
		
		if(!isset($_POST['editVoucherGroup'])) {
			echo '<h1>Edit Voucher Group:</h1>';
			echo '<a href="'.ADMIN_ROOT.'/?controller=vouchers&id=deletegroup&id='.$voucher['voucher_id'].'" class="deleteContent">Delete This Voucher Group</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Voucher Group Can\'t Be Found</div>';
	}
	
	if(isset($_POST['editVoucherGroup'])) {
		$groupName = makeSQLSafe($mysqli,$_POST['voucherName']);
		$voucherURL = makeSQLSafe($mysqli,$_POST['voucherURL']);
		$description = makeSQLSafe($mysqli,$_POST['description']);
		
		//ADD RECORED
		$mysqli->query("UPDATE `LARX_voucher_companies` SET `company_name`='$groupName', `voucher_url`='$voucherURL', `description`='$description' WHERE `voucher_id`='$voucherID' LIMIT 1");
		
	}
	
if(!isset($_POST['editVoucherGroup']) && $voucherQuery->num_rows == 1) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset>
			<label>Voucher Group Name:</label>
			<input type="text" name="voucherName" id="voucherName" value="<?php echo $voucher['company_name']; ?>" />
			<label>Voucher Page URL:</label>
			<input type="text" name="voucherURL" id="voucherURL" value="<?php echo $voucher['voucher_url']; ?>" />
			<label>Voucher Page Description: <small>Replace default description</small></label>
			<textarea name="description"><?php echo $voucher['description']; ?></textarea>
			<button type="submit">Edit Group</button>
		</fieldset>
		<input type="hidden" name="editVoucherGroup" value="<?php echo $voucherID; ?>" />
	</form>
	
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Voucher Group Has Been Updated</div>';

$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/vouchers.js"></script>