<?php
	
	if(session_id() == "") session_start();
	
	if(isset($_GET['logout'])) {
		session_destroy();
		header("Location: /admin/");
		exit();
	}
	
	if(isset($_SESSION['adminID'])) {
		header("Location: /admin/main/");
		exit();
	}
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/admin.inc");
	
	$admin = new admin;
	
	$admin->auth($mysqli,"username","password");
		
	//$password = SHA1("admin".SHA1("engine2012"));
	//$mysqli->query("INSERT INTO `LARX_admins` (`username`,`password`) VALUES ('admin','$password')");
	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Racing X : Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/admin.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/admin/login.js"></script>
<!--[if lt IE 9]>
<script src="/media/js/html5shiv.js"></script>
<![endif]-->
</head>
<body>

<!--MAIN CONTENT-->
<div class="adminLogin">
	<div class="brand"><img src="/media/images/topBrand.png" alt="LA Racing X" /></div>
	<form action="/admin/" method="post">
		<fieldset>
			<input type="text" name="username" id="username" placeholder="username" />
			<input type="password" name="password" id="password" placeholder="password" />
			<button type="submit">Login</button>
		</fieldset>
	</form>
	<div class="acmedia">
		Powered By: <a href="http://www.acmediadesigns.com" target="_blank">AC Media Designs</a>
	</div>
</div>
<!--END MAIN CONTENT-->

</body>
</html>
<?php $mysqli->close(); ?>