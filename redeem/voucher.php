<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	$template = new template;
	$booking = new booking;
	$vouchers = new vouchers;
	
	$v = makeSQLSafe($mysqli,$_GET['v']);
	if(isset($_GET['v'])) {
		$voucherQuery = $mysqli->query("SELECT * FROM `LARX_voucher_companies` WHERE `voucher_url` = '$v'");
		if($voucherQuery->num_rows == 1) {
			$voucher = $voucherQuery->fetch_array();
			$voucherName = $voucher['company_name'];
		} else {
			header("Location: /");
			exit;
		}
	} else {
		header("Location: /");
		exit;
	}
	
	//SUBMIT VOUCHER INFORMATION
	if(isset($_POST['redeemVoucher']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['voucherNum1'])) {
		$name = makeSQLSafe($mysqli,$_POST['name']);
		$phone = makeSQLSafe($mysqli,$_POST['phone']);
		$email = makeSQLSafe($mysqli,$_POST['email']);
		$address = makeSQLSafe($mysqli,$_POST['address']);
		$city = makeSQLSafe($mysqli,$_POST['city']);
		$state = makeSQLSafe($mysqli,$_POST['state']);
		$zip_code = makeSQLSafe($mysqli,$_POST['zip_code']);
		$comments = makeSQLSafe($mysqli,$_POST['voucherComments']);
		$assignDate = makeSQLSafe($mysqli,$_POST['assignDate']);
		
		$voucherNum1 = makeSQLSafe($mysqli,$_POST['voucherNum1']);
		$voucherDriver1 = makeSQLSafe($mysqli,$_POST['voucherDriver1']);
		$voucherNum2 = makeSQLSafe($mysqli,$_POST['voucherNum2']);
		$voucherDriver2 = makeSQLSafe($mysqli,$_POST['voucherDriver2']);
		$voucherNum3 = makeSQLSafe($mysqli,$_POST['voucherNum3']);
		$voucherDriver3 = makeSQLSafe($mysqli,$_POST['voucherDriver3']);
		$voucherNum4 = makeSQLSafe($mysqli,$_POST['voucherNum4']);
		$voucherDriver4 = makeSQLSafe($mysqli,$_POST['voucherDriver4']);
		$voucherNum5 = makeSQLSafe($mysqli,$_POST['voucherNum5']);
		$voucherDriver5 = makeSQLSafe($mysqli,$_POST['voucherDriver5']);
		$voucherNum6 = makeSQLSafe($mysqli,$_POST['voucherNum6']);
		$voucherDriver6 = makeSQLSafe($mysqli,$_POST['voucherDriver6']);
		$voucherNum7 = makeSQLSafe($mysqli,$_POST['voucherNum7']);
		$voucherDriver7 = makeSQLSafe($mysqli,$_POST['voucherDriver7']);
		$voucherNum8 = makeSQLSafe($mysqli,$_POST['voucherNum8']);
		$voucherDriver8 = makeSQLSafe($mysqli,$_POST['voucherDriver8']);
		
		$voucherArray = 
		array(
			array($voucherNum1,$voucherDriver1),
			array($voucherNum2,$voucherDriver2),
			array($voucherNum3,$voucherDriver3),
			array($voucherNum4,$voucherDriver4),
			array($voucherNum5,$voucherDriver5),
			array($voucherNum6,$voucherDriver6),
			array($voucherNum7,$voucherDriver7),
			array($voucherNum8,$voucherDriver8)
		);
		
		$voucherNums = 
		array(
			array($voucherNum1,$voucherDriver1),
			array($voucherNum2,$voucherDriver2),
			array($voucherNum3,$voucherDriver3),
			array($voucherNum4,$voucherDriver4),
			array($voucherNum5,$voucherDriver5),
			array($voucherNum6,$voucherDriver6),
			array($voucherNum7,$voucherDriver7),
			array($voucherNum8,$voucherDriver8)
		);
		
		//Get User IP Address
		$userIP = $_SERVER['REMOTE_ADDR'];
		
		$todayis = date("Y-m-d H:i --- P/T [U]");
		$order_date = date("Y-m-d H:i:s");
		
		$voucherData = array();
		$voucherData  = '';
		$voucherData  .= 'ip='.$userIP.'&';
		$voucherData  .= 'realname ='.strip_tags($name).'&';
		$voucherData  .= 'address='.strip_tags($address).'&';
		$voucherData  .= 'city='.strip_tags($city).'&';
		$voucherData  .= 'us_state='.strip_tags($state).'&';
		$voucherData  .= 'zip='.strip_tags($zip_code).'&';
		$voucherData  .= 'phone='.strip_tags($phone).'&';
		$voucherData  .= 'email='.strip_tags($email).'&';
		$voucherData  .= 'comments='.strip_tags($message).'&';
		$voucherData  .= 'ad_source='.$voucherName.'&';
		$voucherData  .= 'created='.$todayis.'&';

    // Message for DB and mail
    $message = '';

    $message .= "L.A. Racing Experience - ". $voucherName ." Voucher Redemption Information<br>";
    $message .= "--------------------------------<br><br>";
    $message .= "Name:   ".strip_tags($name)." <br>";
    $message .= "Addr:   ".strip_tags($address)." <br>";
    $message .= "           ".strip_tags($city).' '.strip_tags($state).' '.strip_tags($zip_code)."<br><br>";
    $message .= "Ph#:    ".strip_tags($phone)." <br><br>";

    $message .= "E-mail: ".strip_tags($email)." <br>";
    $message .= "Location: LosAngeles <br>";
    $message .= "From:   ". $name ." <br><br><br>";
    $message .= "Comments:\n".strip_tags($comments)." <br><br>";

		$vn = 1;
		$vv = 1;
		foreach($voucherArray as $value) {
			if($value[0] != "") {
				$message .= "Voucher#".$vn.": ".$value[0]." - ".$value[1]." <br>";
				$voucherData  .= 'voucher'.$vn.'='.$value[0].'&';
				$vn++;
				$validateQuery = $LARXDB->query("SELECT * FROM `das_vouchers` WHERE `voucher_num` = '".$value[0]."' AND `redemption_status` = 'valid'");
				if($validateQuery->num_rows == 1) {
					$vv++;
				}
			}
		}
		$voucherData  .= 'location=LosAngeles &';
		
	}	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Racing X : Redeem Your Voucher</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/slideshow.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript" src="/media/js/voucher.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

<?php echo $template->header(); ?>

<!--MAIN CONTENT-->
<div class="content">

<?php 
if(!isset($_POST['redeemVoucher']) && !isset($_POST['email']) && !isset($_POST['phone']) && !isset($_POST['voucherNum1'])) {
	
	$vouchers->redeem($mysqli,$voucherName,$voucher['description']);
	
} else {
	
	if($vv == $vn) {
		$template->pageHeader("Thank You For Redeeming Your Voucher",NULL,"/media/images/voucherIcon.png");
		
		//$LARXDB->query("INSERT INTO db_changes (old_data, new_data) VALUES ('new voucher record created','".$voucherData."')");
		
		//Save Voucher
		$saveVoucher = $LARXDB->query("INSERT INTO `vouchers` (`created`,`ip`,`status`,`name`,`address`,`city`,`us_state`,`zip`,`phone`,`email`,`comments`,`ad_source`,`location`) VALUES (NOW(),'$userIP','new','$name','$address','$city','$state','$zip_code','$phone','$email','".$template->dbFriendlyString($message)."','$voucherName','LosAngeles')");
		
		//Voucher ID
		$v_id = $LARXDB->insert_id;
				
		foreach($voucherNums as $voucherValue) {
			if($voucherValue[0] != "") {
				$LARXDB->query("UPDATE das_vouchers SET redemption_num = '".$v_id."', redemption_status = 'redeemed', redemption_attempts = redemption_attempts+1, redemption_date = '".$order_date."', 
				voucher_notes = CONCAT( voucher_notes, '".$order_date." - redemption request #".$v_id." submitted from ".$email."\n') 
				WHERE ( redemption_status = 'valid' OR redemption_status = 'redeemed' ) AND voucher_num = '".$voucherValue[0]."' ");
			}
		}
?>
	<section class="redeemVoucher">
		<div class="voucherInfo">
			<?php if($saveVoucher) { ?>
				Thank You <?php echo $name; ?>! Your comments have been sent to our pit crew. A copy has also been sent to the e-mail you provided for your convenience. Your Race Pass[es] will be processed shortly.	
				
			<?php
						// Send email notification
            $vouchers->send_redemption_notification($mysqli, array(
              'email' => $email,
              'subject' => "L.A. Racing Experience - ". $voucherName ." Voucher Redemption Information",
              'comments' => $comments,
              'message' => $message,
              'voucher' => array(
                'name' => $voucherName,
                'number' => $voucherNum1
              )
            ));
					} else echo 'An error has occurred. Please try again.' ?>
		</div>
	</section>
<?php
	} else {
		$template->pageHeader("Please try again",NULL,"/media/images/voucherIcon.png");
?>
	<section class="redeemVoucher">
		<div class="voucherInfo">
			You tried to redeem a voucher that has already been redeemed or has expired. Please try again with valid vouchers.
		</div>
	</section>
<?php
	} ?>
<?php
}
?>
	
</div>
<!--END MAIN CONTENT-->

<?php $template->footer($mysqli); ?>

<div class="voucherPopUp"></div>

<noscript>
	<div class="racingOverlay" style="display:block;"></div>
	<div class="voucherPopUp" style="display:block; z-index:600;">
		This website relies heavily on javascript. Please enable it.
	</div>
</noscript>
<!--[if lt IE 8]>
	<div class="racingOverlay" style="display:block;"></div>
	<div class="voucherPopUp" style="display:block; z-index:600;">
		This website relies on newer techonology. Please update your browser to the latest version. Thank You!
	</div>
<[endif]-->

</body>
</html>
<?php $mysqli->close(); ?>