<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['order'])) {
		$orderID = makeSQLSafe($mysqli,$_GET['order']);
		
		//FETCH CUSTOMER ORDER
		$orderQuery = $mysqli->query("SELECT * FROM `LARX_orders` WHERE `id` = '$orderID' LIMIT 1");
		$order = $orderQuery->fetch_array();
		
		//FETCH CLASS INFO
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates`,`LARX_track_locations` WHERE LARX_class_dates.id = ".$order['class_id']." AND LARX_class_dates.track_location = LARX_track_locations.track_id LIMIT 1");
		$class = $classQuery->fetch_assoc();
		
		//FETCH PASS NUMBERS
		$orderPasses = array();
		$passQuery = $mysqli->query("SELECT * FROM `LARX_race_passes` WHERE `order_id` = '$orderID'");
		while($passes = $passQuery->fetch_array()) {
			$orderPasses[] = $passes;
		}
		
		//COUNT PACKAGES AND DATA
		$packages = explode(",",$order['package_data']);
		$packageCount = count($packages);
		$i = 0;
		$v = 1;
		$videosCount = 0;
		
		//COUNT VIDEOS AND DATA
		$videos = explode(",",$order['video_data']);
		foreach($videos as $value) {
			if($value == 1) {
				$videosCount = $v;
				$v++;
			}
		}
	} else exit;

?>
	<style>
		body {background:url('http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/bg.jpg') repeat #000; width:100%; height:100%; color:#FFF; font-family:Helvetica Neue, Helvetica, Arial; font-size:14px; margin:0; padding:0;}
	</style>
	<table width="500" align="center" border="0">
		<tr>
			<td colspan="5" width="500" height="255">
				<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/header.png" alt="">
			</td>
		</tr>
		<tr>
			<td width="20"></td>
			<td colspan="4" align="center">
				<font color="cb202a">Customer Name:</font><br><font size="5"><b><?php echo strtoupper($order['name']); ?></b></font><br>
				<font color="cb202a">Class Date:</font><br><font size="4"><b><?php echo strtoupper(date("M jS, Y",strtotime($class['date']))); ?></b></font><br>
				<font color="cb202a">Class Time:</font><br><font size="4"><b><?php echo strtoupper(date("h:i A",strtotime($class['time']))); ?></b></font><br>
				<font color="cb202a">Track Location:</font><br><font size="4"><b><?php echo strtoupper($class['track_name']); ?></b></font><br>
				<font size="3">
					<?php echo strtoupper($class['address']); ?><br>
					<?php echo strtoupper($class['city'].", ".$class['state']." ".$class['zip_code']); ?>
				</font>
			</td>
		</tr>
		<tr>
			<td width="20" height="16"></td>
			<td colspan="3" width="462" height="16"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/cartDivLine.png" alt=""></td>
			<td width="18" height="16"></td>
		</tr>
		<tr>
			<td colspan="5" width="500" height="52">
				<img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/orderSummary.png" alt="">
			</td>
		</tr>
<?php foreach($packages as $item) {
		$itemArray = split("-",$item);
		$packageQuery = $mysqli->query("SELECT * FROM `LARX_classes` WHERE `id` = '$itemArray[0]' LIMIT 1");
		$itemPackage = $packageQuery->fetch_assoc(); ?>
		<tr>
			<td width="20"></td>
			<td colspan="3" width="462" height="71" style="background:url(/media/images/emailPasses/itemBG.png);">
				<font color="808080" size="5"><?php echo strtoupper($itemPackage['class_name']); ?></font><br>
				$<?php echo $itemPackage['class_price']."  |  ".$orderPasses[$i]['pass_number']; ?>  |  <a href="http://laracingx.com/passes/print.php?id=<?php echo $orderPasses[$i]['pass_hash']; ?>" target="_blank">Print This Pass</a><br>
				<img src="<?php if($videos[$i] == 1) echo 'http://'.$_SERVER['SERVER_NAME'].'/media/images/emailPasses/video-included.png'; else echo 'http://'.$_SERVER['SERVER_NAME'].'/media/images/emailPasses/video-not-included.png'; ?>" alt="Race Video" style="float:right;">
			</td>
			<td width="18"></td>
		</tr>
<?php $i++;
	  } ?>
		<tr>
			<td colspan="5" width="500" height="14"></td>
		</tr>
		<tr>
			<td width="20" height="77"></td>
			<td width="227" height="77"></td>
			<td width="2" height="77"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/totalDivVert.png" alt=""></td>
			<td width="233" height="77" align="right">
				<font color="d3d3d3" size="3">x<?php echo $packageCount; ?> Passes</font><br>
				<font color="d3d3d3" size="3">x<?php echo $videosCount; ?> Videos</font><br>
				<font color="cb202a" size="4">Order Total:</font> <font color="d3d3d3" size="4">$<?php echo $order['order_total']; ?></font>
			</td>
			<td width="18" height="77"></td>
		</tr>
		<tr>
			<td width="20" height="63"></td>
			<td width="462" height="63" colspan="3"><img src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/media/images/emailPasses/getPrepared.png" alt="Get Prepared For Your Raceday!"></td>
			<td width="18" height="63"></td>
		</tr>
		<tr>
			<td width="20"></td>
			<td width="462" colspan="3">
				Fire suits, helmets, & gloves are provided for drivers who need them. You may use your own IF the appropriate specs are met. THIN SOLE, CLOSED TOE SHOES are required to drive. Racing shoes and other safety gear is generally available for purchase.
			</td>
			<td width="18"></td>
		</tr>
	</table>
<?php $mysqli->close(); ?>