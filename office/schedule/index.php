<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$today = date("Y-m-d");
	$classQuery = $mysqli->query("SELECT DISTINCT(date) FROM `LARX_class_dates` WHERE `date` >= '$today' ORDER BY `date` ASC");

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Upcoming Class Dates</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main" data-title="LA Racing X | Upcoming Class Dates">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
	   <a href="/office/transactions/newPass.php" data-role="button" data-rel="dialog" class="ui-btn-right" data-icon="star" data-iconpos="left" id="btn-buy-pass">Buy Passes</a>
	   <a href="" data-role="button" id="logout" class="ui-btn-right" data-icon="home" data-iconpos="left">Logout</a>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($classQuery->num_rows > 0) { ?>
   		<div data-role="collapsible-set" data-inset="true" data-icon="arrow-d" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u">
   	<?php while($class = $classQuery->fetch_assoc()) {
   			$classTimeQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `date` = '".$class['date']."' ORDER BY `time` ASC"); ?>
   			<div data-role="collapsible">
				<h3><?php echo date("D - F jS, Y",strtotime($class['date'])); ?></h3>
				<ul data-role="listview" data-theme="a">
			<?php while($classTime = $classTimeQuery->fetch_array()) { ?>
					<li>
					<a href="/office/schedule/classOptions.php?h=<?php echo $classTime['class_hash']; ?>" data-rel="dialog" data-transition="pop">
						<?php echo date("h:i A",strtotime($classTime['time'])); ?>
						<span class='ui-li-count ui-btn-up-c ui-btn-corner-all'><?php echo $classTime['class_limit'] - $classTime['slots_taken']; ?></span>
					</a>
					</li>
			<?php } ?>
				</ul>
			</div>
   	<?php } ?>
   		</div>
<?php } ?>
   		
   </div>
   <!--END CONTENT-->
   
<?php echo footer("scheduling"); ?>
      
</div>

</body>
</html>
<?php
$classQuery->close();
$mysqli->close();	
?>