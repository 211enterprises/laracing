<?php
		
	if(!isset($_POST['addClass'])) {
		echo '<h1>Add Class:</h1>';
	}
	
	if(isset($_POST['addClass'])) {
		//DATE
		$dateM = makeSQLSafe($mysqli,$_POST['dateM']);
		$dateD = makeSQLSafe($mysqli,$_POST['dateD']);
		$dateY = makeSQLSafe($mysqli,$_POST['dateY']);
		$formatDate = $dateY."-".$dateM."-".$dateD;
		
		//TIME
		$timeH = makeSQLSafe($mysqli,$_POST['timeH']);
		$timeM = makeSQLSafe($mysqli,$_POST['timeM']);
		$timePeriod = makeSQLSafe($mysqli,$_POST['timePeriod']);
		$formatTime = date("H:i:00",strtotime($timeH.":".$timeM.":00 ".$timePeriod));
		
		$classLimit = makeSQLSafe($mysqli,$_POST['classLimit']);
		$trackLocation = makeSQLSafe($mysqli,$_POST['trackLocation']);
		$classPackages = makeSQLSafe($mysqli,implode(",",$_POST['classPackages']));
		$hash = SHA1(RAND(0000,9999).$formatDate.$_SESSION['adminID'].RAND(0000,9999).$formatTime);
		
		$mysqli->query("INSERT INTO `LARX_class_dates` (`class_id`,`date`,`time`,`track_location`,`class_limit`,`class_hash`,`timestamp`) VALUES ('$classPackages','$formatDate','$formatTime','$trackLocation','$classLimit','$hash',NOW())");

	}
	
	$trackQuery = $mysqli->query("SELECT `track_id`,`track_name` FROM `LARX_track_locations` ORDER BY `track_name` ASC");
	$classesQuery = $mysqli->query("SELECT `id`,`class_name` FROM `LARX_classes` ORDER BY `order` ASC");
	
?>

<?php if(!isset($_POST['addClass'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>Class Date:</label>
			<select name="dateM" id="dateM" class="classDate">
				<option value="">Month:</option>
		<?php for($m = 01; $m <= 12; $m++) {
				echo '<option value="'.str_pad($m,2,0,STR_PAD_LEFT).'">'.str_pad($m,2,0,STR_PAD_LEFT).'</option>';
		  } ?>
			</select>
			<select name="dateD" id="dateD" class="classDate">
				<option value="">Day:</option>
		<?php for($d = 01; $d <= 31; $d++) {
				echo '<option value="'.str_pad($d,2,0,STR_PAD_LEFT).'">'.str_pad($d,2,0,STR_PAD_LEFT).'</option>';
		  } ?>
			</select>
			<select name="dateY" id="dateY" class="classDate">
				<option value="">Year:</option>
				<option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
				<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
			</select>
			
			<label>Class Time:</label>
			<select name="timeH" id="timeH" class="classTime">
				<option value="">Hour:</option>
		<?php for($h = 01; $h <= 12; $h++) {
				echo '<option value="'.str_pad($h,2,0,STR_PAD_LEFT).'">'.str_pad($h,2,0,STR_PAD_LEFT).'</option>';
		  } ?>
			</select>
			<select name="timeM" id="timeM" class="classTime">
				<option value="">Minutes:</option>
		<?php for($min = 00; $min <= 59; $min++) {
				echo '<option value="'.str_pad($min,2,0,STR_PAD_LEFT).'">'.str_pad($min,2,0,STR_PAD_LEFT).'</option>';
		  } ?>
			</select>
			<select name="timePeriod" id="timePeriod" class="classTime">
				<option value="">Time Period:</option>
				<option value="AM">AM</option>
				<option value="PM">PM</option>
			</select>
			
			<label>Class Limit:</label>
			<select name="classLimit" id="classLimit">
				<option value="">Select Class Limit:</option>
	<?php for($l = 1; $l <= 100; $l++) {
				echo '<option value="'.$l.'">'.$l.'</option>';
		  } ?>
			</select>
			<label>Class Track Location:</label>
			<select name="trackLocation" id="trackLocation">
				<option value="">Select:</option>
	<?php if($trackQuery->num_rows > 0) {
			while($track = $trackQuery->fetch_array()) {
				echo '<option value="'.$track['track_id'].'">'.$track['track_name'].'</option>';
			}
		} ?>
			</select>
			
			<label>Available Class Packages: <small>Windows: Hold (ctrl). Mac: Hold (command).</small></label>
			<select name="classPackages[]" id="classPackages" multiple="true">
	<?php if($classesQuery->num_rows > 0) {
			while($classes = $classesQuery->fetch_array()) {
				echo '<option value="'.$classes['id'].'">'.$classes['class_name'].'</option>';
			}
		} ?>
			</select>
			
			<button type="submit">Add Class</button>
		</fieldset>
		<input type="hidden" name="addClass" />
	</form>	
<?php
	} else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Class Has Been Added</div>';

$trackQuery->close();
$mysqli->close();
?>