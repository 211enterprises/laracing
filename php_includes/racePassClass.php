<?php
	class RacePasses extends Core {
		
		function __construct($connection) {
			$this->db = $connection;
		}
		
		public function set_pass_expiration_date($format = 'Y-m-d') {
			return date($format, strtotime('+1 year'));
		}
		
		public function process_fund_race_passes($data) {
			$pass_numbers = Array();
			
			for($num = 1; $num <= $data['quantity']; $num++) {
				$data['pass_num'] = $this->save_pass_data($data);
				array_push($pass_numbers, $data['pass_num']);
				$this->email_fund_race_pass($data);
			}
			
			return $pass_numbers;
		}
		
		protected function email_fund_race_pass($data) {
			$data['driver_name'] = $data['firstName'].' '.$data['lastName'];
			$data['class_date'] = 'TBD';
			$data['expires'] = $this->set_pass_expiration_date();
			$data['pass_type'] = 'Adventure 20';
			$data['location'] = 'LosAngeles';
			
			$file_name = $this->generate_pass_email_template($data);
			
			$attachment = chunk_split(base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/ssxdbaccess/fPDF/race_passes/'.$file_name)));
			$random_hash = md5(date('r', time()));
			
			$from = 'L.A. Racing <support@laracingx.com>';
			$subject = "L.A. Racing Experience - Race Pass #" . $data['pass_num'];
			
			$extra_headers  = "From: ". $from ." \r\n";
			$extra_headers .= "Reply-To: ". $from ." \r\n";
			//$extra_headers .= "Bcc: ".$bcc." \r\n";
			$extra_headers .= "Content-Type: multipart/mixed;\r\n  boundary=\"PHP-mixed-".$random_hash."\"\r\n"; 
			
			$text = '';
			$text .= "--PHP-mixed-".$random_hash."\r\n";
			$text .= "Content-Type: text/plain; charset=utf-8\r\n";
			$text .= "Content-Transfer-Encoding: 7bit\r\n";
			
			$text  .= "\r\n";
			$text .= "Driver Name:          ".$data['driver_name'] ." \r\n\r\n";
			$text .= "Scheduled Class Date: TBD \r\n";
			$text .= "Scheduled Class Time: TBD \r\n";
			$text .= "Race Pass #:          ".$data['pass_num']." \r\n";
			$text .= "Location:             ".$data['location']." \r\n\r\n";
			
			$text .= "\r\n\r\nPlease check the class schedule for available dates when you are ready to schedule or if you might need to reschedule at http://laracing.com/ \r\n\r\n";
			
			//$text .= "--PHP-alt-".$random_hash."--\r\n";
			
			$text .= "--PHP-mixed-".$random_hash."\r\n";
			$text .= "Content-Type: application/pdf; name=\"".$file_name."\"\r\n";
			$text .= "Content-Transfer-Encoding: base64\r\n";
			$text .= "Content-Disposition: attachment\r\n\r\n";
			
			$text .= $attachment."\n\n";
			$text .= "--PHP-mixed-".$random_hash."--\r\n";

			if(mail($data['email'], $subject, $text, $extra_headers)) {
			  return true;
			} else {
			  return false;
			}
		}
		
		//Private methods
		private function save_pass_data($data) {
			$date = date('Y-m-d');
			$expireDate = $this->set_pass_expiration_date();
			$organization = makeSQLSafe($this->db, $data['organization']);
			$firstName = makeSQLSafe($this->db, $data['firstName']);
			$lastName = makeSQLSafe($this->db, $data['lastName']);
			$fullName = $firstName.' '.$lastName;
			$member = makeSQLSafe($this->db, $data['memberName']);
			$email = makeSQLSafe($this->db, $data['email']);
			$phone = makeSQLSafe($this->db, $data['phone']);
			$address = makeSQLSafe($this->db, $data['address']);
			$city = makeSQLSafe($this->db, $data['city']);
			$state = makeSQLSafe($this->db, $data['state']);
			$zipCode = makeSQLSafe($this->db, $data['zip_code']);
			$comments = makeSQLSafe($this->db, $data['comments']);
				
			$query = $this->db->query("INSERT INTO `race_passes` (`created`,`modified`,`expires`,`status`,`class_date_confirmed`,`class_date`,`driver_company`,`driver_name`,
				`driver_addr1`,`driver_addr2`,`driver_state`,`driver_zip`,`driver_country`,`driver_email1`,`buyer_1st_ph`,`race_pass_laps`,`race_pass_type`,
				`race_pass_paid`,`race_pass_notes`,`race_pass_mailed`,`race_pass_mailed_date`,`sales_rep`,`location`,`created_for`) VALUES 
				(NOW(),NOW(),'$expireDate','unscheduled','false','0000-00-00','$fullName','$fullName','$address','$city','$state','$zipCode','USA','$email','$phone',
				'20','Adventure 20','150.00','$comments','email','$date','$member','LosAngeles','LosAngeles')");
			
			if($query) {
				return $this->db->insert_id;
			}
		}
		
		private function generate_pass_email_template($data, $temp='02-Adv20.pdf', $location='LosAngeles', $unscheduled = true) {
		
			$ssx_dir = $_SERVER['DOCUMENT_ROOT'].'/ssxdbaccess';
			
			require_once($ssx_dir.'/fPDF/fpdf.php');
			require_once($ssx_dir.'/fPDF/fpdi.php');

			$pdf = new FPDI();
			$race_pass_template = $ssx_dir.'/fPDF/blanks/'.$location.'/'.$temp;
			
			$pagecount = $pdf->setSourceFile($race_pass_template); 
			$tplidx = $pdf->importPage(1, '/ArtBox');
			 
			$pdf->addPage(); 
			$pdf->useTemplate($tplidx, 2, 1, 214); 
			$pdf->AddFont('BankGothicBT-Medium', '', $ssx_dir.'/fPDF/bankgothicmedium.php');
			$pdf->SetFont('BankGothicBT-Medium', '', 18);
			$pdf->SetXY(126, 54);
			$pdf->Cell(86, 10, $data['driver_name'], 0, 0, 'C', false);
			$pdf->SetFont('BankGothicBT-Medium', '', 16);
			$pdf->SetXY(126, 65);
			
			if( $data['class_date'] == '0000-00-00' || $unscheduled ) {
				$data['class_date'] = 'TBD';
				$printed_time = 'TBD';
			} else {
				$printed_time = date("g:i", strtotime($data['class_time']));
			}
			
			$pdf->Cell(86, 10, $data['class_date'], 0, 0, 'C', false);
			$pdf->SetXY(194,69);
			$pdf->Cell(16, 10, $printed_time, 0, 0, 'C', false);
			
			$pdf->SetXY(39, 109);
			$pdf->Cell(65, 10, $data['expires'], 0, 0, 'C', false);
			$pdf->SetXY(39, 114);
			$pdf->Cell(65, 10, $data['pass_num'], 0, 0, 'C', false);
			
			$file_name = "LARX Race Pass ".$data['pass_num']."-".$location.'-'.$data['driver_name']."-[".time()."]-".$data['pass_type'].".pdf";
				 
			$pdf->Output($ssx_dir."/fPDF/race_passes/".$file_name, 'F'); //saves as a file on the server
			//$pdf->Output($ssx_dir."/fPDF/race_passes/".$file_name, 'D'); //forces a download in the browser
			
			return $file_name;
		}
	}
?>