<?php
	$voucherQuery = $mysqli->query("SELECT * FROM `LARX_voucher_companies` ORDER BY `company_name` ASC");
?>

	<h1>Vouchers</h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=vouchers&action=uploader" class="addContent">Bulk Upload</a>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=vouchers&action=addvoucher" class="addContent">Add Voucher Code</a>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=vouchers&action=addgroup" class="addContent">Add Voucher Group</a>
	
<?php if($voucherQuery->num_rows > 0) { ?>
	<ul class="testimonials">
<?php while($voucher = $voucherQuery->fetch_assoc()) { ?>
		<li>
			<a href="<?php echo ADMIN_ROOT; ?>/?controller=vouchers&action=editgroup&id=<?php echo $voucher['voucher_id']; ?>">
				<div class="clientName"><?php echo $voucher['company_name']; ?></div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Voucher Companies</div>';

$voucherQuery->close();
$mysqli->close(); ?>