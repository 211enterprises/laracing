<?php
	class Core {
		public function __construct() {
		
		}
	
		public function labelFor($for, $title, $required=false) {
			if($required) {
				echo '<label for="'.$for.'">'.$title.' <span>*</span></label>';	
			} else {
				echo '<label for="'.$for.'">'.$title.'</label>';
			}
		}
		
		public function input($type, $name, $class=null, $val=null, $data=null, $required=false, $maxlength='') {
			($class) ? $class = ' class="'.$class.'"' : '';
			($val) ? $val = ' value="'.$val.'"' : '';
			($required) ? $required = 'required' : '';
			($maxlength != "") ? $maxlength = ' maxlength="'.$maxlength.'"' : $maxlength = '';
			($data != "") ? $data = ' value="'. $data[$name] .'"' : '';
			
			echo '<input type="'.$type.'" name="'.$name.'" id="'.$name.'"'. $class . $val . $data . $maxlength . $required.' />';
		}
		
		public function textarea($name, $class=null, $val=null) {
			echo '<textarea name="'.$name.'" id="'.$name.'" class="'.$class.'">'.$val.'</textarea>';
		}
		
		public function selectBox($name, $options, $class=null, $disabled=false, $data=null, $required=false) {
			($disabled) ? $disabled = ' disabled' : '';
			($required) ? $required = ' required' : '';
			
			echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'"'.$disabled . $required.'>';
			foreach($options as $k => $v) {
				if($data[$name] == $k) {
					echo '<option value="'.$k.'" selected="selected">'.$v.'</option>';
				} else {
					echo '<option value="'.$k.'">'.$v.'</option>';	
				}
			}
			echo '</select>';
		}	
		
		public function submitBtn($title, $class=null) {
			echo '<button type="submit" class="'.$class.'">'.$title.'</button>';
		}
		
		public function cardMonths() {
			return array(
						'01' => 'Jan - 01',
						'02' => 'Feb - 02',
						'03' => 'Mar - 03',
						'04' => 'Apr - 04',
						'05' => 'May - 05',
						'06' => 'Jun - 06',
						'07' => 'Jul - 07',
						'08' => 'Aug - 08',
						'09' => 'Sep - 09',
						'10' => 'Oct - 10',
						'11' => 'Nov - 11',
						'12' => 'Dec - 12'
			);
		}
		
		public function cardYears($num=5) {
			$date = date('Y');
			$years[$date] = $date;
			
			for($i = 1; $i <= $num; $i++) {
				$year = $date + $i;
				$years[$year] = $year;
			}
			
			return $years;
		}
		
		public function flashNotice($notice, $display=false) {
			($display) ? $display = ' style="display:block;"' : $display = '';

			echo '<div class="flashNotice"'. $display .'">';
			echo '<span>'. $notice .'</span>';
			echo '<i class="close"></i>';
			echo '</div>';
		}
		
		protected function redirect($url) {
			header("Location: " . $url);
			exit;
		}
	}
?>