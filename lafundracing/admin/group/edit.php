<?php
	if(session_id() == "") session_start();
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	
	$LAFundAdmin->check_admin_access();
	
	$groupObj = $LAFundRacing->get_fund_division_by_id($_GET['group']);
	
	// Redirect to admin if organization is not found
	if($groupObj->num_rows != 1) {
		header('Location: '.LAFR_ADMIN_ROOT);
		exit;
	} else {
		$group = $groupObj->fetch_array();
	}
	
	//Get group payable information
	$payableObj = $LAFundAdmin->get_division_payable_by_id($group['id']);
	$payment = $payableObj->fetch_array();
	
	// Submit edited group
	$LAFundAdmin->edit_organization_group($_POST);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing Administration</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<link href="/media/style/datepicker.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/adminLAFR.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript">
	$(function() {
		$('input#campaignStart').datepicker();
		$('input#campaignEnd').datepicker();
	});
</script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../shared/admin-header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
		<h1>Edit Group - <?php echo $group['name']; ?>:</h1>
		<hr />
		
		<form action="" method="post">
			<div class="row">
				<div class="col-md-12">
					<label for="name">Name:</label>
					<input type="text" name="name" id="name" class="LAFRAdmin" value="<?php echo $group['name']; ?>" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Campaign Start Date:</label>
					<input type="text" name="campaignStart" id="campaignStart" data-date-format="yyyy-mm-dd" class="LAFRAdmin" value="<?php echo $group['campaign_start']; ?>" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Campaign End Date:</label>
					<input type="text" name="campaignEnd" id="campaignEnd" data-date-format="yyyy-mm-dd" class="LAFRAdmin" value="<?php echo $group['campaign_end']; ?>" required />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Contact Name:</label>
					<input type="text" name="contactName" id="contactName" class="LAFRAdmin" value="<?php echo $group['contact_name']; ?>" />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Contact Email Address:</label>
					<input type="text" name="contactEmail" id="contactEmail" class="LAFRAdmin" value="<?php echo $group['contact_email']; ?>" />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Contact Phone:</label>
					<input type="text" name="contactPhone" id="contactPhone" class="LAFRAdmin" value="<?php echo $group['contact_phone']; ?>" />
				</div>
				<div class="col-md-12">
					<label for="campaignStart">Representative Name:</label>
					<input type="text" name="repName" id="repName" class="LAFRAdmin" value="<?php echo $group['rep_name']; ?>" />
				</div>
				<div class="col-md-12">
					<label for="mission">Mission Statement:</label>
					<textarea name="mission" id="mission" style="width: 100%;"><?php echo $group['mission_statement']; ?></textarea>
				</div>
			</div>
			
			<hr />
			<h3>Payment Information:</h3>
			
			<div class="row">
				<div class="col-md-6">
					<label for="campaignStart">Attn:</label>
					<input type="text" name="payAttn" id="payAttn" class="LAFRAdmin" value="<?php echo $payment['attention']; ?>" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Payable Name:</label>
					<input type="text" name="payName" id="payName" class="LAFRAdmin" value="<?php echo $payment['payable_name']; ?>" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Address:</label>
					<input type="text" name="payAddress" id="payAddress" class="LAFRAdmin" value="<?php echo $payment['address']; ?>" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">City:</label>
					<input type="text" name="payCity" id="payCity" class="LAFRAdmin" value="<?php echo $payment['city']; ?>" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">State:</label>
					<input type="text" name="payState" id="payState" class="LAFRAdmin" value="<?php echo $payment['state']; ?>" required />
				</div>
				<div class="col-md-6">
					<label for="campaignStart">Zip Code:</label>
					<input type="text" name="payZip" id="payZip" class="LAFRAdmin" value="<?php echo $payment['zip_code']; ?>" required />
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 pull-right">
					<button type="submit">Edit Group</button>
				</div>
			</div>
			
			<input type="hidden" name="group" value="<?php echo $group['id']; ?>" />
		</form>
	</div>	

	<script type="text/javascript" src="/media/js/lafundracing.js"></script>
</body>
</html>