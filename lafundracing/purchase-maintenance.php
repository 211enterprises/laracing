<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/racePassClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/paymentsClass.php");
	
	$template = new template;
	$core = new core;
	$LAFundRacing = new LAFundRacing($mysqli);
	$RacePass = new RacePasses($LARXDB);
	$payments = new Payments($mysqli, LAFR_PAYMENT_STATUS);
	
	$processed = false;
	$displayError = false;
	
	if(isset($_POST) && !empty($_POST)) {
		$captureCard = $payments->authorize_capture($_POST);
		
		if($captureCard['status']) {
			if($LAFundRacing->process_save_order($_POST)) {
				$passes = $RacePass->process_fund_race_passes($_POST);
				$processed = true;
			}
		} else {
			//Display card error message to user
			$errorMsg = $captureCard['message'];
			$displayError = true;
		}
	}	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="LA Racing X" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index,follow" />
<title>LA Fund Racing</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('partials/header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
    <h1 style="text-align: center; margin: 40px 0;">Under maintenance. Be back soon...</h1>
  </div>
</body>
</html>
<?php $mysqli->close(); ?>