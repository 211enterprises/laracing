<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script>
<script src="test.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<style type="text/css">
	.ui-controlgroup.ui-login {width:50%; margin:auto;}
</style>
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
   		
   		
   </div>
   <!--END CONTENT-->
   
<?php echo footer(); ?>
      
</div>

</body>
</html>
