<hr />
<h2>Sales History:</h2>
<hr />
<h4>Total Sales: <?php echo (is_array($transactions)) ? count($transactions) : '0'; ?></h4>
<hr />

<table class="table">
	<thead>
		<th>Member Name</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Email Address</th>
		<th>Phone</th>
		<th>Address</th>
		<th>City</th>
		<th>State</th>
		<th>Zip Code</th>
		<th>Quantity</th>
		<th>Purchase Date</th>
	</thead>
	<tbody>
<?php 
if(is_array($transactions)) {
	foreach($transactions as $value) {
?>
		<tr>
			<td><?php echo $value['member_name']; ?></td>
			<td><?php echo $value['first_name']; ?></td>
			<td><?php echo $value['last_name']; ?></td>
			<td><?php echo $value['email_address']; ?></td>
			<td><?php echo $value['phone']; ?></td>
			<td><?php echo $value['address']; ?></td>
			<td><?php echo $value['city']; ?></td>
			<td><?php echo $value['state']; ?></td>
			<td><?php echo $value['zip_code']; ?></td>
			<td><?php echo $value['pass_quantity']; ?></td>
			<td><?php echo date("M dS, Y", strtotime($value['created_at'])); ?></td>
		</tr>
<?php 
	}
} 
?>
	</tbody>
</table>