<?php
	
	$id = makeSQLSafe($mysqli,$_GET['id']);
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$id' LIMIT 1");
	$classFound = $classQuery->num_rows;
	if($classFound == 1) {
		$class = $classQuery->fetch_array();
		$classID = $class['id'];
		$classSlots = $class['slots_taken'] + 1;
		$classesArray = $class['class_id'];
		
		if(!isset($_POST['addRacer'])) {
			echo '<h1>Add a Racer:</h1>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Class Can\'t Be Found</div>';
	}
	
		
	if(isset($_POST['addRacer'])) {
		$racerName = makeSQLSafe($mysqli,$_POST['racerName']);
		$raceVideo = makeSQLSafe($mysqli,$_POST['raceVideo']);
		$racerNotes = makeSQLSafe($mysqli,$_POST['racerNotes']);
		$passOverride = makeSQLSafe($mysqli,$_POST['passOverride']);
		$generatePass = generateRacePass($mysqli);
		$generateHash = generateRaceHash($mysqli);
		
		$classCheckQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$id' LIMIT 1");
		$classCheck = $classCheckQuery->fetch_assoc();
		$takenClass = $classCheck['class_limit'] - $classCheck['slots_taken'];
		$newTakenClass = $classCheck['slots_taken'] + 1;
		if($takenClass > 0) {
			$mysqli->query("UPDATE `LARX_class_dates` SET `slots_taken` = '$newTakenClass' WHERE `id` = '$id' LIMIT 1");
					
			if($_POST['passOverride'] == "") {
				$mysqli->query("INSERT INTO `LARX_race_passes` (`class_id`,`driver_name`,`track_location`,`pass_number`,`pass_hash`,`timestamp`) VALUES ('$classID','$racerName','1','$generatePass','$generateHash',NOW())");
			} else {
				$mysqli->query("INSERT INTO `LARX_race_passes` (`class_id`,`driver_name`,`track_location`,`pass_number`,`pass_hash`,`timestamp`) VALUES ('$classID','$racerName','1','$passOverride','$generateHash',NOW())");
			}
			
			$lastID = $mysqli->insert_id;		
			$mysqli->query("INSERT INTO `LARX_class_roster` (`class_id`,`racer_name`,`pass_id`,`racer_video`,`racer_notes`,`timestamp`) VALUES ('$classID','$racerName','$lastID','$raceVideo','$racerNotes',NOW())");

		}
		
	}

?>

<?php 
if($classFound == 1) {
	if(!isset($_POST['addRacer'])) { ?>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<fieldset class="editFAQ">
				<label>Racer Name:</label>
				<input type="text" name="racerName" id="racerName" />
				
				<label>Override Race Pass:</label>
				<input type="text" name="passOverride" id="passOverride" />
				
				<label>Race Video Purchased</label>
				<select name="raceVideo" id="raceVideo">
					<option value="0">Select:</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
				
				<label>Racer Notes:</label>
				<textarea name="racerNotes" id="racerNotes"></textarea>
				
				<button type="submit">Add Racer</button>
			</fieldset>
			<input type="hidden" name="addRacer" />
		</form>	
	<?php
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Racer Has Been Added</div>';
	}
}



$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/racer.js"></script>
