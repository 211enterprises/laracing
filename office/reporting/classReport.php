<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$currentDate = date("Y-m-d");
	$currentTime = date("H:i:s");
	$h = makeSQLSafe($mysqli,$_GET['h']);
	if(isset($_GET['h'])) {
		$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `class_hash` = '$h' LIMIT 1");
		$class = $classQuery->fetch_array();
		$classID = $class['id'];
		//FETCH HISTORY
		$historyQuery = $mysqli->query("SELECT * FROM `LARX_racer_history` WHERE `class_id` = '$classID'");
		//COUNT PURCHASES
		$insuranceCount = 0;
		$videoCount = 0;
		$photoCount = 0;
		
		while($history = $historyQuery->fetch_array()) {
			if($history['insurance'] == 1) { $insuranceCount++; }
			if($history['video'] != "") { $videoCount++; }
			if($history['photo'] != "") { $photoCount++; }
		}
		
	}

?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Class Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<!-----------------------------------------------MAIN PAGE-------------------------------------------------------->
<div data-role="page" id="main">
   
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo date("m/d/Y",strtotime($class['date'])); ?> - Class Report</h1>
   </div>
   <!--END HEADER-->
   
   <!--CONTENT-->
   <div data-role="content">
   		
<?php if($classQuery->num_rows == 1) { ?>
		<ul data-role="listview">
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						Insurance Policies Purchased:
					</div>
					<div class="ui-block-b">
						 <span style="color:red; margin-left: 20px;"><?php echo $insuranceCount; ?></span>
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						In Car Media Purchases:
					</div>
					<div class="ui-block-b">
						<span style="color:red; margin-left: 20px;"><?php echo $videoCount; ?></span
					</div>
				</div>
			</li>
			<li data-role="fieldcontain">
				<div class="ui-grid-a">
					<div class="ui-block-a">
						Photo Purchases:
					</div>
					<div class="ui-block-b">
						<span style="color:red; margin-left: 20px;"><?php echo $photoCount; ?></span>
					</div>
				</div>
			</li>
		</ul>
<?php } else echo '<h3 style="text-align:center; margin:30px 0;">No Class Found</h3>'; ?>
   		
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$historyQuery->close();
$classQuery->close();
$mysqli->close();	
?>