<?php

	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/connection.php");
	require($_SERVER['DOCUMENT_ROOT']."/office/php_includes/functions.php");
	
	$r = makeSQLSafe($mysqli,$_GET['r']);
	$racerQuery = $mysqli->query("SELECT * FROM `LARX_racer_profiles` WHERE `id` = '$r' LIMIT 1");
	$racer = $racerQuery->fetch_array();
	
	if(isset($_POST['rid']) && isset($_POST['fname']) && isset($_POST['lname'])) {
		$firstName = makeSQLSafe($mysqli,$_POST['fname']);
		$lastName = makeSQLSafe($mysqli,$_POST['lname']);
		$address = makeSQLSafe($mysqli,$_POST['address']);
		$city = makeSQLSafe($mysqli,$_POST['city']);
		$state = makeSQLSafe($mysqli,$_POST['state']);
		$zipCode = makeSQLSafe($mysqli,$_POST['zip']);
		$phone = makeSQLSafe($mysqli,$_POST['phone']);
		$email = makeSQLSafe($mysqli,$_POST['email']);
		$flagged = makeSQLSafe($mysqli,$_POST['redflag']);
		$rid = makeSQLSafe($mysqli,$_POST['rid']);
		
		//UPDATE RACER
		$mysqli->query("UPDATE `LARX_racer_profiles` SET `first_name`='$firstName', `last_name`='$lastName', `address`='$address',`city`='$city',`state`='$state',`zip_code`='$zipCode',`phone`='$phone',`email_address`='$email',`red_flag`='$flagged' WHERE `id` = '$rid' LIMIT 1");
		
	}
	
?>
<!DOCTYPE html>
<html>
<head>
<title>LA Racing X Back Office | Edit Racer Profile</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<link rel="stylesheet" href="/office/global/style/custom.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript" src="/office/global/js/custom.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>

<div data-role="page">
   
<?php if($racerQuery->num_rows > 0) { ?>
   <!--HEADER-->
   <div data-role="header">
	   <h1><?php echo $racer['first_name']." ".$racer['last_name']; ?></h1>
	   <a href="" data-theme="b" data-role="button" data-iconpos="left" data-icon="delete" class="deleteProfile" data-racerprofile="<?php echo $racer['id']; ?>">Delete Profile</a>
   </div>
   <!--END HEADER-->
<?php } else { ?>
	<!--HEADER-->
   <div data-role="header">
	   <h1><img src="/media/images/topBrand.png" alt="LA Racing X" /></h1>
   </div>
   <!--END HEADER-->
<?php } ?>
   
   <!--CONTENT-->
   <div data-role="content">
   
<?php if($racerQuery->num_rows > 0) { ?>
	
	<form action="" method="post">
		<ul data-role="fieldcontain" style="text-align:center;">
			<li data-role="fieldcontain">
				<label>Racer #: <?php echo $racer['id']; ?></label>
			</li>
			<li data-role="fieldcontain">
				<label for="firstName">First Name:</label>
				<input type="text" name="firstName" id="firstName" value="<?php echo $racer['first_name']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="lastName">Last Name:</label>
				<input type="text" name="lastName" id="lastName" value="<?php echo $racer['last_name']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="address">Address:</label>
				<input type="text" name="address" id="address" value="<?php echo $racer['address']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="city">City:</label>
				<input type="text" name="city" id="city" value="<?php echo $racer['city']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="state">State:</label>
				<input type="text" name="state" id="state" value="<?php echo $racer['state']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="zipCode">Zip Code:</label>
				<input type="text" name="zipCode" id="zipCode" value="<?php echo $racer['zip_code']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="phone">Phone #:</label>
				<input type="tel" name="phone" id="phone" value="<?php echo $racer['phone']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" value="<?php echo $racer['email_address']; ?>" />
			</li>
			<li data-role="fieldcontain">
				<label for="flagged">Red Flag Driver:</label>
				<select name="flagged" id="flagged">
					<optgroup label="Red Flag Driver From Racing">
						<option value="0">Select:</option>
						<option value="1" <?php if($racer['red_flag'] == 1) echo 'selected="selected"'; ?>>Yes</option>
						<option value="0" <?php if($racer['red_flag'] != 1) echo 'selected="selected"'; ?>>No</option>
					</optgroup>
				</select>
			</li>
			<li data-role="fieldcontain">
				<button type="submit" id="editRacerBtn" data-theme="a" data-icon="arrow-r" data-iconpos="right">Edit Racer</button>
			</li>
			<input type="hidden" name="rid" id="rid" value="<?php echo $racer['id']; ?>" />
		</ul>
	</form>
	
<?php } else echo '<h3 style="text-align:center;">No Racer Profile Found.</h3>'; ?>
   		
 
   </div>
   <!--END CONTENT-->
      
</div>

</body>
</html>
<?php
$racerQuery->close();
$mysqli->close();	
?>