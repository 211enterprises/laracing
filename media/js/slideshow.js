/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/
	$(function() {
		
		//SLIDESHOW ARROWS---------------------------------------------------------------
		$('div.slideArrowL').on({
			mouseover: function() {$('div.slideArrowL').animate({backgroundPosition: "-16px"},300,"easeOutBack");},
			mouseout: function() {$('div.slideArrowL').animate({backgroundPosition: "0px"},300,"easeInBack");}
		});
		
		$('div.slideArrowR').on({
			mouseover: function() {$('div.slideArrowR').animate({backgroundPosition: "16px"},300,"easeOutBack");},
			mouseout: function() {$('div.slideArrowR').animate({backgroundPosition: "0px"},300,"easeInBack");}
		});
		
		//CHANGE SLIDES----------------------------------------------------------------
		//LEFT ARROW
		$('div.slideArrowL').live("click",function() {
			var slideID = $(this).attr("data-slide"),
				currentPosition = $('div.subSlideContainer ul').css("left").replace(/[-px]/g,""),
				newSlidePosition = Number(currentPosition) - 2960,
				slideNum = $('div.subSlideContainer ul li').index(),
				prevSlide = Number(slideID) - 1,
				nextSlide = Number(slideID) + 1;
				
			if(currentPosition > 0) {
				$('div.slideArrowL').attr("data-slide",prevSlide);
				$('div.slideArrowR').attr("data-slide",nextSlide);
				
				$('div.subSlideContainer ul').animate({
					left: "-"+newSlidePosition+"px"
				},400,"easeInBack");
			
			}
		});
		
		//RIGHT ARROW
		$('div.slideArrowR').live("click",function() {
			var slideID = $(this).attr("data-slide"),
				currentPosition = $('div.subSlideContainer ul').css("left").replace(/[-px]/g,""),
				newSlidePosition = Number(currentPosition) + 2960,
				slideNum = Number($('div.subSlideContainer ul li').index()) + 1,
				prevSlide = Number(slideID) - 1,
				nextSlide = Number(slideID) + 1;
			
			if(slideID != slideNum) {
				$('div.slideArrowL').attr("data-slide",prevSlide);
				$('div.slideArrowR').attr("data-slide",nextSlide);
				
				$('div.subSlideContainer ul').animate({
					left: "-"+newSlidePosition+"px"
				},400,"easeInBack");
			}
			
		});
		
	});
	
	//SLIDE PROPERTIES---------------------------------------------------------------
	var imgPath = "/media/images/slideshow/";
	//SLIDE ELEMENTS
	var slideshow = [

		//SLIDE 1
		[
			{"image" : imgPath+'smoke.png', "x1" : 0, "y1" : -2000, "x2" : 0, "y2" : 0, "delay" : 300, "timing" : 300, "directionX" : "left", "directionY" : "top"},
			{"image" : imgPath+'femaleRacer.png', "x1" : -2000, "y1" : 0, "x2" : 0, "y2" : 0, "delay" : 100, "timing" : 300, "directionX" : "left", "directionY" : "top"},
			{"image" : imgPath+'car12.png', "x1" : -2000, "y1" : 100, "x2" : 500, "y2" : 100, "delay" : 500, "timing" : 300, "directionX" : "right", "directionY" : "top"}
		],
		[
			{"image" : imgPath+'smoke.png', "x1" : 0, "y1" : -2000, "x2" : 0, "y2" : 0, "delay" : 300, "timing" : 300, "directionX" : "left", "directionY" : "top"},
			{"image" : imgPath+'femaleRacer.png', "x1" : -2000, "y1" : 0, "x2" : 0, "y2" : 0, "delay" : 100, "timing" : 300, "directionX" : "left", "directionY" : "top"},
			{"image" : imgPath+'car12.png', "x1" : -2000, "y1" : 100, "x2" : 500, "y2" : 100, "delay" : 500, "timing" : 300, "directionX" : "right", "directionY" : "top"}
		]
		
		
	];
	
	
	
	
	//TEXT MARQUEE
	$(function() {
		var marqueeList = $('div.textMarquee ul');
		var marqueeItems = marqueeList.children();
		var marqueeLength = marqueeItems.length;
		var lineHeight = marqueeItems.eq(0).outerHeight();
	    var listHeight = lineHeight * marqueeLength;
		current = 0,
        nextMarquee = function() {
            marqueeList.animate({top: '+='+ lineHeight}, 500, 'easeOutBack', function(){
                current++;
                if (current == marqueeLength) {
                    marqueeList.css('top', -listHeight);
                    current = 0;
                }
                setTimeout(nextMarquee, 3000);
            });
        };
        
        marqueeItems.each(function(i, li){
	        marqueeList.prepend(li);
	    });
	    marqueeItems.eq(0).clone().prependTo(marqueeList);
	    marqueeList.css('top', -listHeight);
	    setTimeout(nextMarquee, 3000);
        
	});
	
	
	//WATCH VIDEO
	$(function() {
		
		$('a.watchVideo').click(function() {
			var videoSlide = $('div.slideVideoContainer');
				videoSlide.fadeToggle(400);
			return false;
		});
		
	});
	
	
	