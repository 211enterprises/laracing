<?php

	if(!isset($_POST['addTestimonial'])) {
		echo '<h1>Add Testimonial:</h1>';
	}
	
	if(isset($_POST['addTestimonial'])) {
		$name = makeSQLSafe($mysqli,$_POST['clientName']);
		$location = makeSQLSafe($mysqli,$_POST['clientLocation']);
		$writtenTestimonial = makeSQLSafe($mysqli,$_POST['testimonial']);
		$testmonialType = makeSQLSafe($mysqli,$_POST['testimonialType']);
		$videoSource = makeSQLSafe($mysqli,$_POST['videoSource']);		
		$videoPath = makeSQLSafe($mysqli,$_POST['videoPath']);
		
		//ADD RECORED
		$mysqli->query("INSERT INTO `LARX_testimonials` (`client_name`,`client_location`,`description`,`video_source`,`video_url`,`testimonial_type`,`timestamp`) VALUES ('$name','$location','$writtenTestimonial','$videoSource','$videoPath','$testmonialType', NOW())");
		
	}
	
if(!isset($_POST['addTestimonial'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<div class="testimonialType">
			Testimonial Type:
			<select name="testimonialType" id="testimonialType">
				<option value="0">Written</option>
				<option value="1">Video</option>
			</select>
		</div>
		<fieldset>
			<label>Client Name:</label>
			<input type="text" name="clientName" id="clientName" />
			<label>Client Location/Company:</label>
			<input type="text" name="clientLocation" id="clientLocation" />
			<div id="writtenTestimonial">
				<label>Client Testimonial:</label>
				<textarea name="testimonial"></textarea>
			</div>
			<div id="videoTestimonial">
				<label>Video Type:</label>
				<select name="videoSource" id="videoSource">
					<option value="youtube">YouTube</option>
					<option value="vimeo">Vimeo</option>
				</select>
				<label>Video ID: <span id="videoSample">http://www.youtube.com/watch?v=<span>12345678</span></span></label>
				<input type="text" name="videoPath" id="videoPath" />
			</div>
			<button type="submit">Add</button>
		</fieldset>
		<input type="hidden" name="addTestimonial" />
	</form>
	
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Testimonial Has Been Added</div>';

$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/testimonials.js"></script>