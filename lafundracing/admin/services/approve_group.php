<?php
	if(session_id() == "") session_start();

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundAdmin.php");
	
	$LAFundRacing = new LAFundRacing($mysqli);
	$LAFundAdmin = new LAFundAdmin($mysqli);
	$Register = new LAFundRegister($mysqli);
	// Check admin permissions
	$LAFundAdmin->check_admin_access();
	//Array to hold callback data
	$status = [];
	
	if(isset($_POST['id']) && is_numeric($_POST['id'])) {
		$id = makeSQLSafe($mysqli, $_POST['id']);
		if($Register->approve_group($id)) {
			$status['status'] = 1;
		} else {
			$status['status'] = 0;
		}
	} else {
		$status['status'] = 0;
	}
	
	echo json_encode($status);
?>