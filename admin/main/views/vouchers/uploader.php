<?php
	
	$groupQuery = $mysqli->query("SELECT `voucher_id`,`company_name` FROM `LARX_voucher_companies` ORDER BY `company_name` ASC");

	if(!isset($_POST['addVoucherBulk'])) {
		echo '<h1>Voucher Bulk Uploader:</h1>';
	}
	
	if(isset($_POST['addVoucherBulk'])) {
		$group = makeSQLSafe($mysqli,$_POST['voucherGroup']);
		
		if($_FILES['voucherFile']['size'] > 0) {
			$fname = $_FILES['voucherFile']['name'];
			$chk_ext = explode(".",$fname);
			 
			 if(strtolower($chk_ext[1]) == "csv") {
			 
				 $filename = $_FILES['voucherFile']['tmp_name'];
				 $handle = fopen($filename, "r");
			
				 while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$data = str_replace("'","\'",$data);
					/*$numCols = count($data);
					$acceptedNumCols = 1;*/
					
					//$checkCode = $mysqli->query("SELECT `voucher_number` FROM `LARX_voucher_codes` WHERE `voucher_number` = '$data[0]'");
					//$checkCode = $LARXDB->query("SELECT `voucher_num` FROM `das_vouchers` WHERE `voucher_num` = '".$data[0]."'");
					//if($checkCode->num_rows == 0) {
						//$mysqli->query("INSERT INTO `LARX_voucher_codes` (`voucher_id`,`voucher_number`,`buyer_name`,`address`,`city`,`state`,`zip_code`,`phone`,`email`,`timestamp_purchased`,`timestamp`)
						 //VALUES ('$group','$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]','$data[8]',NOW())");
					$LARXDB->query("INSERT INTO `das_vouchers` (`voucher_src`,`voucher_num`,`voucher_name`,`voucher_purchase_date`,`redemption_status`,`location`) VALUES ('$group','".$data[0]."','No Name Given',NOW(),'valid','LosAngeles')");
					//}

				 }
				 fclose($handle);
			 }    
		}
		
	}
	
if(!isset($_POST['addVoucherBulk'])) { ?>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<fieldset>
			<label>Voucher CSV File:</label>
			<input type="file" name="voucherFile" id="voucherFile" />
			<label>Voucher Group:</label>
			<select name="voucherGroup" id="voucherGroup">
				<option value="">Select:</option>
	<?php 
		if($groupQuery->num_rows > 0) {
			while($group = $groupQuery->fetch_array()) {
				echo '<option value="'.$group['voucher_id'].'">'.$group['company_name'].'</option>';
			}
		} ?>
			</select>
			<span style="color:#cb202a;">CSV Format:</span><br /><span style="color:#FFF;">Voucher Number, Customer Name, Address, City, State, Zip Code, Phone, Email Address, Date Purchased</span>
			<button type="submit">Upload</button>
		</fieldset>
		<input type="hidden" name="addVoucherBulk" />
	</form>
	
<?php } else {
		if($_FILES['voucherFile']['size'] > 0) {
			if($numCols == $acceptedNumCols) { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				Voucher Uploader Complete!<br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Add Additional Codes</a>
			</div>
		<?php } else { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				<span style="color:#cb202a; font-size:20px;">CSV was not formatted correctly. Please use this format:</span><br />
				<span style="font-size:16px;">Voucher Number, Customer Name, Address, City, State, Zip Code, Phone, Email Address, Date Purchased</span><br /><br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Back To Uploader</a>
			</div>
		<?php } ?>
	<?php } else { ?>
			<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
				Please try again<br />
				<a href="/admin/main/vouchers/uploader/" class="addMoreVouchers">Add Voucher Codes</a>
			</div>
	<?php }
	  }

$groupQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/vouchers.js"></script>