<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	define(ADMIN_ROOT,"/admin/main");
	
	class admin {
		
		private $urlvalues;
		
		public function __construct($urlvalues) {
			$this->urlvalues = $urlvalues;
		}
		
		public function auth($mysqli,$username,$password, $session) {
			$username = makeSQLSafe($mysqli,$_POST[$username]);
			$password = makeSQLSafe($mysqli,sha1($username.sha1($_POST[$password])));
	
			if(isset($_POST['username'])) {
				$authQuery = $mysqli->query("SELECT * FROM `LARX_admins` WHERE `username` = '$username' AND `password` = '$password' LIMIT 1");
				if($authQuery->num_rows == 1) {
					$auth = $authQuery->fetch_array();
					$_SESSION['adminID'] = $auth['id'];
					
					$mysqli->query("UPDATE `LARX_admins` SET `last_login` = NOW() WHERE `id` = '".$auth['id']."' LIMIT 1");
					
					header("Location: /admin/main/");
					exit();
				} else {
					header("Location: /admin/?loginError");
					exit();
				}
			}
		}
			
		public function Loader($mysqli) {
				
			if ($this->urlvalues['controller'] == "") {
				$this->pageview = "/admin/main/views/home/";
				$this->pagemodel = "/admin/main/models/home/";
			} else {
				$this->pageview = "/admin/main/views/".$this->urlvalues['controller']."/";
				$this->pagemodel = "/admin/main/models/".$this->urlvalues['controller']."/";
			}
			if ($this->urlvalues['action'] == "") {
				$this->action = "index.php";
			} else {
				$this->action = $this->urlvalues['action'].".php";
			}
			
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$this->pageview.$this->action)) {
				require($_SERVER['DOCUMENT_ROOT'].$this->pageview.$this->action);
			} else {
				require($_SERVER['DOCUMENT_ROOT']."/admin/main/views/error/index.php");
			}
		}
			
		
		public function access($mysqli,$userID,$level) {
			$accessQuery = $mysqli->query("SELECT * FROM `LARX_admins` WHERE `id` = '$userID' LIMIT 1");
			if($accessQuery->num_rows == 1) {
				$access = $accessQuery->fetch_array();
				$accessLevel = $access['access_level'];
				if($accessLevel < $level) {
					echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">You do not have access to this page.</div>';
				}
			}
			$accessQuery->close();
		}
		
		public function adminRole($mysqli,$adminID) {
			$adminQuery = $mysqli->query("SELECT * FROM LARX_admins, LARX_admin_access WHERE LARX_admins.id = ".$adminID." AND LARX_admins.access_level = LARX_admin_access.id LIMIT 1");
			$admin = $adminQuery->fetch_assoc();
			if($admin['access'] == 4) {
				return "Hello <span style=\"color:#cb202a;\">".$admin['access_name']."</span> | <a href=\"/admin/main/users/\">Edit Users</a>";
			} else {
				return "Hello <span style=\"color:#cb202a;\">".$admin['access_name']."</span>";
			}
			$adminQuery->close();
		}
		
		public function adminRoleValue($mysqli,$adminID) {
			$adminQuery = $mysqli->query("SELECT * FROM LARX_admins, LARX_admin_access WHERE LARX_admins.id = ".$adminID." AND LARX_admins.access_level = LARX_admin_access.id LIMIT 1");
			$admin = $adminQuery->fetch_assoc();
			return $admin['access'];
			$adminQuery->close();
		}
		
		
	}
	
	
?>