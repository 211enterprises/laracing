<?php

	$id = makeSQLSafe($mysqli,$_GET['id']);
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates` WHERE `id` = '$id' LIMIT 1");

	if($classQuery->num_rows == 1) {
		$class = $classQuery->fetch_array();
		$classesArray = explode(",",$class['class_id']);
		
		if(!isset($_POST['updateClass'])) {
			echo '<h1>Edit Class:</h1>';
			echo '<a href="/admin/main/?controller=classes&action=delete&id='.$class['id'].'" class="deleteContent">Delete This Class</a>';
			echo '<a href="/admin/main/?controller=classes&action=roster&id='.$class['id'].'" class="viewContent">View Class Roster</a>';
		}
		
	} else {
		echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">Class Can\'t Be Found</div>';
	}
	
	if(isset($_POST['updateClass'])) {
		//DATE
		$dateM = makeSQLSafe($mysqli,$_POST['dateM']);
		$dateD = makeSQLSafe($mysqli,$_POST['dateD']);
		$dateY = makeSQLSafe($mysqli,$_POST['dateY']);
		$formatDate = $dateY."-".$dateM."-".$dateD;

		//TIME
		$timeH = makeSQLSafe($mysqli,$_POST['timeH']);
		$timeM = makeSQLSafe($mysqli,$_POST['timeM']);
		$timePeriod = makeSQLSafe($mysqli,$_POST['timePeriod']);
		$formatTime = date("H:i:00",strtotime($timeH.":".$timeM.":00 ".$timePeriod));

		$classLimit = makeSQLSafe($mysqli,$_POST['classLimit']);
    $slotsTaken = makeSQLSafe($mysqli, $_POST['classTaken']);
		$trackLocation = makeSQLSafe($mysqli,$_POST['trackLocation']);
		$classPackages = makeSQLSafe($mysqli,implode(",",$_POST['classPackages']));

		$mysqli->query("UPDATE `LARX_class_dates` SET `class_id`='$classPackages', `date`='$formatDate', `time`='$formatTime', `track_location`='$trackLocation', `slots_taken`='$slotsTaken', `class_limit`='$classLimit' WHERE `id` = '$id' LIMIT 1");

	}
	
	$trackQuery = $mysqli->query("SELECT `track_id`,`track_name` FROM `LARX_track_locations` ORDER BY `track_name` ASC");
	$classesQuery = $mysqli->query("SELECT `id`,`class_name` FROM `LARX_classes` ORDER BY `order` ASC");
	
?>

<?php if(!isset($_POST['updateClass'])) { ?>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<fieldset class="editFAQ">
			<label>Class Date:</label>
			<select name="dateM" id="dateM" class="classDate">
				<option value="">Month:</option>
		<?php for($m = 01; $m <= 12; $m++) { ?>
				<option value="<?php echo str_pad($m,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['date'],5,2) == $m) echo 'selected="selected"'; ?>><?php echo str_pad($m,2,0,STR_PAD_LEFT); ?></option>
		<?php } ?>
			</select>
			<select name="dateD" id="dateD" class="classDate">
				<option value="">Day:</option>
		<?php for($d = 01; $d <= 31; $d++) { ?>
				<option value="<?php echo str_pad($d,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['date'],8,2) == $d) echo 'selected="selected"'; ?>><?php echo str_pad($d,2,0,STR_PAD_LEFT); ?></option>
		<?php } ?>
			</select>
			<select name="dateY" id="dateY" class="classDate">
				<option value="">Year:</option>
				<option value="<?php echo date("Y"); ?>" <?php if(substr($class['date'],0,4) == date("Y")) echo 'selected="selected"'; ?>><?php echo date("Y"); ?></option>
				<option value="<?php echo date("Y") + 1; ?>" <?php if(substr($class['date'],0,4) == date("Y") + 1) echo 'selected="selected"'; ?>><?php echo date("Y") + 1; ?></option>
			</select>
			
			<label>Class Time:</label>
			<select name="timeH" id="timeH" class="classTime">
				<option value="">Hour:</option>
				<option value="01" <?php if(substr($class['time'],0,2) == "01" || substr($class['time'],0,2) == "13") echo 'selected="selected"'; ?>>01</option>
				<option value="02" <?php if(substr($class['time'],0,2) == "02" || substr($class['time'],0,2) == "14") echo 'selected="selected"'; ?>>02</option>
				<option value="03" <?php if(substr($class['time'],0,2) == "03" || substr($class['time'],0,2) == "15") echo 'selected="selected"'; ?>>03</option>
				<option value="04" <?php if(substr($class['time'],0,2) == "04" || substr($class['time'],0,2) == "16") echo 'selected="selected"'; ?>>04</option>
				<option value="05" <?php if(substr($class['time'],0,2) == "05" || substr($class['time'],0,2) == "17") echo 'selected="selected"'; ?>>05</option>
				<option value="06" <?php if(substr($class['time'],0,2) == "06" || substr($class['time'],0,2) == "18") echo 'selected="selected"'; ?>>06</option>
				<option value="07" <?php if(substr($class['time'],0,2) == "07" || substr($class['time'],0,2) == "19") echo 'selected="selected"'; ?>>07</option>
				<option value="08" <?php if(substr($class['time'],0,2) == "08" || substr($class['time'],0,2) == "20") echo 'selected="selected"'; ?>>08</option>
				<option value="09" <?php if(substr($class['time'],0,2) == "09" || substr($class['time'],0,2) == "21") echo 'selected="selected"'; ?>>09</option>
				<option value="10" <?php if(substr($class['time'],0,2) == "10" || substr($class['time'],0,2) == "22") echo 'selected="selected"'; ?>>10</option>
				<option value="11" <?php if(substr($class['time'],0,2) == "11" || substr($class['time'],0,2) == "23") echo 'selected="selected"'; ?>>11</option>
				<option value="12" <?php if(substr($class['time'],0,2) == "12" || substr($class['time'],0,2) == "24") echo 'selected="selected"'; ?>>12</option>
			</select>
			<select name="timeM" id="timeM" class="classTime">
				<option value="">Minutes:</option>
		<?php for($min = 00; $min <= 59; $min++) { ?>
				<option value="<?php echo str_pad($min,2,0,STR_PAD_LEFT); ?>" <?php if(substr($class['time'],3,2) == $min) echo 'selected="selected"'; ?>><?php echo str_pad($min,2,0,STR_PAD_LEFT); ?></option>
		<?php } ?>
			</select>
			<select name="timePeriod" id="timePeriod" class="classTime">
				<option value="">Time Period:</option>
				<option value="AM" <?php if(substr($class['time'],0,2) < 12) echo 'selected="selected"'; ?>>AM</option>
				<option value="PM" <?php if(substr($class['time'],0,2) >= 12) echo 'selected="selected"'; ?>>PM</option>
			</select>

			<label>Class Limit:</label>
			<select name="classLimit" id="classLimit">
				<option value="">Select Class Limit:</option>
	<?php for($l = 1; $l <= 100; $l++) { ?>
				<option value="<?php echo $l; ?>" <?php if($class['class_limit'] == $l) echo 'selected="selected"'; ?>><?php echo $l; ?></option>
	<?php } ?>
			</select>

      <label>Class Taken:</label>
      <input type="number" name="classTaken" value="<?php echo $class['slots_taken']; ?>" />

			<label>Class Track Location:</label>
			<select name="trackLocation" id="trackLocation">
	<?php if($trackQuery->num_rows > 0) {
			while($track = $trackQuery->fetch_array()) { ?>
				<option value="<?php echo $track['track_id']; ?>" <?php if($track['track_id'] == $class['track_location']) echo 'selected="selected"'; ?>><?php echo $track['track_name']; ?></option>
	<?php } } ?>
			</select>
			
			<label>Available Class Packages: <small>Windows: Hold (ctrl). Mac: Hold (command).</small></label>
			<select name="classPackages[]" id="classPackages" multiple="true">
	<?php if($classesQuery->num_rows > 0) {
			while($classes = $classesQuery->fetch_array()) { ?>
				<option value="<?php echo $classes['id']; ?>" <?php if(in_array($classes['id'], $classesArray)) echo 'selected="selected"'; ?>><?php echo $classes['class_name']; ?></option>
	<?php } } ?>
			</select>
			
			<button type="submit">Edit Class</button>
		</fieldset>
		<input type="hidden" name="updateClass" value="<?php echo $class['id']; ?>" />
	</form>	
<?php } else { ?>
  <div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">
    Class Has Been Updated
  </div>
<?php }

$trackQuery->close();
$mysqli->close();
?>
<script type="text/javascript" src="/media/js/admin/classes.js"></script>
