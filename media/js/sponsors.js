/*
*         LA Racing Experience
*				Sep 2012
*       http://www.laracingx.com/
*    ______________________________
*   Powered By AC Media Designs, Inc
*/		
		
		//MOVING NAVIGATION
		$(function() {
			$(window).on('scroll', function() {
				var scrollPosition = $(document).scrollTop();
				if(scrollPosition > 124) {
					$('header section nav.mainNav').css("position","fixed").css("top","4px");
				} else {
					$('header section nav.mainNav').css("position","relative").css("top","0px");
				}
			});
		});
		
		
		//CALENDAR BOOKING
		$(document).ready(function() {
		
			$('table.date tbody tr td.arrow a.calArrowL,table.date tbody tr td.arrow a.calArrowR').live("click",function() {
				var showCalDate = this.id;
				$.ajax({
					url: "/ajax/calendar.php",
					data: "d="+showCalDate,
					success: function(data) {
						$('div.calendarContainer').html("");
						$('div.calendarContainer').append(data);
					}
				});
				return false;
			});
		
		});
		
		
		$(function() {
	
			//CALENDAR SHOW DETAILS
			var mouseX, mouseY;
			var classInfo = $('div.calendarClassInfo'); 
			
			$('td.available').live("mouseenter", function() {
				var classDate = this.id;
				if(classDate != "") {
					classInfo.html('');
					classInfo.html('<div class="loading"></div>');
					$(this).mousemove(function(e) {
						mouseX = e.pageX; 
						mouseY = e.pageY;
						classInfo.css("top",mouseY+"px");
						classInfo.css("left",mouseX+"px");
					});
					classInfo.show();
					$.ajax({
						url: "/ajax/classesDetails.php",
						data: "d="+classDate,
						success: function(data) {
							classInfo.html("");
							classInfo.append(data);
						}
					});
				}
			});
			
			classInfo.live("mouseleave", function() {
				classInfo.hide();
			});
			
		});