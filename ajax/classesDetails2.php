<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['d'])) {
		$date = makeSQLSafe($mysqli,$_GET['d']);
		$today = date("Y-m-d");
		
		echo '<div style="position:relative; float:left; width:100%; color:black; font-size:18px; text-align:center; margin:20px 0px;">
			Give us a call at <br /><span style="color:#cb202a; font-weight:bold;">877-901-RACE</span><br /> to check availability for <br />'.date("M jS, Y",strtotime($date)).'
		</div>';
		
	} else {
		exit;
	}

	$dateQuery->close();
	$mysqli->close();
?>