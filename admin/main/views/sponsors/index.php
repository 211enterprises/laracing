<?php
	$sponsorQuery = $mysqli->query("SELECT * FROM `LARX_sponsors` ORDER BY `order` ASC");
?>

	<h1>LARX Sponsors</h1>
	<a href="<?php echo ADMIN_ROOT; ?>/?controller=sponsors&action=add" class="addContent">Add a Sponsor</a>
	
<?php if($sponsorQuery->num_rows > 0) { ?>
	<ul class="sponsors">
<?php while($sponsor = $sponsorQuery->fetch_assoc()) { ?>
		<li>
			<a href="<?php echo ADMIN_ROOT; ?>/?controller=sponsors&action=edit&id=<?php echo $sponsor['id']; ?>">
				<div class="sponsorName"><?php echo substr($sponsor['sponsor_name'],0,30); ?></div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Testimonials</div>';

$sponsorQuery->close();
$mysqli->close(); ?>