<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	
	if(isset($_GET['m']) && is_numeric($_GET['m'])) {
		$m = makeSQLSafe($mysqli,$_GET['m']);
		
		$mediaQuery = $mysqli->query("SELECT * FROM `LARX_album_media` WHERE `id` = '$m' LIMIT 1");
		if($mediaQuery->num_rows > 0) {
			$media = $mediaQuery->fetch_assoc();
			
			switch($media['media_source']) {
				case "youtube":
					echo '<iframe src="http://www.youtube.com/embed/'.$media['media_path'].'?autoplay=1"></iframe>';	
					break;
				case "vimeo":
					echo '<iframe src="http://player.vimeo.com/video/'.$media['media_path'].'?autoplay=1"></iframe>';
					break;
				case "image":
					echo '<img src="'.$media['media_path'].'" alt="" />';
					break;
				default:
					echo '<img src="'.$media['media_path'].'" alt="" />';
					break;
			  }
			
		} else {
			echo '<div class="voucherPopUp" style="display:block; z-index:503;">Media coming soon.</div>';
		}
	} else {
		echo '<div class="voucherPopUp" style="display:block; z-index:503;">No media found. Please try again.</div>';
	}
	
?>