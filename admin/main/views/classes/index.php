<?php
	
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	//require($_SERVER['DOCUMENT_ROOT']."/php_includes/admin.inc");
	
	//$admin = new admin;
	
	//$admin->access($mysqli,$_SESSION['adminID'],0);
	
	$classQuery = $mysqli->query("SELECT * FROM `LARX_class_dates`,`LARX_track_locations` WHERE LARX_class_dates.track_location = LARX_track_locations.track_id AND LARX_class_dates.date >= NOW() ORDER BY LARX_class_dates.date ASC");
?>

	<h1>Class Schedule</h1>
	<a href="/admin/main/?controller=classes&action=add" class="addContent">Add a Class</a>
	
<?php if($classQuery->num_rows > 0) { ?>
	<ul class="classes">
<?php while($class = $classQuery->fetch_assoc()) { ?>
		<li>
			<a href="/admin/main/?controller=classes&action=edit&id=<?php echo $class['id']; ?>">
				<div class="datetime"><?php echo date("M. jS, Y", strtotime($class['date']))."<br />".date("h:i A", strtotime($class['time'])); ?></div>
				<div class="location"><?php echo substr($class['track_name'],0,24); ?></div>
				<div class="status">Taken: <span><?php echo $class['slots_taken']; ?></span> Limit: <span><?php echo $class['class_limit']; ?></span></div>
				<div class="edit">Edit Class</div>
			</a>
		</li>
<?php } ?>
	</ul>
<?php } else echo '<div style="position:relative; float:left; width:100%; color:white; font-size:26px; text-align:center; margin:100px 0px;">No Current Classes</div>'; ?>