<div class="row-fluid">
	<div class="col-md-12">
		<h3>Group Information:</h3>
	</div>
	<div class="col-md-12">
		Group Name: <span><?php echo $group['name']; ?></span>
	</div>
	<div class="col-md-6">
		Campaign Start Date: <span><?php echo date("M dS, Y", strtotime($group['campaign_start'])); ?></span>
	</div>
	<div class="col-md-6">
		Campaign End Date: <span><?php echo date("M dS, Y", strtotime($group['campaign_end'])); ?></span>
	</div>
</div>

<div class="row-fluid">
	<div class="col-md-6">
		Contact Name: <span><?php echo ($group['contact_name'] != NULL) ? $group['contact_name'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Contact Email: <span><?php echo ($group['contact_email'] != NULL) ? $group['contact_email'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Contact Phone: <span><?php echo ($group['campaign_phone'] != NULL) ? $group['campaign_phone'] : 'N/A'; ?></span>
	</div>
	<div class="col-md-6">
		Representative Name: <span><?php echo ($group['rep_name'] != NULL) ? $group['rep_name'] : 'N/A'; ?></span>
	</div>
</div>