<?php

	require($_SERVER['DOCUMENT_ROOT']."/php_includes/connection.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/config.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/classes.inc");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/coreClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/LAFundClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/racePassClass.php");
	require($_SERVER['DOCUMENT_ROOT']."/php_includes/paymentsClass.php");
	
	$template = new template;
	$core = new core;
	$LAFundRacing = new LAFundRacing($mysqli);
	$Register = new LAFundRegister($mysqli);
	
	$processed = false;
	$displayError = false;
	
	if(isset($_POST) && !empty($_POST)) {
		$processRegistration = $Register->process_registration($_POST);
		
		if($processRegistration) {
			$processed = true;
		} else {
			//Display card error message to user
			$errorMsg = "An error occured during registeration. Please call us at (855)RACE-4-FUNDS to finalize your registration.";
			$displayError = true;
		}
	}	
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>LA Fund Racing</title>
<link href="/media/images/favicon.ico" type="image/png" rel="icon" />
<link href="/media/style/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="/media/style/datepicker.css" type="text/css" rel="stylesheet" />
<link href="/media/style/default.css" type="text/css" rel="stylesheet" />
<link href="/media/style/lafundracing.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script type="text/javascript" src="/media/js/xdr.min.js"></script>
<script type="text/javascript" src="/media/js/global.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/lafundracing.js"></script>
<script type="text/javascript" src="/media/js/LAFR-register.js"></script>
<script type="text/javascript" src="/media/js/bootstrap/bootstrap-datepicker.js"></script>
<script>
	$(function() {
		$('input#groupCampaignStart').datepicker();
		$('input#groupCampaignEnd').datepicker();
	});
</script>
<!--[if IE]>
<script src="/media/js/compatibility.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

	<?php require_once('../partials/header.php'); ?>
	
	<!--MAIN CONTENT-->
	<div class="container">
<?php if(!$processed) { ?>		
		<h2 class="orgHeader">Register Your Organization:</h2>
	</div>
	
	<form action="" method="post">
		<div class="container form">
			<hr />
			
			<div class="row registeredOrganizations">
				<div class="col-md-12">
					<h3>Are you already a registered organization?</h3>
					<h4>If not, please leave blank.</h4>
				</div>
				<div class="col-md-12">
					<select name="registeredOrg" id="registeredOrg" class="greySelect">
						<option value="">Select:</option>
						<?php
						foreach($Register->get_active_fund_organizations() as $val) {
							echo '<option value="'.$val['id'].'">'.$val['name'].'</option>';
						}
						?>
					</select>
				</div>
			</div>
			
			<hr />
			
			<div id="registerOrg">
				<h3>Organization Information:</h3>
				<div class="row">
					<div class="col-md-6">
					<?php 
						$core->labelFor('name', 'Name:', true);
						$core->input('text', 'name', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('email_address', 'Email Address:', true);
						$core->input('email', 'email_address', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php 
						$core->labelFor('address', 'Address:', true);
						$core->input('text', 'address', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('city', 'City:', true);
						$core->input('tel', 'city', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php 
						$core->labelFor('state', 'State:', true);
						$core->input('text', 'state', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('zip_code', 'Zip Code:', true);
						$core->input('text', 'zip_code', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php 
						$core->labelFor('phone', 'Phone:', true);
						$core->input('text', 'phone', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('EIN', 'EIN/Federal Tax Number:', true);
						$core->input('text', 'EIN', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php $core->labelFor('nonprofit', 'Non-Profit Organization:', true); ?>
						<select name="nonprofit" id="nonprofit" required>
							<option value="">Select:</option>
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
					<div class="col-md-6">
						<?php $core->labelFor('category', 'Organization Category:', true); ?>
						<select name="category" id="category" required>
							<option value="">Select:</option>
							<?php
							foreach($LAFundRacing->get_lafr_categories() as $val) {
								echo '<option value="'.$val['id'].'">'.ucfirst($val['type']).'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<hr />
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3>Organization Group:</h3>
					<h5>(i.e. Team Name, School Sport or Activity, etc.)</h5>
				</div>
			</div>

			<div class="group">
				<div class="row">
					<div class="col-md-6">
					<?php
						$core->labelFor('groupName', 'Name:', true);
						$core->input('text', 'groupName', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupCampaignStart', 'Campaign Start Date:', true);
					?>
						<input type="text" name="groupCampaignStart" id="groupCampaignStart" class="form-control input-lg" data-date-format="yyyy-mm-dd" readonly required>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupCampaignEnd', 'Campaign End Date:', true);
					?>
						<input type="text" name="groupCampaignEnd" id="groupCampaignEnd" class="form-control input-lg" data-date-format="yyyy-mm-dd" readonly required>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupContactName', 'Contact Name:', true);
						$core->input('text', 'groupContactName', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupEmail', 'Email Address:', true);
						$core->input('text', 'groupEmail', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupPhone', 'Phone:');
						$core->input('text', 'groupPhone', 'form-control input-lg', '', $_POST, false);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('groupRep', 'Who referred you to LA Fund Racing:', true);
						$core->input('text', 'groupRep', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-12">
					<?php
						$core->labelFor('groupMission', 'Group Mission:');
						$core->textarea('groupMission', 'form-control');
					?>
					</div>
				</div>
				
				<hr />
				
				<div class="row">
					<div class="col-md-12">
						<h3>Group Payable Information:</h3>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payAttn', 'Attention:', true);
						$core->input('text', 'payAttn', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payName', 'Payable Name:', true);
						$core->input('text', 'payName', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payAddress', 'Address:', true);
						$core->input('text', 'payAddress', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payCity', 'City:', true);
						$core->input('text', 'payCity', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payState', 'State:', true);
						$core->input('text', 'payState', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
					<div class="col-md-6">
					<?php
						$core->labelFor('payZip', 'Zip Code:', true);
						$core->input('text', 'payZip', 'form-control input-lg', '', $_POST, true);
					?>
					</div>
				</div>
			</div>
			
			<hr />
			<div class="row">
				<div class="col-md-8 iAgree">
					<input type="checkbox" name="iAgree" id="iAgree" value="1" required />
					I, the authorized agent of the above organization and/or group, declare under penalty of perjury that the information I have provided is true and correct to the best of my information and belief.
				</div>
				<div class="col-md-4">
					<?php $core->submitBtn('Register My Organization'); ?>
				</div>
			</div>
		</div>
	</form>
	
	<?php include_once('_terms.php') ?>
<?php } else { ?>
	<div class="container form">
		<div class="laFundProcessed">
			<img src="/media/images/driverCelebrating.png" class="celebrating-driver" alt="" />
			<hr style="margin-top: 0;" />
			<h2>
				Thank you!<br />
				You have successfully submitted your <?php echo ($_POST['registeredOrg'] == "") ? 'organization' : 'group' ?> registration.<br />
				We will contact you once your <?php echo ($_POST['registeredOrg'] == "") ? 'organization' : 'group' ?> has been approved.
			</h2>
			<hr />
		</div>
	</div>
<?php } ?>
	<!--END MAIN CONTENT-->
	
<?php 
	if($displayError) {
		$core->flashNotice($errorMsg, true);
	}
?>
</body>
</html>
<?php $mysqli->close(); ?>